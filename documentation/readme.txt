=================================================================
testEM : Library suit to test electrical models from Elvira
Release 1.0. 14 Feb 2014
=================================================================
Table of Contents
-----------------

1 Models Implemented
2 Input File
3 Compiling the Program
4 Running the Program


1 Models Implemented:
--------------------
The Id Number of the model is found in the model_Id.inc file in the
source/src directory. This Table must be updated with the model number

Legend
NSVAR   : Number of state variables (Potential is not included)
NCur    : Number of ionic currents 
NModPar : Number of Modifiable parameters of the model
	Table 1. Electrical Models
    Name            MN  NSVar  NCur NModPar
Isquemic Zone       -1    1     0      3
Roger&McCulloc       0    1     0      6
TenTusscher ENDO     1   21    14     21
TenTusscher  MID     2   21    14     21
TenTusscher  EPI     3   21    14     21 
Luo Rudy    ENDO     4   27    21     22 
Luo Rudy     MID     5   27    21     22
Luo Rudy     EPI     6   27    21     22
BuenoOrovio ENDO     7    3     4     30
BuenoOrovio  MID     8    3     4     30
BuenoOrovio  EPI     9    3     4     30
Nygren     ATRIA    10   35    19     13
Purkinje Stewart    11   20    16     19
Grandi      ENDO    12   38    24     20
Grandi       EPI    13   38    24     20
Carro       ENDO    14   39    24     11
Carro        EPI    15   39    24     11
GrandiHF    ENDO    16   38    25     20
GrandiHF     EPI    17   38    25     20
Fibroblast    --    18    2     5      4
MALECKAR   ATRIA    19   28    19     14
MALECKAR   RA-PM    191  28    19     14
MALECKAR   CT-BB    192  28    19     14
MALECKAR    RAA     193  28    19     14
MALECKAR    TVR     194  28    19     14
MALECKAR     LA     195  28    19     14
MALECKAR     PV     196  28    19     14
MALECKAR    LAA     197  28    19     14
MALECKAR    MVR     198  28    19     14
Ohara       ENDO    20   40    17     14
Ohara        MID    21   40    17     14
Ohara        EPI    22   40    17     14
Ohara       ENDO    201   40    17     14
Ohara        MID    211   40    17     14
Ohara        EPI    221   40    17     14
Courtemanche ATRIA  23   20    17      9 
Courtemanche RA_PM  231  20    17      9 
Courtemanche CT_BB  232  20    17      9 
Courtemanche  TVR   233  20    17      9 
Courtemanche  RAA   234  20    17      9 
Courtemanche   LA   235  20    17      9 
Courtemanche  LAA   236  20    17      9 
Courtemanche   PV   237  20    17      9 
Courtemanche  MVR   238  20    17      9 


2 Input File: 
------------
The program uses an input file to define the model
to be tested, the parameters to be modified, if any,
the simulation protocol and the output frequency.

Keywords in the input file:

#MODEL     : Defines the model ID according to Table 1
#CAPACITANCE: Cell capacitance. If not defined assumed as 1.0
#PARAMETERS: Is used to defined parameters to be modified
             in the model. It defines a mask with the 
			 parameters and the new values, e.g.

   #PARAMETERS
   2 7 15 0.4 3.2

   It is defining a mask of two values where the code 
   will overwrite the default value of parameter 7 and 15
   with the new values 0.4 and 3.2 respectively
   
   NOTE: If no parameters are to be modified, do not include
   this label in the input file, or comment it out with a !
   symbol at the begining of the line

#STEP      : Defines total simulation time and time increment.
             Three parameters must be defined, Tinitial, Tincrement,
             and Tfinal in msec
             e.g.,

	#STEP
	0.0 0.002 2000.0

	Defines a simulation initiating at 0.0 msec, ending at 2000.0 msec
	with a time increment of 0.002 msec

#STIMULUS  : Defines the stimulation protocol. The definition is based 
             on trains of stimulus, the parameters for this label are
             defined as follows

    #STIMULUS
    N_Train   --> Number of train of stimulus
    Tini BCL StimDur Stim_Curr

    example:
    #STIMULUS
    2
    0.0 1000.0 5.0 9.5
    5000.0 600.0 2.0 20.5

    It defines two trains of stimulus, the first starting at 0.0 msec with
    a basic cyclic length (BCL) of 1000.0 msec, a stimulus duration of 5 msec
    with a stimulation current of 9.5 (the current units must be consistent
    with the model used). The second train starts at 5000.0 msec, with a BCL 
    of 600.0 msec, a stimulus duration of 2.0 msec and a stimulation current
    of 20.5.

    #POST      : Defines the output frequency and the first iteration at
	             which data is saved, e.g.

    #POST
    2500 50

    State variables and currents will be saved each 50 increments, starting
	at increment 2500
    (dt = 50*Tincrement)
    #END       : The input file must end with this tag

Input File Examples:

Example 1: Uses Grandi ENDO with default parameters. Simulation of
           8000 msec, with a time increment of 0.002 msec. Defines
           two trains of stimulus with a BCL of 1000 and 600 msec
           and starting at 0.0 and 5000.0 msec respectively. Output
           is saved after 100 time increments, starting at increment
		   500000 (1sec)
#MODEL
12     
#CAPACITANCE
1.0           
!#PARAMETERS
!2 7 17 17.25 8.0
#STEP
0.0 0.002 8000.0
#STIMULUS
2 
0.0 1000.0 5.0 9.5
5000.0 600.0 5.0 9.5
#POST
500000 100
#END

Example 2: Uses Grandi ENDO modifing two parameters (7, Na max conductance
           and 17, extracellular Potasium concentration). Simulation of
           8000 msec, with a time increment of 0.002 msec. Defines
           two trains of stimulus with a BCL of 1000 and 600 msec
           and starting at 0.0 and 5000.0 msec respectively. Output
           is saved after 100 time increments, starting at increment
		   500000 (1sec)
#MODEL
12
#CAPACITANCE
1.0
#PARAMETERS
2 7 17 17.25 8.0
#STEP
0.0 0.002 8000.0
#STIMULUS
2 
0.0 1000.0 5.0 9.5
5000.0 600.0 5.0 9.5
#POST
500000 100
#END

3 Compiling the Program:
-----------------------

make clean
make COMP=intel (compiles with intel -default-)
make COMP=gcc   (compiles with gfortran)

NOTE: Depending on your system, you may need to load
the module for the compiler. Talk with your system 
administrator

4 Running the Program:
---------------------

All libraries in the executable are linked statically. Therefore,
the code can be excuted in any computer in the same platform where
the program has been compiled.

Linux:
./test_main -i <path_for_input_file>/<input_file> -o <path_for_output_file>/<output_identifier>

For the output, the code attaches to the <output_identifier> two suffixes: i) _stat.dat, 
for state variables, ii) _curr.dat, for currents

The total time is not included in the _curr.dat file.

5 Benchmarking and model setting:
--------------------------------

The models in EM_SRC library have been paced for at least 20 minutes at 1Hz and the 
value attained for each state variable at the end of the protocol is taken as
initial conditions. Therefore, the models can be considered "stable" for an 
stimulation frequency of 1Hz.

For every model under development, in order to be added to the library, must be
fully tested and benchmarked against the original code (if it exists), or a 
golden standard data set.

Along with this file you will find input files for each of the electric models 
that have been incorporated. These files have been  prepared to run the 
benchmarking protocol. The benchmarking protocol consistis on stimulating the 
model 100 times at a pace of 1Hz. Data for the last action potential is 
compared against the golden standard data. Initial conditions are assumed to
be stable.

Golden standard data can be found in the directory VALIDATION
