#!/bin/bash
# $1 input data file with full path
# $2 output identifier with full path of output directory
# $3 log file identifier
# SOFT_PATH: path to main package folder 

SOFT_PATH=<path to installation directory>

nohup $SOFT_PATH/bin/testem_intel -i $1 -o $2 >& $3 &
