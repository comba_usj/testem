function plot_bench()
clear;
clc;

outC=load('output_courtemancheRL2.dat');

outE=load('courtemanche_elvira_stat.dat');

plot(outC(:,1)-outC(1,1),outC(:,2),'r'); hold
plot(outE(:,end)-outE(1,end),outE(:,end-1),'b');
legend('CellML','Elvira');
xlabel('t, ms');
ylabel('V, mV');
hold;

Id = [2 19 20 21 15 5 12 3 7 18 9 11 4 6 16 13 8 10 17 14 22];
nvar = length(outE(1,:))-1;

R2 = zeros(nvar,1);
R2(1,1) = R2calc(outC(:,Id(1)),outE(:,end-1));
for i=2:nvar
    R2(i,1)=R2calc(outC(:,Id(i)),outE(:,i-1));
end
fprintf('Fitting (R^2 value):\n');
fprintf('\n');
fprintf('V      %8.4f\n',R2(1,1));
fprintf('u      %8.4f\n',R2(2,1));
fprintf('v      %8.4f\n',R2(3,1));
fprintf('w      %8.4f\n',R2(4,1));
fprintf('d      %8.4f\n',R2(5,1));
fprintf('h      %8.4f\n',R2(6,1));
fprintf('xr     %8.4f\n',R2(7,1));
fprintf('Nai    %8.4f\n',R2(8,1));
fprintf('Ki     %8.4f\n',R2(9,1));
fprintf('Carel  %8.4f\n',R2(10,1));
fprintf('oi     %8.4f\n',R2(11,1));
fprintf('ui     %8.4f\n',R2(12,1));
fprintf('m      %8.4f\n',R2(13,1));
fprintf('j      %8.4f\n',R2(14,1));
fprintf('f      %8.4f\n',R2(15,1));
fprintf('xs     %8.4f\n',R2(16,1));
fprintf('oa     %8.4f\n',R2(17,1));
fprintf('ua     %8.4f\n',R2(18,1));
fprintf('fCa    %8.4f\n',R2(19,1));
fprintf('Cai    %8.4f\n',R2(20,1));
fprintf('Caup   %8.4f\n',R2(21,1));
end

function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
R2 = 1-Serr/Stot;
end
