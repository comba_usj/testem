courtemanche_elvira_stat.dat has been generated with the implementation in Elvira

Files output_coutermancheRL2.dat has been generated with the codes downloded from CellML. The original codes have been modified to introduced Rush-Larsen integration for the gating variables.

The validation shows a R2 of 1 for all state variables, except for the concentrations: Ki, Nai and CaC where a small biass is observed. The difference in the values is less than a 0.01% 

The validation have been performed by comparing the output after 10min of simulation at a BCL=1s with an stimulation amplitude of 20 pA/pF and a duration of 2ms
