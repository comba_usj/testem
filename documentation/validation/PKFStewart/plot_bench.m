function plot_bench()
clear;
clc;

endoC=load('ST_var.dat');
endoC(:,1)=endoC(:,1)-endoC(1,1);

endoE=load('ST_elvira_stat.dat');

plot(endoC(:,1),endoC(:,2),'r'); hold
plot(endoE(:,end)-endoE(1,end),endoE(:,end-1),'b');

legend(' PKF C',' PKF Elvira');
xlabel('t, ms');
ylabel('V, mV');
hold;

nvar=length(endoE(1,:))
R2 = zeros(nvar-1,1);
R2(1) = R2calc(endoC(:,2),endoE(:,end-1));
for i=3:nvar
    R2(i-1) = R2calc(endoC(:,i),endoE(:,i-2));
end
fprintf('Fitting (R^2 value):\n');
fprintf('       \n');
fprintf('V     %8.4f\n',R2(1));
fprintf('Cai   %8.4f\n',R2(2));
fprintf('CaSR  %8.4f\n',R2(3));
fprintf('CaSS  %8.4f\n',R2(4));
fprintf('Nai   %8.4f\n',R2(5));
fprintf('Ki    %8.4f\n',R2(6));
fprintf('m     %8.4f\n',R2(7));
fprintf('h     %8.4f\n',R2(8));
fprintf('j     %8.4f\n',R2(9));
fprintf('xs    %8.4f\n',R2(10));
fprintf('r     %8.4f\n',R2(11));
fprintf('s     %8.4f\n',R2(12));
fprintf('d     %8.4f\n',R2(13));
fprintf('f     %8.4f\n',R2(14));
fprintf('f2    %8.4f\n',R2(15));
fprintf('fcass %8.4f\n',R2(16));
fprintf('rr    %8.4f\n',R2(17));
fprintf('oo    %8.4f\n',R2(18));
fprintf('xr1   %8.4f\n',R2(19));
fprintf('xr2   %8.4f\n',R2(20));
fprintf('y     %8.4f\n',R2(21));

end

function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
if(Stot>1.0e-2)
   R2 = 1-Serr/Stot;
else
    R2=1.0;
end
end
