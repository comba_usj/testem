Maleckar_stat.dat and Maleckar_cur.dat have been generated with the implementation in Elvira

Files output_Maleckar*.dat have been generated with the codes downloded from CellML. The original codes have been modified to introduced Rush-Larsen integration for the gating variables.

The validation shows a R2 of 1 for all state variables, except for the concentrations: Ki, Nai and CaC where a small biass is observed. The difference in the values is less than a 0.5% for CaC and less that 0.01% for the other two.

The validation have been performed by comparing the output after 100s of simulation at a BCL=1s
