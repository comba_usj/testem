function plot_bench()
clear;
clc;

outC=load('output_MaleckarRL.dat');

outE=load('Maleckar_stat.dat');

plot(outC(:,1)-outC(1,1),outC(:,2),'r'); hold
plot(outE(:,end)-outE(1,end),outE(:,end-1),'b');
legend('CellML','Elvira');
xlabel('t, ms');
ylabel('V, mV');
hold;

Id = [2 5 6 7 9 10 11 14 15 16 17 19 18 4 13 21 28 27 8 22 23 24 ...
      25 29 30 31 3 12 20];
nvar = length(outE(1,:))-1;
R2 = zeros(nvar,1);
R2(1,1) = R2calc(outC(:,Id(1)),outE(:,end-1));
for i=2:nvar
    R2(i,1)=R2calc(outC(:,Id(i)),outE(:,i-1));
end
fprintf('Fitting (R^2 value):\n');
fprintf('\n');
fprintf('V      %8.4f\n',R2(1,1));
fprintf('m      %8.4f\n',R2(2,1));
fprintf('h1     %8.4f\n',R2(3,1));
fprintf('h2     %8.4f\n',R2(4,1));
fprintf('d1     %8.4f\n',R2(5,1));
fprintf('fl1    %8.4f\n',R2(6,1));
fprintf('fl2    %8.4f\n',R2(7,1));
fprintf('r      %8.4f\n',R2(8,1));
fprintf('s      %8.4f\n',R2(9,1));
fprintf('a_ur   %8.4f\n',R2(10,1));
fprintf('i_ur   %8.4f\n',R2(11,1));
fprintf('pa     %8.4f\n',R2(12,1));
fprintf('n      %8.4f\n',R2(13,1));
fprintf('Nai    %8.4f\n',R2(14,1));
fprintf('Ki     %8.4f\n',R2(15,1));
fprintf('Cai    %8.4f\n',R2(16,1));
fprintf('Caup   %8.4f\n',R2(17,1));
fprintf('Carel  %8.4f\n',R2(18,1));
fprintf('Cab    %8.4f\n',R2(19,1));
fprintf('Oc     %8.4f\n',R2(20,1));
fprintf('Otc    %8.4f\n',R2(21,1));
fprintf('OtMgC  %8.4f\n',R2(22,1));
fprintf('OtMgMg %8.4f\n',R2(23,1));
fprintf('Ocalse %8.4f\n',R2(24,1));
fprintf('f1     %8.4f\n',R2(25,1));
fprintf('f2     %8.4f\n',R2(26,1));
fprintf('Nac    %8.4f\n',R2(27,1));
fprintf('Kc     %8.4f\n',R2(28,1));
fprintf('CaC    %8.4f\n',R2(29,1));
end

function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
R2 = 1-Serr/Stot;
end