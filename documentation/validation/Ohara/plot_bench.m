function plot_bench()
clear;
clc;

endoC=load('output_ENDO.txt');
midC =load('output_MID.txt');
epiC =load('output_EPI.txt');

endoE=load('OHENDO_stat.dat');
midE =load('OHMID_stat.dat');
epiE =load('OHEPI_stat.dat');

plot(endoC(:,end)-endoC(1,end),endoC(:,end-1),'r'); hold;
plot(endoE(:,end)-endoE(1,end),endoE(:,end-1),'b');

plot(midC(:,end)-midC(1,end),midC(:,end-1),'c');
plot(midE(:,end)-midE(1,end),midE(:,end-1),'k');

plot(epiC(:,end)-epiC(1,end),epiC(:,end-1),'g');
plot(epiE(:,end)-epiE(1,end),epiE(:,end-1),'m');

legend('endo C','endo Elvira','mid C','mid Elvira','epi C','epi Elvira');
xlabel('t, ms');
ylabel('V, mV');

nvar = length(endoE(1,:))-1;
R2 = zeros(nvar,3);
for i=1:nvar
    R2(i,1)=R2calc(endoC(:,i),endoE(:,i));
    R2(i,2)=R2calc(midC(:,i),midE(:,i));
    R2(i,3)=R2calc(epiC(:,i),epiE(:,i));
end
fprintf('Fitting (R^2 value):\n');
fprintf('       ENDO      MID     EPI\n');
fprintf('V      %8.4f %8.4f %8.4f\n',R2(end,1),R2(end,2),R2(end,3));
fprintf('Nai    %8.4f %8.4f %8.4f\n',R2(1,1),R2(1,2),R2(1,3));
fprintf('Nass   %8.4f %8.4f %8.4f\n',R2(2,1),R2(2,2),R2(2,3));
fprintf('Ki     %8.4f %8.4f %8.4f\n',R2(3,1),R2(3,2),R2(3,3));
fprintf('Kss    %8.4f %8.4f %8.4f\n',R2(4,1),R2(4,2),R2(4,3));
fprintf('Cai    %8.4f %8.4f %8.4f\n',R2(5,1),R2(5,2),R2(5,3));
fprintf('Cass   %8.4f %8.4f %8.4f\n',R2(6,1),R2(6,2),R2(6,3));
fprintf('CanSR  %8.4f %8.4f %8.4f\n',R2(7,1),R2(7,2),R2(7,3));
fprintf('CajSR  %8.4f %8.4f %8.4f\n',R2(8,1),R2(8,2),R2(8,3));
fprintf('m      %8.4f %8.4f %8.4f\n',R2(9,1),R2(9,2),R2(9,3));
fprintf('hf     %8.4f %8.4f %8.4f\n',R2(10,1),R2(10,2),R2(10,3));
fprintf('hs     %8.4f %8.4f %8.4f\n',R2(11,1),R2(11,2),R2(11,3));
fprintf('j      %8.4f %8.4f %8.4f\n',R2(12,1),R2(12,2),R2(12,3));
fprintf('hsp    %8.4f %8.4f %8.4f\n',R2(13,1),R2(13,2),R2(13,3));
fprintf('jp     %8.4f %8.4f %8.4f\n',R2(14,1),R2(14,2),R2(14,3));
fprintf('mL     %8.4f %8.4f %8.4f\n',R2(15,1),R2(15,2),R2(15,3));
fprintf('hL     %8.4f %8.4f %8.4f\n',R2(16,1),R2(16,2),R2(16,3));
fprintf('hLp    %8.4f %8.4f %8.4f\n',R2(17,1),R2(17,2),R2(17,3));
fprintf('a      %8.4f %8.4f %8.4f\n',R2(18,1),R2(18,2),R2(18,3));
fprintf('iF     %8.4f %8.4f %8.4f\n',R2(19,1),R2(19,2),R2(19,3));
fprintf('iS     %8.4f %8.4f %8.4f\n',R2(20,1),R2(20,2),R2(20,3));
fprintf('ap     %8.4f %8.4f %8.4f\n',R2(21,1),R2(21,2),R2(21,3));
fprintf('iFp    %8.4f %8.4f %8.4f\n',R2(22,1),R2(22,2),R2(22,3));
fprintf('iSp    %8.4f %8.4f %8.4f\n',R2(23,1),R2(23,2),R2(23,3));
fprintf('d      %8.4f %8.4f %8.4f\n',R2(24,1),R2(24,2),R2(24,3));
fprintf('ff     %8.4f %8.4f %8.4f\n',R2(25,1),R2(25,2),R2(25,3));
fprintf('fs     %8.4f %8.4f %8.4f\n',R2(26,1),R2(26,2),R2(26,3));
fprintf('fcaf   %8.4f %8.4f %8.4f\n',R2(27,1),R2(27,2),R2(27,3));
fprintf('fcas   %8.4f %8.4f %8.4f\n',R2(28,1),R2(28,2),R2(28,3));
fprintf('jca    %8.4f %8.4f %8.4f\n',R2(29,1),R2(29,2),R2(29,3));
fprintf('nca    %8.4f %8.4f %8.4f\n',R2(30,1),R2(30,2),R2(30,3));
fprintf('ffp    %8.4f %8.4f %8.4f\n',R2(31,1),R2(31,2),R2(31,3));
fprintf('fcafp  %8.4f %8.4f %8.4f\n',R2(32,1),R2(32,2),R2(32,3));
fprintf('xrf    %8.4f %8.4f %8.4f\n',R2(33,1),R2(33,2),R2(33,3));
fprintf('xrs    %8.4f %8.4f %8.4f\n',R2(34,1),R2(34,2),R2(34,3));
fprintf('xs1    %8.4f %8.4f %8.4f\n',R2(35,1),R2(35,2),R2(35,3));
fprintf('xs2    %8.4f %8.4f %8.4f\n',R2(36,1),R2(36,2),R2(36,3));
fprintf('xk1    %8.4f %8.4f %8.4f\n',R2(37,1),R2(37,2),R2(37,3));
fprintf('Jrelnp %8.4f %8.4f %8.4f\n',R2(38,1),R2(38,2),R2(38,3));
fprintf('Jrelp  %8.4f %8.4f %8.4f\n',R2(39,1),R2(39,2),R2(39,3));
fprintf('CaMKt  %8.4f %8.4f %8.4f\n',R2(40,1),R2(40,2),R2(40,3));

end

function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
R2 = 1-Serr/Stot;
end