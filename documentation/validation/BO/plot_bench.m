function plot_bench()
clear;
clc;
close all;

endoC=load('output_endo_var.dat');
midC =load('output_mid_var.dat');
epiC =load('output_epi_var.dat');

endoE=load('BO_endo_elvira_stat.dat');
midE =load('BO_mid_elvira_stat.dat');
epiE =load('BO_epi_elvira_stat.dat');

plot(endoC(:,1)-endoC(1,1),endoC(:,2),'r'); hold
plot(endoE(:,end)-endoE(1,end),endoE(:,end-1),'b');

plot(midC(:,1)-midC(1,1),midC(:,2),'c');
plot(midE(:,end)-midE(1,end),midE(:,end-1),'k');

plot(epiC(:,1)-epiC(1,1),epiC(:,2),'g');
plot(epiE(:,end)-epiE(1,end),epiE(:,end-1),'m');

legend('endo C','endo Elvira','mid C','mid Elvira','epi C','epi Elvira');
xlabel('t, ms');
ylabel('V, mV');


nvar=length(endoC(1,:));
R2 = zeros(nvar-1,3);
R2(1,1) = R2calc(endoC(:,2),endoE(:,end-1));
R2(1,2) = R2calc(midC(:,2),midE(:,end-1));
R2(1,3) = R2calc(epiC(:,2),epiE(:,end-1));
for i=3:nvar
    R2(i-1,1) = R2calc(endoC(:,i),endoE(:,i-2));
    R2(i-1,2) = R2calc(midC(:,i),midE(:,i-2));
    R2(i-1,3) = R2calc(epiC(:,i),epiE(:,i-2));
end

fprintf('COMPUTED ERRORS (in %%):\n');
fprintf('       ENDO      MID     EPI\n');
fprintf('V     %8.4f %8.4f %8.4f\n',R2(1,1),R2(1,2),R2(1,3));
fprintf('vg    %8.4f %8.4f %8.4f\n',R2(2,1),R2(2,2),R2(2,3));
fprintf('wg    %8.4f %8.4f %8.4f\n',R2(3,1),R2(3,2),R2(3,3));
fprintf('sg    %8.4f %8.4f %8.4f\n',R2(4,1),R2(4,2),R2(4,3));
end


function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
if(Stot>1.0e-2)
   R2 = 1-Serr/Stot;
else
    R2=1.0;
end
end