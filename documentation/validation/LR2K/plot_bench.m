function plot_bench()
clear;
clc;
close all;

endoC=load('LR2K_var.dat');

endoE=load('LR2K_elvira_stat.dat');
plot(endoC(:,1)-endoC(1,1),endoC(:,2),'r'); hold
plot(endoE(:,end)-endoE(1,end),endoE(:,end-1),'b');


legend('C code','Elvira');
xlabel('t, ms');
ylabel('V, mV');

%nvar = length(endoC(1,:));
nvar=22;
R2 = zeros(nvar-1,1);
R2(1) = R2calc(endoC(:,2),endoE(:,end-1));
for i=3:nvar
    R2(i-1) = R2calc(endoC(:,i),endoE(:,i-2));
end
fprintf('Fitting (R^2 value):\n');
fprintf('V     %8.4f\n',R2(1));
fprintf('m     %8.4f\n',R2(2));
fprintf('h     %8.4f\n',R2(3));
fprintf('j     %8.4f\n',R2(4));
fprintf('d     %8.4f\n',R2(5));
fprintf('f     %8.4f\n',R2(6));
fprintf('xs1   %8.4f\n',R2(7));
fprintf('xs2   %8.4f\n',R2(8));
fprintf('xr    %8.4f\n',R2(9));
fprintf('b     %8.4f\n',R2(10));
fprintf('g     %8.4f\n',R2(11));
fprintf('zdv   %8.4f\n',R2(12));
fprintf('ydv   %8.4f\n',R2(13));
fprintf('Nai   %8.4f\n',R2(14));
fprintf('Nabm  %8.4f\n',R2(15));
fprintf('Ki    %8.4f\n',R2(16));
fprintf('Kibm  %8.4f\n',R2(17));
fprintf('Cai   %8.4f\n',R2(18));
fprintf('Cabm  %8.4f\n',R2(19));
fprintf('jsr   %8.4f\n',R2(20));
fprintf('nsr   %8.4f\n',R2(21));
end


function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
if(Stot>1.0e-2)
   R2 = 1-Serr/Stot;
else
    R2=1.0;
end
end