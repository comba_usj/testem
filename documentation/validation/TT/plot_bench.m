function plot_bench()
clear;
clc;

endoC=load('TT_ENDO_var.dat');
midC =load('TT_MID_var.dat');
epiC =load('TT_EPI_var.dat');

endoE=load('TT_endo_elvira_stat.dat');
midE =load('TT_mid_elvira_stat.dat');
epiE =load('TT_epi_elvira_stat.dat');

plot(endoC(:,1),endoC(:,2),'r'); hold
plot(endoE(:,end)-endoE(1,end),endoE(:,end-1),'b');

plot(midC(:,1),midC(:,2),'c');
plot(midE(:,end)-midE(1,end),midE(:,end-1),'k');

plot(epiC(:,1),epiC(:,2),'g');
plot(epiE(:,end)-epiE(1,end),epiE(:,end-1),'m');

legend('endo C','endo Elvira','mid C','mid Elvira','epi C','epi Elvira');
xlabel('t, ms');
ylabel('V, mV');

nvar = length(endoE(1,:));
R2 = zeros(nvar,3);
R2(1,1) = R2calc(endoC(:,2),endoE(:,end-1));
R2(1,2) = R2calc(midC(:,2),midE(:,end-1));
R2(1,3) = R2calc(epiC(:,2),epiE(:,end-1));
for i=3:nvar
    R2(i-1,1)=R2calc(endoC(:,i),endoE(:,i-2));
    R2(i-1,2)=R2calc(midC(:,i),midE(:,i-2));
    R2(i-1,3)=R2calc(epiC(:,i),epiE(:,i-2));
end
fprintf('Fitting (R^2 value):\n');
fprintf('       ENDO      MID     EPI\n');
fprintf('V     %8.4f %8.4f %8.4f\n',R2(1,1),R2(1,2),R2(1,3));
fprintf('Cai   %8.4f %8.4f %8.4f\n',R2(2,1),R2(2,2),R2(2,3));
fprintf('CaSR  %8.4f %8.4f %8.4f\n',R2(3,1),R2(3,2),R2(3,3));
fprintf('CaSS  %8.4f %8.4f %8.4f\n',R2(4,1),R2(4,2),R2(4,3));
fprintf('Nai   %8.4f %8.4f %8.4f\n',R2(5,1),R2(5,2),R2(5,3));
fprintf('Ki    %8.4f %8.4f %8.4f\n',R2(6,1),R2(6,2),R2(6,3));
fprintf('m     %8.4f %8.4f %8.4f\n',R2(7,1),R2(7,2),R2(7,3));
fprintf('h     %8.4f %8.4f %8.4f\n',R2(8,1),R2(8,2),R2(8,3));
fprintf('j     %8.4f %8.4f %8.4f\n',R2(9,1),R2(9,2),R2(9,3));
fprintf('xs    %8.4f %8.4f %8.4f\n',R2(10,1),R2(10,2),R2(10,3));
fprintf('r     %8.4f %8.4f %8.4f\n',R2(11,1),R2(11,2),R2(11,3));
fprintf('s     %8.4f %8.4f %8.4f\n',R2(12,1),R2(12,2),R2(12,3));
fprintf('d     %8.4f %8.4f %8.4f\n',R2(13,1),R2(13,2),R2(13,3));
fprintf('f     %8.4f %8.4f %8.4f\n',R2(14,1),R2(14,2),R2(14,3));
fprintf('f2    %8.4f %8.4f %8.4f\n',R2(15,1),R2(15,2),R2(15,3));
fprintf('fcass %8.4f %8.4f %8.4f\n',R2(16,1),R2(16,2),R2(16,3));
fprintf('rr    %8.4f %8.4f %8.4f\n',R2(17,1),R2(17,2),R2(17,3));
fprintf('oo    %8.4f %8.4f %8.4f\n',R2(18,1),R2(18,2),R2(18,3));
fprintf('xr1   %8.4f %8.4f %8.4f\n',R2(19,1),R2(19,2),R2(19,3));
fprintf('xr2   %8.4f %8.4f %8.4f\n',R2(20,1),R2(10,3),R2(20,3));
end

function R2=R2calc(xobs,xpred)


meanX = mean(xobs);
Stot= sum((xobs-meanX).^2);
Serr= sum((xobs-xpred).^2);
if(Stot>1.0e-2)
   R2 = 1-Serr/Stot;
else
    R2=1.0;
end
end
