! ------------------------------------------------------------------------------
module mod_precision
! ------------------------------------------------------------------------------
! --------------------- Working precision for MYDAS ----------------------------
!           result = select_real_kind(p,r)
!  p is the number of mantissa digits
!  r is the number of exponent range
! ------------------------------------------------------------------------------
  implicit none
! ---------- integer kinds -----------------------------------------------------
  integer,private, parameter :: int1 = selected_int_kind( 2)
  integer,private, parameter :: int2 = selected_int_kind( 4)
  integer,private, parameter :: int4 = selected_int_kind( 8)
  integer,private, parameter :: int8 = selected_int_kind(18)
! ---------- real kinds --------------------------------------------------------
  integer,public, parameter  :: rs_p = selected_real_kind( 6,  30)
  integer,public, parameter  :: rd_p = selected_real_kind(15, 300)
  integer,public, parameter  :: rq_p = selected_real_kind(30,4500)
! ---------- complex kinds -----------------------------------------------------
  integer,private, parameter :: cs_p = rs_p
  integer,private, parameter :: cd_p = rd_p
  integer,private, parameter :: cq_p = rq_p
! ------------------------------------------------------------------------------  
  integer,public, parameter  :: ip = int4
  integer,public, parameter  :: rp = rd_p
  integer,public, parameter  :: cp = cd_p
! ------------------------------------------------------------------------------
end module mod_precision
! ------------------------------------------------------------------------------

