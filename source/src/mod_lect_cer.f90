!  -----------------------------------------------------------------------------
module mod_readcir
!  -----------------------------------------------------------------------------
  use mod_precision;
!  -----------------------------------------------------------------------------
  implicit none
  interface read_cir
    module procedure read_c
    module procedure read_i
    module procedure read_r
  end interface
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine read_c(lu_in,str,val)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_in
  character (len=*),intent(in)  :: str
  character (len=*),intent(out) :: val
  integer(ip)                   :: io_err
 

  if (search(lu_in,str)) then
    read (lu_in,'(A80)',iostat=io_err) val
  else
    write (*,'(A,A)') '###.Error en subroutine read_c, str:', str
  end if
  return
end subroutine read_c
!  -----------------------------------------------------------------------------
subroutine read_i(lu_in,str,val)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_in
  character (len=*),intent(in)  :: str
  integer(ip),      intent(out) :: val
 

  if (search(lu_in,str)) then
    read (lu_in,*) val
  else
    write (*,'(A,A)') '###.Error en subroutine read_i, str:',str
  end if
  return
end subroutine read_i
!  -----------------------------------------------------------------------------
subroutine read_r(lu_in,str,val)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_in
  character (len=*),intent(in)  :: str
  real(rp),        intent(out)  :: val
 

  if (search(lu_in,str)) then
    read (lu_in,*) val
  else
    write (*,'(A,A)') '###.Error en subroutine read_r, str:',str
  end if
  return
end subroutine read_r
!  -----------------------------------------------------------------------------
function search (lu,str)
!  -----------------------------------------------------------------------------
!   busca str en la unidad lu y devuelve .true.// si lo encuentra. si no
! lo encuentra devuelve un .false.
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)       :: lu
  character (len=*),intent(in) :: str
  logical                      :: search
  !.
  integer(ip)                  :: l1,i_r, io_err
  character (len=256)          :: rec
  logical                      :: salida


  search = .false.;       salida = .true.
  l1     = len_trim(str); i_r    = 0
  !.
  do while (salida)
    read (lu, '(a80)',iostat = io_err) rec
    if (rec(1:l1) == str) then
      salida = .false.
      search = .true.
    end if
    !.
    if (io_err > 0) then
      write (*,'(A,A)') '###.Error en function search, str:',str
    else if (io_err == -1) then
      if (i_r .eq. 0) then
        rewind (lu);i_r = 1 !.rebobina el archivo
      else
        salida  = .false.
      endif
    end if
  enddo
  return
end function search
!  -----------------------------------------------------------------------------
subroutine srch_lec (lu,str,strout,search,io_err)
!  -----------------------------------------------------------------------------
!   Busca str en la unidad lu y devuelve .true.// si lo encuentra. si no
! lo encuentra devuelve un .false.
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu
  character (len=*), intent(in) :: str
  character (len=*), intent(out):: strout
  logical, intent(out)          :: search
  integer(ip), intent(out)      :: io_err
  !.
  character (len=256)           :: rec
  integer(ip)                   :: l1,l2
  logical                       :: salida

  search = .false.;       salida = .true.
  l1     = len_trim(str); l2     = len(strout)
  !.
  do while (salida)
    read (lu, '(A80)',iostat=io_err) rec
    if (rec(1:l1) == str) then
      strout =  rec(1:l2)
      salida = .false.
      search = .true.
    end if
    !.
    if (io_err > 0) then
      write (*,'(A,A)') '###.Error en subroutine srch_lec, str:',str
    elseif (io_err == -1) then
      salida  = .false.
      strout='#END'
    end if
  enddo
  return
end subroutine srch_lec
!  -----------------------------------------------------------------------------
subroutine srch_lec02 (lu, str1, str2, strout, search, io_err)
!  -----------------------------------------------------------------------------
!   Busca str en la unidad lu y devuelve .true.// si lo encuentra. si no
! lo encuentra devuelve un .false.
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu
  character (len=*), intent(in) :: str1, str2
  character (len=*), intent(out):: strout
  logical, intent(out)          :: search
  integer(ip), intent(out)      :: io_err
  !.
  character (len=256)           :: rec
  integer(ip)                   :: l1, l2, l3
  logical                       :: salida

  search = .false.;       salida = .true.
  l1     = len_trim(str1)
  l2     = len_trim(str2)  
  l3     = len(strout)
  !.
  do while (salida)
    read (lu, '(A80)', iostat = io_err) rec
    if ( (rec(1:l1) == str1) .or. (rec(1:l2) == str2) ) then
      strout =  rec(1:l3)
      salida = .false.
      search = .true.
    end if
    !.
    if (io_err > 0) then
      write (*,'(A,A)') '###.Error en subroutine srch_lec02, str:',str1
    elseif (io_err == -1) then
      salida  = .false.
      strout='#END'
    end if
  enddo
  return
end subroutine srch_lec02
!  -----------------------------------------------------------------------------
end module mod_readcir
!  -----------------------------------------------------------------------------
