module mod_io

use mod_precision
use mod_Iion

implicit none
contains 

subroutine output_param_sim(tcell, Cm, nprm, dat_i, dat_r, &
                            step, init_save, outfreq) 
!
implicit none
integer(ip), intent(in)     :: tcell, nprm, dat_i(:), init_save, outfreq
real(rp), intent(in)        :: Cm, dat_r(:), step(:)
integer(ip)                 :: aux

  call PrintNumCellEM ('Solving ',tcell,tcell)
  write(*,30) 'Cm  :',1.0/Cm
  write(*,*) 'Number of param to modified : ',nprm
  if (nprm>0) then
      write(*,*) 'Mask:'
      write(*,10) 'dat_i: ', (dat_i(aux),aux=1,nprm)
      write(*,20) 'dat_r: ', (dat_r(aux),aux=1,nprm)
  endif
  write(*,*) 'Step'
  write(*,30) 'Tini: ', step(1)
  write(*,30) 'DT  : ', step(2)
  write(*,30) 'Tfin: ', step(3)
  write(*,40) 'Saving starts at increment: ',init_save
  write(*,40) 'OutputFreq: ',outfreq

10 format (A,40(2X,I3))
20 format (A,40(2X,F12.6))
30 format (A,2X,F16.4)
40 format (A,2X,I20)
end subroutine output_param_sim

subroutine output_result(lu_stat, lu_curr, time, U, v_ve, v_cr, Istm, Jion)
!
implicit none
integer(ip), intent(in)   :: lu_stat, lu_curr
real(rp), intent(in)   :: time, U, v_ve(:), v_cr(:), Jion, Istm

   if(size(v_ve)>0) then
       write(lu_stat,50) v_ve, U, time
   else
       write(lu_stat,50) U, time
   endif

   if(size(v_cr)>0) then
       write(lu_curr,50) v_cr, Jion, Istm, time
   else
       write(lu_curr,50) Jion, time
   endif

50 format (100(E18.10,2X))
end subroutine output_result
! ------------------------------------------------------------------------------
subroutine PrintHeader_state (lu_st, t_cel)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_st, t_cel

  select case(t_cel)
  case(IZ_mod)    !.Ischemic Zone  Model
  case(RM_mod)    !.Roger and McCulloc Model
     call write_state_RM(lu_st) 
  case(TT_ENDO_mod)   !.Ten Tusscher Model
     call write_state_TT(lu_st) 
  case(TT_MID_mod)   !.Ten Tusscher Model
     call write_state_TT(lu_st) 
  case(TT_EPI_mod)   !.Ten Tusscher Model
     call write_state_TT(lu_st) 
  case(LR_ENDO_mod)   !.Luo Rudy Model
     call write_state_LR(lu_st) 
  case(LR_MID_mod)   !.Luo Rudy Model
     call write_state_LR(lu_st) 
  case(LR_EPI_mod)   !.Luo Rudy Model
     call write_state_LR(lu_st) 
  case(BO_ENDO_mod)   !.Bueno-Orovio Model
     call write_state_BO(lu_st) 
  case(BO_MID_mod)   !.Bueno-Orovio Model
     call write_state_BO(lu_st) 
  case(BO_EPI_mod)   !.Bueno-Orovio Model
     call write_state_BO(lu_st) 
  case(NYGREN_mod)    !.Nygren Model
     call write_state_NYGREN(lu_st) 
  case(ST_PK_mod)    !.Purkinje Stewart Model
     call write_state_ST_PK(lu_st) 
  case(GR_ENDO_mod) !.Grandi Model
     call write_state_GR(lu_st) 
  case(GR_EPI_mod) !.Grandi Model
     call write_state_GR(lu_st) 
  case(CRR_ENDO_mod) !.Carro Model
     call write_state_CRR(lu_st) 
  case(CRR_EPI_mod) !.Carro Model
     call write_state_CRR(lu_st) 
  case(CRR2_ENDO_mod) !.Carro2 Model
     call write_state_CRR2(lu_st) 
  case(CRR2_EPI_mod) !.Carro2 Model
     call write_state_CRR2(lu_st) 
  case(GRHF_ENDO_mod) !.Grandi Model Heart Failure
     call write_state_GRHF(lu_st) 
  case(GRHF_EPI_mod) !.Grandi Model Heart Failure
     call write_state_GRHF(lu_st) 
  case(FIBRO_mod)    !.Fibroblast Model (MacCannell)
     call write_state_FIB(lu_st) 
  case(MALEC_mod)    !.Maleckar Model
     call write_state_MALEC(lu_st) 
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
     call write_state_MALEC(lu_st) 
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
     call write_state_MALEC(lu_st) 
  case(MALEC_RAA_mod)! Maleckar atria model RAA
     call write_state_MALEC(lu_st) 
  case(MALEC_TVR_mod)! Maleckar atria model TVR
     call write_state_MALEC(lu_st) 
  case(MALEC_LA_mod)! Maleckar atria model LA
     call write_state_MALEC(lu_st) 
  case(MALEC_PV_mod)! Maleckar atria model PV
     call write_state_MALEC(lu_st) 
  case(MALEC_LAA_mod)! Maleckar atria model LAA
     call write_state_MALEC(lu_st) 
  case(MALEC_MVR_mod)! Maleckar atria model MVR
     call write_state_MALEC(lu_st) 
  case(OH_ENDO_mod)  !.Ohara Model
     call write_state_OH(lu_st) 
  case(OH_MID_mod)   !.Ohara Model
     call write_state_OH(lu_st) 
  case(OH_EPI_mod)   !.Ohara Model
     call write_state_OH(lu_st) 
  case(Courte_mod)    ! Courtemanche model EPI
  case(OH_NA_ENDO_mod)  !.OharaNa Model
     call write_state_OHNA(lu_st) 
  case(OH_NA_MID_mod)   !.OharaNa Model
     call write_state_OHNA(lu_st) 
  case(OH_NA_EPI_mod)   !.OharaNa Model
     call write_state_OHNA(lu_st) 
    call write_state_Courte (lu_st)
  case(Courte_mod_RA_PM)    ! Courtemanche model RA/PM
    call write_state_Courte (lu_st)
  case(Courte_mod_CT_BB)    ! Courtemanche model CT/BB
    call write_state_Courte (lu_st)
  case(Courte_mod_MVR)    ! Courtemanche model MVR
    call write_state_Courte (lu_st)
  case(Courte_mod_RAA)    ! Courtemanche model RAA
    call write_state_Courte (lu_st)
  case(Courte_mod_LA)    ! Courtemanche model LA
    call write_state_Courte (lu_st)
  case(Courte_mod_LAA)    ! Courtemanche model LAA
    call write_state_Courte (lu_st)
  case(Courte_mod_PV)    ! Courtemanche model PV
    call write_state_Courte (lu_st)
  case(Courte_mod_TVR)    ! Courtemanche model TVR
    call write_state_Courte (lu_st)
  case default
    write(*,*)  '---Unknow cell type:                ', t_cel
  end select
 
  write(lu_st,'(A)',advance='no') 'V time' 
  write(lu_st,*)
  return
end subroutine printHeader_state

! ------------------------------------------------------------------------------
subroutine PrintHeader_current (lu_cur, t_cel)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_cur, t_cel

  select case(t_cel)
  case(IZ_mod)    !.Ischemic Zone  Model
     call write_current_IZ(lu_cur) 
  case(RM_mod)    !.Roger and McCulloc Model
     call write_current_RM(lu_cur) 
  case(TT_ENDO_mod)   !.Ten Tusscher Model
     call write_current_TT(lu_cur) 
  case(TT_MID_mod)   !.Ten Tusscher Model
     call write_current_TT(lu_cur) 
  case(TT_EPI_mod)   !.Ten Tusscher Model
     call write_current_TT(lu_cur) 
  case(LR_ENDO_mod)   !.Luo Rudy Model
     call write_current_LR(lu_cur) 
  case(LR_MID_mod)   !.Luo Rudy Model
     call write_current_LR(lu_cur) 
  case(LR_EPI_mod)   !.Luo Rudy Model
     call write_current_LR(lu_cur) 
  case(BO_ENDO_mod)   !.Bueno-Orovio Model
     call write_current_BO(lu_cur) 
  case(BO_MID_mod)   !.Bueno-Orovio Model
     call write_current_BO(lu_cur) 
  case(BO_EPI_mod)   !.Bueno-Orovio Model
     call write_current_BO(lu_cur) 
  case(NYGREN_mod)    !.Nygren Model
     call write_current_NYGREN(lu_cur) 
  case(ST_PK_mod)    !.Purkinje Stewart Model
     call write_current_ST_PK(lu_cur) 
  case(GR_ENDO_mod) !.Grandi Model
     call write_current_GR(lu_cur) 
  case(GR_EPI_mod) !.Grandi Model
     call write_current_GR(lu_cur) 
  case(CRR_ENDO_mod) !.Carro Model
     call write_current_CRR(lu_cur) 
  case(CRR_EPI_mod) !.Carro Model
     call write_current_CRR(lu_cur) 
  case(CRR2_ENDO_mod) !.Carro2 Model
     call write_current_CRR2(lu_cur) 
  case(CRR2_EPI_mod) !.Carro2 Model
     call write_current_CRR2(lu_cur) 
  case(GRHF_ENDO_mod) !.Grandi Model Heart Failure
     call write_current_GRHF(lu_cur) 
  case(GRHF_EPI_mod) !.Grandi Model Heart Failure
     call write_current_GRHF(lu_cur) 
  case(FIBRO_mod)    !.Fibroblast Model (MacCannell)
     call write_current_FIB(lu_cur) 
  case(MALEC_mod)    !.Maleckar Model
     call write_current_MALEC(lu_cur) 
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
     call write_current_MALEC(lu_cur) 
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
     call write_current_MALEC(lu_cur) 
  case(MALEC_RAA_mod)! Maleckar atria model RAA
     call write_current_MALEC(lu_cur) 
  case(MALEC_TVR_mod)! Maleckar atria model TVR
     call write_current_MALEC(lu_cur) 
  case(MALEC_LA_mod)! Maleckar atria model LA
     call write_current_MALEC(lu_cur) 
  case(MALEC_PV_mod)! Maleckar atria model PV
     call write_current_MALEC(lu_cur) 
  case(MALEC_LAA_mod)! Maleckar atria model LAA
     call write_current_MALEC(lu_cur) 
  case(MALEC_MVR_mod)! Maleckar atria model MVR
     call write_current_MALEC(lu_cur) 
  case(OH_ENDO_mod)  !.Ohara Model
     call write_current_OH(lu_cur) 
  case(OH_MID_mod)   !.Ohara Model
     call write_current_OH(lu_cur) 
  case(OH_EPI_mod)   !.Ohara Model
     call write_current_OH(lu_cur) 
  case(OH_NA_ENDO_mod)  !.OharaNa Model
     call write_current_OHNA(lu_cur) 
  case(OH_NA_MID_mod)   !.OharaNa Model
     call write_current_OHNA(lu_cur) 
  case(OH_NA_EPI_mod)   !.OharaNa Model
     call write_current_OHNA(lu_cur) 
  case(Courte_mod)    ! Courtemanche model EPI
    call write_current_Courte (lu_cur)
  case(Courte_mod_RA_PM)    ! Courtemanche model RA/PM
    call write_current_Courte (lu_cur)
  case(Courte_mod_CT_BB)    ! Courtemanche model CT/BB
    call write_current_Courte (lu_cur)
  case(Courte_mod_MVR)    ! Courtemanche model MVR
    call write_current_Courte (lu_cur)
  case(Courte_mod_RAA)    ! Courtemanche model RAA
    call write_current_Courte (lu_cur)
  case(Courte_mod_LA)    ! Courtemanche model LA
    call write_current_Courte (lu_cur)
  case(Courte_mod_LAA)    ! Courtemanche model LAA
    call write_current_Courte (lu_cur)
  case(Courte_mod_PV)    ! Courtemanche model PV
    call write_current_Courte (lu_cur)
  case(Courte_mod_TVR)    ! Courtemanche model TVR
    call write_current_Courte (lu_cur)
  case default
    write(*,*) '---Unknow cell type:                ', t_cel
  end select
 
  write(lu_cur,'(A)',advance='no') 'Jion Istim time' 
  write(lu_cur,*) 
  return
end subroutine printHeader_current

end module mod_io
