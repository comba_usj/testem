! -------------------------------------------------------------------------------
program test_main
! -------------------------------------------------------------------------------
  use mod_precision
  use mod_leconf
  use mod_UnitUtil
  use mod_Iion
  use mod_io
  !.
  implicit none

  type(t_cnf)               :: cnf
  character(len=256)        :: outname
  integer(ip), parameter    :: mxprm=100
  integer(ip)               :: lu_curr,lu_stat,iteration
  integer(ip)               :: i, ict, outfreq, init_save
  integer(ip)               :: aux, nstat, ncurr, nprm, nprm_mod
  real(rp)                  :: step(3), Istm, U, time, Jion, invCm
  real(rp)                  :: t1,t2,v_prm(mxprm), dUdt
  real(rp), allocatable     :: v_ve(:)
  real(rp), allocatable     :: v_cr(:)
  real(rp), allocatable     :: dat_r(:)
  integer(ip), allocatable  :: dat_i(:)

  !.

  call random_seed
  write(*,*) 'Begining reading ...'
  call readconfile(cnf)
  write(*,*) 'Done reading ...'
  ict = get_celltype(cnf)               ! Retrieve cell type
  invCm = get_cellcapacitance(cnf)      ! Retrieve cell capacitance
  invCm = 1.0/invCm
  nprm_mod = get_number_of_prm(cnf%prm) ! Retrieve mask
  if (nprm_mod>0) then
      allocate(dat_r(nprm_mod))
      allocate(dat_i(nprm_mod))
      call get_mask(cnf%prm, dat_i, dat_r)
  endif
  call get_step(cnf,step)   ! Retrieve simulation time and increment
  outfreq =  get_post_freq(cnf) 
  init_save= get_init_save(cnf) 
  ! Print simulation parameters
  call output_param_sim(ict,invCm,nprm_mod,dat_i,dat_r, &
                        step,init_save,outfreq) 
  call fun_getarg(3,'-o',outname) !.Retrieve root name for outputfile
  aux=len_trim(outname)
  call a_unit(lu_curr) 
  call a_unit(lu_stat) 
  open(lu_curr,FILE=outname(1:aux)//'_curr.dat');
  open(lu_stat,FILE=outname(1:aux)//'_stat.dat');
  call PrintHeader_state (lu_stat, ict)
  call PrintHeader_current (lu_curr, ict)

  iteration =0;
  time = step(1);
  ! Initialicing state variables and parameters
  nstat = get_numstatevar(ict)
  allocate(v_ve(nstat))
  v_ve(1:nstat)=get_ic_em(ict,nstat)
  ncurr = get_numcur(ict)
  allocate(v_cr(ncurr))
  call get_parameter_nd(ict,v_prm,nprm);
  if(nprm_mod>0) then
      v_prm(dat_i(1:nprm_mod))=dat_r(1:nprm_mod)
  endif
  U = initcond_elecmodel (ict) 
  dUdt=0.0
  call get_time(t1)
  !
!  write(*,*) nprm, nstat, ncurr
!  write(*,*) (v_prm(i),i=1,nprm)
  do while (time < step(3))
    Istm = get_stimulus(cnf%stm, time, step(3))
!
    if(iteration.ge.init_save) then
      if(mod(iteration,outfreq) == 0) then
         call output_result(lu_stat, lu_curr, time, U, v_ve, &
                            v_cr, Istm, Jion)
      end if
    endif
!
    call get_Iion (ict,step(2),Istm,0.0_rp,U,dUdt,Jion, &
                   v_prm(1:nprm),v_ve, v_cr)
    U = U - step(2) * invCm * (Jion+Istm);
    dUdt = -invCm * (Jion+Istm)
    time = time + step(2);
    iteration = iteration + 1;
!
  enddo
  call get_time(t2)
  write(*,60) 'Elapsed time:',t2-t1,'s'
  close(lu_curr)
  close(lu_stat)
  !
  deallocate(v_cr)
  deallocate(v_ve)
  if(nprm_mod>0) then
    deallocate(dat_i)
    deallocate(dat_r)
  end if

60 format (A,2X,F16.3,2X,A)
end program test_main


