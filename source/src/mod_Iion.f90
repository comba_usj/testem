! Test-EM
module mod_Iion
! -------------------------------------------------------------------------------
  use mod_precision
!  use mod_error
  use mod_carro
  use mod_carro2
  use mod_courtemanche
  use mod_FK
  use mod_grandi
  use mod_grandiHF
  use mod_IZ
  use mod_lr2k
  use mod_MacCannell
  use mod_maleckar
  use mod_nygren
  use mod_ohara
  use mod_oharana
  use mod_PKFstewart
  use mod_rm
  use mod_tentusscher
  implicit none

contains
! -------------------------------------------------------------------------------
subroutine get_Iion (t_cel, dt, Istm, Iax, Vo, Vp, Iion, p_rm, v_ve,v_cr)
! -------------------------------------------------------------------------------
! t_cell: cell type. Not all models required this parameter
! dt    : time increment
! Istm  : Stimulation current
! Iax   : Axial current
! Vo    : Potential
! Vp    : Potential partial time derivative
! Iion  : Ionic current
! p_rm  : vector with modifiable model parameters
! v_ve  : vector with electric model state variables
! v_cr  : vector with model currents
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: t_cel
  real(rp),    intent(in)    :: dt, Istm, Iax, Vo, Vp
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: p_rm(:)
  real(rp),    intent(inout) :: v_ve(:)
  real(rp),    intent(out)   :: v_cr(:)


  select case(t_cel)
  case(IZ_mod)        !.Ischemic Zone Model
    call IZ_A_P01 (Vo, dt, Iion, p_rm)
  case(RM_mod)        !.Roger and McCulloc Model
    call RMC_A_P01 (Vo, dt, v_ve(1), Iion, p_rm)
  case(TT_ENDO_mod)   !.Ten Tusscher Model ENDO
    call TenTusscher_A_P01 (t_cel,dt,Istm,Iax,Vo,Iion,p_rm, v_ve, v_cr)
  case(TT_MID_mod)    !.Ten Tusscher Model MID
    call TenTusscher_A_P01 (t_cel,dt,Istm,Iax,Vo,Iion,p_rm, v_ve, v_cr)
  case(TT_EPI_mod)    !.Ten Tusscher Model EPI
    call TenTusscher_A_P01 (t_cel,dt,Istm,Iax,Vo,Iion,p_rm, v_ve, v_cr)
  case(LR_ENDO_mod)   !.Luo Rudy Model ENDO
    call LR2K_A_P01    (t_cel, dt, Vp, Vo, Iion, p_rm, v_ve, v_cr)
  case(LR_MID_mod)    !.Luo Rudy Model MID
    call LR2K_A_P01    (t_cel, dt, Vp, Vo, Iion, p_rm, v_ve, v_cr)
  case(LR_EPI_mod)    !.Luo Rudy Model EPI
    call LR2K_A_P01    (t_cel, dt, Vp, Vo, Iion, p_rm, v_ve, v_cr)
  case(BO_ENDO_mod)   !.Bueno Orovio Model ENDO
    call FK_A_P01     (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(BO_MID_mod)    !.Bueno Orovio Model MID
    call FK_A_P01     (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(BO_EPI_mod)    !.Bueno Orovio Model EPI
    call FK_A_P01     (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(NYGREN_mod)    !.Nygren atria Model
    call Nygren_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(ST_PK_mod)     !.Steward_PKF Model
    call PKFstewart_A_P01 (t_cel, dt, Istm, Vo, Iion, p_rm, v_ve, v_cr)
  case(GR_ENDO_mod)   !.Grandi Model ENDO
    call Grandi_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(GR_EPI_mod)    !.Grandi Model EPI
    call Grandi_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(CRR_ENDO_mod)   !.Carro Model ENDO
    call Carro_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(CRR_EPI_mod)    !.Carro Model EPI
    call Carro_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(CRR2_ENDO_mod)   !.Carro2 Model ENDO
    call Carro2_A_P01 (t_cel, dt, Istm, Vo, Iion, p_rm, v_ve, v_cr)
  case(CRR2_EPI_mod)    !.Carro2 Model EPI
    call Carro2_A_P01 (t_cel, dt, Istm, Vo, Iion, p_rm, v_ve, v_cr)
  case(GRHF_ENDO_mod)  !.Grandi Heart Failure ENDO Model
    call GrandiHF_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(GRHF_EPI_mod)   !.Grandi Heart Failure EPI Model
    call GrandiHF_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(FIBRO_mod)      ! Fibroblas model (MacCannell)
    call MacCannell_A_P01 (t_cel, dt, Vo, Iion, p_rm, v_ve, v_cr)
  case(MALEC_mod)      ! Maleckar atria model
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_LA_mod)! Maleckar atria model LA
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_PV_mod)! Maleckar atria model PV
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    call Maleckar_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(OH_ENDO_mod)    ! Ohara model ENDO
    call Ohara_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(OH_MID_mod)    ! Ohara model MID
    call Ohara_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(OH_EPI_mod)    ! Ohara model EPI
    call Ohara_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(OH_NA_ENDO_mod)    ! OharaNa model ENDO
    call OharaNa_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(OH_NA_MID_mod)    ! OharaNa model MID
    call OharaNa_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(OH_NA_EPI_mod)    ! OharaNa model EPI
    call OharaNa_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod)    ! Courtemanche model EPI
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_RA_PM)    ! Courtemanche model RA/PM
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_CT_BB)    ! Courtemanche model CT/BB
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_MVR)    ! Courtemanche model MVR
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_RAA)    ! Courtemanche model RAA
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_LA)    ! Courtemanche model LA
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_LAA)    ! Courtemanche model LAA
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_PV)    ! Courtemanche model PV
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case(Courte_mod_TVR)    ! Courtemanche model TVR
    call Courtemanche_A_P01 (t_cel, dt, Vo, Istm, Iion, p_rm, v_ve, v_cr)
  case default
  end select
  !.
  return
end subroutine Get_Iion
! -------------------------------------------------------------------------------
function get_numstatevar (t_cel)  result(nvar)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: t_cel
  integer(ip)                :: nvar

  select case(t_cel)
  case(IZ_mod)        !.Ischemic Zone Model
    nvar = nvar_iz    !1
  case(RM_mod)        !.Roger and McCulloc Model
    nvar = nvar_rmc   !1
  case(TT_ENDO_mod)   !.Ten Tusscher Model
    nvar = nvar_tt    !19
  case(TT_MID_mod)    !.Ten Tusscher Model
    nvar = nvar_tt    !19
  case(TT_EPI_mod)    !.Ten Tusscher Model
    nvar = nvar_tt    !19
  case(LR_ENDO_mod)   !.Luo Rudy Model
    nvar = nvar_lr    !27
  case(LR_MID_mod)    !.Luo Rudy Model
    nvar = nvar_lr    !27
  case(LR_EPI_mod)    !.Luo Rudy Model
    nvar = nvar_lr    !27
  case(BO_ENDO_mod)   !.Bueno Orovio Model
    nvar = nvar_fk    !3
  case(BO_MID_mod)    !.Bueno Orovio Model
    nvar = nvar_fk    !3
  case(BO_EPI_mod)    !.Bueno Orovio Model
    nvar = nvar_fk    !3
  case(NYGREN_mod)    !.Nygren Model
    nvar = nvar_ny    !35
  case(ST_PK_mod)     !.Purkinje Stewart Model
    nvar = nvar_pkf   !20
  case(GR_ENDO_mod)   !.Grandi Model
    nvar = nvar_gr    !38
  case(GR_EPI_mod)    !.Grandi Model
    nvar = nvar_gr    !38
  case(CRR_ENDO_mod)  !.Carro Model
    nvar = nvar_crr   !39
  case(CRR_EPI_mod)   !.Carro Model
    nvar = nvar_crr   !39
  case(CRR2_ENDO_mod) !.Carro2 Model
    nvar = nvar_crr2  !39
  case(CRR2_EPI_mod)  !.Carro2 Model
    nvar = nvar_crr2  !39
  case(GRHF_ENDO_mod) !.Grandi Heart Failure Model
    nvar = nvar_grHF  !38
  case(GRHF_EPI_mod)  !.Grandi Heart Failure Model
    nvar = nvar_grHF  !38
  case(FIBRO_mod)     !.Fibroblast Model
    nvar = nvar_Mac   !2
  case(MALEC_mod)     !.Maleckar Model
    nvar = nvar_maleck!28
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    nvar = nvar_maleck!28
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    nvar = nvar_maleck!28
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    nvar = nvar_maleck!28
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    nvar = nvar_maleck!28
  case(MALEC_LA_mod)! Maleckar atria model LA
    nvar = nvar_maleck!28
  case(MALEC_PV_mod)! Maleckar atria model PV
    nvar = nvar_maleck!28
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    nvar = nvar_maleck!28
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    nvar = nvar_maleck!28
  case(OH_ENDO_mod)   !.Ohara Model 
    nvar = nvar_ohr   !40
  case(OH_MID_mod)    !.Ohara Model
    nvar = nvar_ohr   !40
  case(OH_EPI_mod)    !.Ohara Model
    nvar = nvar_ohr   !40
  case(OH_NA_ENDO_mod)   !.OharaNa Model 
    nvar = nvar_ohrna   !37
  case(OH_NA_MID_mod)    !.OharaNa Model
    nvar = nvar_ohrna   !37
  case(OH_NA_EPI_mod)    !.OharaNa Model
    nvar = nvar_ohrna   !37
  case(Courte_mod)    !.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_RA_PM)!.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_CT_BB)!.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_RAA)  !.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_TVR)  !.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_LA)   !.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_LAA)  !.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_PV)   !.Courtemanche Model
    nvar = nvar_courte!20
  case(Courte_mod_MVR)  !.Courtemanche Model
    nvar = nvar_courte!20
  case default
    nvar =  0 
  end select
  !.
  return
end function get_numstatevar
! -------------------------------------------------------------------------------
function get_numcur (t_cel)  result(ncur)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: t_cel
  integer(ip)                :: ncur

  select case(t_cel)
  case(IZ_mod)        !.Ischemic Zone Model
    ncur = ncur_iz    ! 0
  case(RM_mod)        !.Roger and McCulloc Model
    ncur = ncur_rmc   ! 0
  case(TT_ENDO_mod)   !.Ten Tusscher Model
    ncur = ncur_tt    !14
  case(TT_MID_mod)    !.Ten Tusscher Model
    ncur = ncur_tt    !14
  case(TT_EPI_mod)    !.Ten Tusscher Model
    ncur = ncur_tt    !14
  case(LR_ENDO_mod)   !.Luo Rudy Model
    ncur = ncur_lr    !21
  case(LR_MID_mod)    !.Luo Rudy Model
    ncur = ncur_lr    !21
  case(LR_EPI_mod)    !.Luo Rudy Model
    ncur = ncur_lr    !21
  case(BO_ENDO_mod)   !.Fenton Karma Model
    ncur =  ncur_fk   !4
  case(BO_MID_mod)    !.Fenton Karma Model
    ncur =  ncur_fk   !4
  case(BO_EPI_mod)    !.Fenton Karma Model
    ncur =  ncur_fk   !4
  case(NYGREN_mod)    !.Nygren Model
    ncur = ncur_ny    !19
  case(ST_PK_mod)     !.Purkinje Stewart Model
    ncur = ncur_pkf   !16
  case(GR_ENDO_mod)   !.Grandi Model
    ncur = ncur_gr    !24
  case(GR_EPI_mod)    !.Grandi Model
    ncur = ncur_gr    !24
  case(CRR_ENDO_mod)  !.Carro Model
    ncur = ncur_crr   !24
  case(CRR_EPI_mod)   !.Carro Model
    ncur = ncur_crr   !24
  case(CRR2_ENDO_mod) !.Carro2 Model
    ncur = ncur_crr2  !24
  case(CRR2_EPI_mod)  !.Carro2 Model
    ncur = ncur_crr2  !24
  case(GRHF_ENDO_mod) !.Grandi Model Heart Failure
    ncur = ncur_grHF  !25
  case(GRHF_EPI_mod)  !.Grandi Model Heart Failure
    ncur = ncur_grHF  !25
  case(FIBRO_mod)     !.MacCanell Model
    ncur = ncur_Mac   !5
  case(MALEC_mod)     !.Maleckar Model
    ncur = ncur_maleck!19
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    ncur = ncur_maleck!19
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    ncur = ncur_maleck!19
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    ncur = ncur_maleck!19
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    ncur = ncur_maleck!19
  case(MALEC_LA_mod)! Maleckar atria model LA
    ncur = ncur_maleck!19
  case(MALEC_PV_mod)! Maleckar atria model PV
    ncur = ncur_maleck!19
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    ncur = ncur_maleck!19
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    ncur = ncur_maleck!19
  case(OH_ENDO_mod)   !.Ohara Model
    ncur = ncur_ohr   !17
  case(OH_MID_mod)    !.Ohara Model
    ncur = ncur_ohr   !17
  case(OH_EPI_mod)    !.Ohara Model
    ncur = ncur_ohr   !17
  case(OH_NA_ENDO_mod)   !.OharaNa Model
    ncur = ncur_ohrna   !17
  case(OH_NA_MID_mod)    !.OharaNa Model
    ncur = ncur_ohrna   !17
  case(OH_NA_EPI_mod)    !.OharaNa Model
    ncur = ncur_ohrna   !17
  case(Courte_mod)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_RA_PM)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_CT_BB)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_RAA)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_TVR)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_LA)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_LAA)   !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_PV)    !.Courtemanche Model
    ncur = ncur_courte!17
  case(Courte_mod_MVR)    !.Courtemanche Model
    ncur = ncur_courte!17
  case default
    ncur =  0 
  end select
  !.
  return
end function get_numcur
! ------------------------------------------------------------------------------
function get_ic_em(t_cel,n_var) result (v_me)
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip), intent(in)  :: t_cel, n_var
  real(rp)                 :: v_me(n_var)

  select case(t_cel)
  case(IZ_mod)        !.Ischemic Zone Model
    v_me(1:n_var) = 0.0
  case(RM_mod)        !.Roger and McCulloc Model
    v_me(1:n_var) = 0.0
  case(TT_ENDO_mod)   !.Ten Tusscher Model ENDO
    v_me(1:n_var) = ic_TenTusscher(t_cel) 
  case(TT_MID_mod)    !.Ten Tusscher Model MID
    v_me(1:n_var) = ic_TenTusscher(t_cel) 
  case(TT_EPI_mod)    !.Ten Tusscher Model EPI
    v_me(1:n_var) = ic_TenTusscher(t_cel) 
  case(LR_ENDO_mod)   !.Luo Rudy Model ENDO
    v_me(1:n_var) = ic_lr2k(t_cel) 
  case(LR_MID_mod)    !.Luo Rudy Model MID
    v_me(1:n_var) = ic_lr2k(t_cel) 
  case(LR_EPI_mod)    !.Luo Rudy Model EPI
    v_me(1:n_var) = ic_lr2k(t_cel) 
  case(BO_ENDO_mod)   !.Bueno Orovio Model ENDO
    v_me(1:n_var) = ic_FK(t_cel) 
  case(BO_MID_mod)   !.Bueno Orovio Model MID
    v_me(1:n_var) = ic_FK(t_cel) 
  case(BO_EPI_mod)   !.Bueno Orovio Model EPI
    v_me(1:n_var) = ic_FK(t_cel) 
  case(NYGREN_mod)   !.Nygren Model
    v_me(1:n_var) = ic_Nygren(t_cel)
  case(ST_PK_mod)    !.Purkinje Stewart  Model
    v_me(1:n_var) = ic_PKFstewart(t_cel)
  case(GR_ENDO_mod) !.Grandi Model ENDO
    v_me(1:n_var) = ic_Grandi(t_cel)
  case(GR_EPI_mod)  !.Grandi Model EPI
    v_me(1:n_var) = ic_Grandi(t_cel)
  case(CRR_ENDO_mod) !.Carro Model ENDO
    v_me(1:n_var) = ic_Carro(t_cel)
  case(CRR_EPI_mod)  !.Carro Model EPI
    v_me(1:n_var) = ic_Carro(t_cel)
  case(CRR2_ENDO_mod) !.Carro2 Model ENDO
    v_me(1:n_var) = ic_Carro2(t_cel)
  case(CRR2_EPI_mod)  !.Carro2 Model EPI
    v_me(1:n_var) = ic_Carro2(t_cel)
  case(GRHF_ENDO_mod) !.Grandi Model Hear Failure ENDO
    v_me(1:n_var) = ic_GrandiHF(t_cel)
  case(GRHF_EPI_mod)  !.Grandi Model Hear Failure EPI
    v_me(1:n_var) = ic_GrandiHF(t_cel)
  case(FIBRO_mod)    !.Fibroblast Model
    v_me(1:n_var) = ic_MacCannell(t_cel)
  case(MALEC_mod)     !.Maleckar Model
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_LA_mod)! Maleckar atria model LA
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_PV_mod)! Maleckar atria model PV
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    v_me(1:n_var) = ic_maleck(t_cel)
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    v_me(1:n_var) = ic_maleck(t_cel)
  case(OH_ENDO_mod)  !.Ohara Model
    v_me(1:n_var) = ic_OHara(t_cel)
  case(OH_MID_mod)   !.Ohara Model
    v_me(1:n_var) = ic_OHara(t_cel)
  case(OH_EPI_mod)   !.Ohara Model
    v_me(1:n_var) = ic_OHara(t_cel)
  case(OH_NA_ENDO_mod)  !.OharaNa Model
    v_me(1:n_var) = ic_OHaraNa(t_cel)
  case(OH_NA_MID_mod)   !.OharaNa Model
    v_me(1:n_var) = ic_OHaraNa(t_cel)
  case(OH_NA_EPI_mod)   !.OharaNa Model
    v_me(1:n_var) = ic_OHaraNa(t_cel)
  case(Courte_mod)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_RA_PM)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_CT_BB)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_RAA)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_TVR)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_LA)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_LAA)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_PV)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case(Courte_mod_MVR)   !.Courtemanche Model
    v_me(1:n_var) = ic_Courte(t_cel)
  case default
    write (*,10) t_cel ; stop
  end select
  !.
  return
10 format('###.Error en get_ic_em, t_cel = ',I6,' no implementado')
end function get_ic_em
! ------------------------------------------------------------------------------
subroutine get_parameter_nd (t_cel, p_rm, np)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: t_cel
  real(rp),    intent(out) :: p_rm(:)
  integer(ip), intent(out) :: np


  select case(t_cel)
  case(IZ_mod)    !.Ischemic Zone Model
    np= np_iz;   p_rm(1:np) = get_parameter_IZ   (t_cel) !3
  case(RM_mod)    !.Roger and McCulloc Model
    np= np_rmc;   p_rm(1:np) = get_parameter_RM   (t_cel)!6
  case(TT_ENDO_mod)   !.Ten Tusscher Model
    np= np_tt;  p_rm(1:np) = get_parameter_TT   (t_cel)  !21
  case(TT_MID_mod)   !.Ten Tusscher Model
    np= np_tt;  p_rm(1:np) = get_parameter_TT   (t_cel)  !21
  case(TT_EPI_mod)   !.Ten Tusscher Model
    np= np_tt;  p_rm(1:np) = get_parameter_TT   (t_cel)  !21
  case(LR_ENDO_mod)   !.Luo Rudy Model
    np= np_lr;  p_rm(1:np) = get_parameter_LR2K (t_cel)  !22
  case(LR_MID_mod)   !.Luo Rudy Model
    np= np_lr;  p_rm(1:np) = get_parameter_LR2K (t_cel)  !22
  case(LR_EPI_mod)   !.Luo Rudy Model
    np= np_lr;  p_rm(1:np) = get_parameter_LR2K (t_cel)  !22
  case(BO_ENDO_mod)  !.Bueno Orovio Model
    np= np_fk;  p_rm(1:np) = get_parameter_FK   (t_cel)  !30
  case(BO_MID_mod)  !.Bueno Orovio Model
    np= np_fk;  p_rm(1:np) = get_parameter_FK   (t_cel)  !30
  case(BO_EPI_mod)  !.Bueno Orovio Model
    np= np_fk;  p_rm(1:np) = get_parameter_FK   (t_cel)  !30
  case(NYGREN_mod)    !.Nygren Model
    np= np_ny;  p_rm(1:np) = get_parameter_NYG  (t_cel)  !13
  case(ST_PK_mod)    !.Purkinje Stewart Model
    np= np_pkf;  p_rm(1:np) = get_parameter_PKF  (t_cel) !19
  case(GR_ENDO_mod)  !. Grandi Model ENDO
    np= np_gr;  p_rm(1:np) = get_parameter_GR   (t_cel)  !20
  case(GR_EPI_mod)   !. Grandi Model EPI
    np= np_gr;  p_rm(1:np) = get_parameter_GR   (t_cel)  !20
  case(CRR_ENDO_mod)  !. Carro Model ENDO
    np= np_crr;  p_rm(1:np) = get_parameter_CRR (t_cel)  !14
  case(CRR_EPI_mod)   !. Carro Model EPI
    np= np_crr;  p_rm(1:np) = get_parameter_CRR (t_cel)  !15
  case(CRR2_ENDO_mod)  !. Carro2 Model ENDO
    np= np_crr2;  p_rm(1:np) = get_parameter_CRR2 (t_cel)  !114
  case(CRR2_EPI_mod)   !. Carro2 Model EPI
    np= np_crr2;  p_rm(1:np) = get_parameter_CRR2 (t_cel)  !115
  case(GRHF_ENDO_mod)!. Grandi Model Heart Failure ENDO
    np= np_grHF;  p_rm(1:np) = get_parameter_GRHF  (t_cel)!14
  case(GRHF_EPI_mod)!. Grandi Model Heart Failure EPI
    np= np_grHF;  p_rm(1:np) = get_parameter_GRHF  (t_cel)!14
  case(FIBRO_mod)   !. Fibroblast Model (MacCannell) 
    np= np_Mac;  p_rm(1:np) = get_parameter_MacCannell  (t_cel)  !4
  case(MALEC_mod)   !. Maleckar Model 
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_LA_mod)! Maleckar atria model LA
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_PV_mod)! Maleckar atria model PV
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    np= np_maleck;  p_rm(1:np) = get_parameter_MLK  (t_cel)!13
  case(OH_ENDO_mod) !. Ohara Model 
    np= np_ohr;  p_rm(1:np) = get_parameter_OHR  (t_cel)!14 
  case(OH_MID_mod)  !. Ohara Model 
    np= np_ohr;  p_rm(1:np) = get_parameter_OHR  (t_cel)!14
  case(OH_EPI_mod)  !. Ohara Model 
    np= np_ohr;  p_rm(1:np) = get_parameter_OHR  (t_cel)!14
  case(OH_NA_ENDO_mod) !. Ohara Model 
    np= np_ohrna;  p_rm(1:np) = get_parameter_OHRNA  (t_cel)!14 
  case(OH_NA_MID_mod)  !. Ohara Model 
    np= np_ohrna;  p_rm(1:np) = get_parameter_OHRNA  (t_cel)!14
  case(OH_NA_EPI_mod)  !. Ohara Model 
    np= np_ohrna;  p_rm(1:np) = get_parameter_OHRNA  (t_cel)!14
  case(Courte_mod)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_RA_PM)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_CT_BB)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_RAA)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_TVR)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_LA)   !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_LAA)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_PV)   !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case(Courte_mod_MVR)  !. Courtemanche Model 
    np= np_courte;  p_rm(1:np) = get_parameter_CRN  (t_cel)!9
  case default
    write (*,10) t_cel ; stop
  end select
  return
10 format('###.Error en get_parameter_nd, t_cel = ',I6,' no implementado')
end subroutine get_parameter_nd
! ------------------------------------------------------------------------------
function initcond_elecmodel (t_cel) result(Vi)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: t_cel
  real(rp)                :: Vi

  select case (t_cel)
  case(IZ_mod)             !.Ischemic Zone; 
      Vi = Vi_IZ 
  case(RM_mod)             !.Roger y McCulloc;
      Vi = Vi_RMC
  case(TT_ENDO_mod)        !.Ten Tusscher ENDO
      Vi = Vi_ENDO_TT  
  case(TT_MID_mod)         !.Ten Tusscher MID
      Vi = Vi_MID_TT  
  case(TT_EPI_mod)         !.Ten Tusscher EPI
      Vi = Vi_EPI_TT  
  case(LR_ENDO_mod)        !.Luo Rudy ENDO
      Vi = Vi_LR
  case(LR_MID_mod)         !.Luo Rudy MID
      Vi = Vi_LR
  case(LR_EPI_mod)         !.Luo Rudy EPI
      Vi = Vi_LR
  case(BO_ENDO_mod)        !.Fenton Karma ENDO
      Vi = Vi_FK
  case(BO_MID_mod)         !.Fenton Karma MID 
      Vi = Vi_FK
  case(BO_EPI_mod)         !.Fenton Karma EPI 
      Vi = Vi_FK
  case(NYGREN_mod)         !.Nygren
      Vi = Vi_NY
  case(ST_PK_mod)          !.Purkinje Stewart 
      Vi = Vi_PKF
  case(GR_ENDO_mod)        !.Grandi ENDO 
      Vi = Vi_GR_end
  case(GR_EPI_mod)         !.Grandi EPI
      Vi = Vi_GR_epi
  case(CRR_ENDO_mod)       !.Carro ENDO  
      Vi = Vi_CRR_end
  case(CRR_EPI_mod)        !.Carro EPI
      Vi = Vi_CRR_epi
  case(CRR2_ENDO_mod)       !.Carro2 ENDO  
      Vi = Vi_CRR2_end
  case(CRR2_EPI_mod)        !.Carro2 EPI
      Vi = Vi_CRR2_epi
  case(GRHF_ENDO_mod)      !.Grandi Heart Failure ENDO
      Vi = Vi_GRHF_end
  case(GRHF_EPI_mod)       !.Grandi Heart Failure EPI
      Vi = Vi_GRHF_epi
  case(FIBRO_mod)          !.Fibroblast (MacCannell)
      Vi = Vi_Fib   
  case(MALEC_mod)          !.Maleckar 
      Vi = Vi_Maleck
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
      Vi = Vi_Maleck_RA_PM
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
      Vi = Vi_Maleck_CT_BB
  case(MALEC_RAA_mod)! Maleckar atria model RAA
      Vi = Vi_Maleck_RAA
  case(MALEC_TVR_mod)! Maleckar atria model TVR
      Vi = Vi_Maleck_TVR
  case(MALEC_LA_mod)! Maleckar atria model LA
      Vi = Vi_Maleck_LA
  case(MALEC_PV_mod)! Maleckar atria model PV
      Vi = Vi_Maleck_PV
  case(MALEC_LAA_mod)! Maleckar atria model LAA
      Vi = Vi_Maleck_LAA
  case(MALEC_MVR_mod)! Maleckar atria model MVR
      Vi = Vi_Maleck_MVR
  case(OH_ENDO_mod)        !.Ohara ENDO
      Vi = Vi_OHR_end
  case(OH_MID_mod)         !.Ohara MID
      Vi = Vi_OHR_mid
  case(OH_EPI_mod)         !.Ohara EPI
      Vi = Vi_OHR_epi
  case(OH_NA_ENDO_mod)        !.Ohara ENDO
      Vi = Vi_OHRNA_end
  case(OH_NA_MID_mod)         !.Ohara MID
      Vi = Vi_OHRNA_mid
  case(OH_NA_EPI_mod)         !.Ohara EPI
      Vi = Vi_OHRNA_epi
  case(Courte_mod)         !.Courtemanche 
      Vi = Vi_courte
  case(Courte_mod_RA_PM)   !.Courtemanche 
      Vi = Vi_courte_RA_PM
  case(Courte_mod_CT_BB)   !.Courtemanche 
      Vi = Vi_courte_CT_BB
  case(Courte_mod_RAA)     !.Courtemanche 
      Vi = Vi_courte_RAA
  case(Courte_mod_TVR)     !.Courtemanche 
      Vi = Vi_courte_TVR  
  case(Courte_mod_LA)      !.Courtemanche 
      Vi = Vi_courte_LA
  case(Courte_mod_LAA)     !.Courtemanche 
      Vi = Vi_courte_LAA
  case(Courte_mod_PV)      !.Courtemanche 
      Vi = Vi_courte_PV
  case(Courte_mod_MVR)     !.Courtemanche 
      Vi = Vi_courte_MVR
  case default
    write (*,10) t_cel ; stop
  end select
  return
10 format('###.Error en initcond_elecmodel, t_cel = ',I6,' no implementado')
end function initcond_elecmodel
! -------------------------------------------------------------------------------
subroutine flag_opttemp (t_cel, ite,  Jion, Irel, dvdt, dt, stp, flg_c, dt_c)
! -------------------------------------------------------------------------------
! i    : numero del nodo
! ite  : numero de la iteracion
! Jion : valor de la corriente de estimulo
! dvdt : valor de la derivada temporal en el paso anterios
! dt   : paso temporal
! stp  : numero de pasos que llevo sin calcular el Iion
! flg_c: bandera de calculo, se calcula o no el Jion 
! dt_c : con que paso de tiempo se calcula el Jion
! -------------------------------------------------------------------------------
  implicit none
  !.
  integer(ip), intent(in)     :: t_cel, ite
  real(rp),    intent(in)     :: Jion, Irel, dvdt, dt
  integer(ip), intent(inout)  :: stp
  logical                :: flg_c
  real(rp), intent(out)  :: dt_c

  !.
  select case (t_cel)
  case(IZ_mod) !.Ischemic Zone;
    dt_c = dt; stp  = 1
  case(RM_mod) !.Roger y McCulloc;
    dt_c = dt; stp  = 1
  case(TT_ENDO_mod) !.Ten Tusscher ENDO
    flg_c = (abs(dvdt) >= m_dvdtTT).or.(stp == m_stpTT).or.(ite < m_iteTT).or.(Jion > 0);
  case(TT_MID_mod) !.Ten Tusscher MID
    flg_c = (abs(dvdt) >= m_dvdtTT).or.(stp == m_stpTT).or.(ite < m_iteTT).or.(Jion > 0);
  case(TT_EPI_mod) !.Ten Tusscher EPI
    flg_c = (abs(dvdt) >= m_dvdtTT).or.(stp == m_stpTT).or.(ite < m_iteTT).or.(Jion > 0);
  case(LR_ENDO_mod) !.Luo Rudy ENDO
    flg_c = (Irel > m_IrelLR).or.(abs(dvdt) >= m_dvdtLR).or.(stp == m_stpLR).or.(ite < m_iteLR)
  case(LR_MID_mod) !.Luo Rudy MID
    flg_c = (Irel > m_IrelLR).or.(abs(dvdt) >= m_dvdtLR).or.(stp == m_stpLR).or.(ite < m_iteLR)
  case(LR_EPI_mod) !.Luo Rudy EPI
    flg_c = (Irel > m_IrelLR).or.(abs(dvdt) >= m_dvdtLR).or.(stp == m_stpLR).or.(ite < m_iteLR)
  case(BO_ENDO_mod) !.Bueno Orovio ENDO
    flg_c = (abs(dvdt) >= m_dvdtFK).or.(stp == m_stpFK).or.(ite < m_iteFK).or.(Jion > 0);
  case(BO_MID_mod) !.Bueno Orovio MID
    flg_c = (abs(dvdt) >= m_dvdtFK).or.(stp == m_stpFK).or.(ite < m_iteFK).or.(Jion > 0);
  case(BO_EPI_mod) !.Bueno Orovio EPI
    flg_c = (abs(dvdt) >= m_dvdtFK).or.(stp == m_stpFK).or.(ite < m_iteFK).or.(Jion > 0);
  case(NYGREN_mod)  !.Nygren
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtNY).or.(stp == m_stpNY).or.(ite < 5).or.(Jion > 0);
  case(ST_PK_mod)  !.Stewart
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtPKF).or.(stp == m_stpPKF).or.(ite < 5).or.(Jion > 0);
  case(GR_ENDO_mod)  !.Grandi ENDO
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGR).or.(stp == m_stpGR).or.(ite < 5).or.(Jion > 0);
  case(GR_EPI_mod)  !.Grandi EPI
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGR).or.(stp == m_stpGR).or.(ite < 5).or.(Jion > 0);
  case(CRR_ENDO_mod)  !.Carro ENDO
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtCRR).or.(stp == m_stpCRR).or.(ite < 5).or.(Jion > 0);
  case(CRR_EPI_mod)  !.Carro EPI
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtCRR).or.(stp == m_stpCRR).or.(ite < 5).or.(Jion > 0);
  case(CRR2_ENDO_mod)  !.Carro2 ENDO
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtCRR2).or.(stp == m_stpCRR2).or.(ite < 5).or.(Jion > 0);
  case(CRR2_EPI_mod)  !.Carro2 EPI
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtCRR2).or.(stp == m_stpCRR).or.(ite < 5).or.(Jion > 0);
  case(GRHF_ENDO_mod)  !.Grandi Heart Failure ENDO
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(GRHF_EPI_mod)  !.Grandi Heart Failure EPI
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(FIBRO_mod)    !.Fibroblast (MacCannell)
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_mod)    !.Maleckar
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_LA_mod)! Maleckar atria model LA
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_PV_mod)! Maleckar atria model PV
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(OH_ENDO_mod)  !.Ohara
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(OH_MID_mod)   !.Ohara
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(OH_EPI_mod)   !.Ohara
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_RA_PM)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_CT_BB)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_RAA)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_TVR)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_LA)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_LAA)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_PV)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case(Courte_mod_MVR)    !.Courtemanche
    flg_c = .true.
    dt_c = dt; stp  = 1
   ! flg_c = (abs(dvdt) >= m_dvdtGRHF).or.(stp == m_stpGRHF).or.(ite < 5).or.(Jion > 0);
  case default
    write (*,10) t_cel ; stop
  end select
  !.
  if (flg_c) then
    dt_c = float(stp)*dt; stp  = 1
  else
    stp = stp + 1  !.Incremento de contador 
  end if
  return
10 format('###.Error en flag_opttemp, t_cel = ',I6,' no implementado')
end subroutine flag_opttemp
! ------------------------------------------------------------------------------
subroutine PrintNumCellEM (str,t_cel,nc)
! ------------------------------------------------------------------------------
  implicit none
  character(len=*), intent(in) :: str
  integer(ip), intent(in)      :: t_cel, nc

  select case(t_cel)
  case(IZ_mod)    !.Ischemic Zone  Model
    write(*,10)                  str,'---Ischemic Zone Model:             ',nc
  case(RM_mod)    !.Roger and McCulloc Model
    write(*,10)                  str,'---Roger and McCulloc Model:        ',nc
  case(TT_ENDO_mod)   !.Ten Tusscher Model
    write(*,10)                  str,'---ENDO, Ten Tusscher Model:        ',nc
  case(TT_MID_mod)   !.Ten Tusscher Model
    write(*,10)                  str,'---MID,  Ten Tusscher Model:        ',nc
  case(TT_EPI_mod)   !.Ten Tusscher Model
    write(*,10)                  str,'---EPI,  Ten Tusscher Model:        ',nc
  case(LR_ENDO_mod)   !.Luo Rudy Model
    write(*,10)                  str,'---ENDO, Luo Rudy Model:            ',nc
  case(LR_MID_mod)   !.Luo Rudy Model
    write(*,10)                  str,'---MID,  Luo Rudy Model:            ',nc
  case(LR_EPI_mod)   !.Luo Rudy Model
    write(*,10)                  str,'---EPI,  Luo Rudy Model:            ',nc
  case(BO_ENDO_mod)   !.Fenton Karma Model
    write(*,10)                  str,'---ENDO, Fenton Karma Model:        ',nc
  case(BO_MID_mod)   !.Fenton Karma Model
    write(*,10)                  str,'---MID,  Fenton Karma Model:        ',nc
  case(BO_EPI_mod)   !.Fenton Karma Model
    write(*,10)                  str,'---EPI,  Fenton Karma Model:        ',nc
  case(NYGREN_mod)    !.Nygren Model
    write(*,10)                  str,'---Nygren Model:                    ',nc
  case(ST_PK_mod)    !.Purkinje Stewart Model
    write(*,10)                  str,'---Purkinje Stewart Model:          ',nc
  case(GR_ENDO_mod) !.Grandi Model
    write(*,10)                  str,'---ENDO, Grandi Model:              ',nc
  case(GR_EPI_mod) !.Grandi Model
    write(*,10)                  str,'---EPI,  Grandi Model:              ',nc
  case(CRR_ENDO_mod) !.Carro Model
    write(*,10)                  str,'---ENDO, Carro Model:               ',nc
  case(CRR_EPI_mod) !.Carro Model
    write(*,10)                  str,'---EPI,  Carro Model:               ',nc
  case(CRR2_ENDO_mod) !.Carro2 Model
    write(*,10)                  str,'---ENDO, Carro2 Model:               ',nc
  case(CRR2_EPI_mod) !.Carro2 Model
    write(*,10)                  str,'---EPI,  Carro2 Model:               ',nc
  case(GRHF_ENDO_mod) !.Grandi Model Heart Failure
    write(*,10)                  str,'---ENDO, Grandi Model Heart Failure:',nc
  case(GRHF_EPI_mod) !.Grandi Model Heart Failure
    write(*,10)                  str,'---EPI,  Grandi Model Heart Failure:',nc
  case(FIBRO_mod)    !.Fibroblast Model (MacCannell)
    write(*,10)                  str,'---Fibroblast (MacCannell) Model:   ',nc
  case(MALEC_mod)    !.Maleckar Model
    write(*,10)                  str,'---Maleckar Model:                  ',nc
  case(MALEC_RA_PM_mod)! Maleckar atria model RA_PM
    write(*,10)                  str,'---Maleckar Model RA PM:            ',nc
  case(MALEC_CT_BB_mod)! Maleckar atria model CT_BB
    write(*,10)                  str,'---Maleckar Model CT BB:            ',nc
  case(MALEC_RAA_mod)! Maleckar atria model RAA
    write(*,10)                  str,'---Maleckar Model RAA:              ',nc
  case(MALEC_TVR_mod)! Maleckar atria model TVR
    write(*,10)                  str,'---Maleckar Model TVR:              ',nc
  case(MALEC_LA_mod)! Maleckar atria model LA
    write(*,10)                  str,'---Maleckar Model LA:               ',nc
  case(MALEC_PV_mod)! Maleckar atria model PV
    write(*,10)                  str,'---Maleckar Model PV:               ',nc
  case(MALEC_LAA_mod)! Maleckar atria model LAA
    write(*,10)                  str,'---Maleckar Model LAA:              ',nc
  case(MALEC_MVR_mod)! Maleckar atria model MVR
    write(*,10)                  str,'---Maleckar Model MVR:              ',nc
  case(OH_ENDO_mod)  !.Ohara Model
    write(*,10)            str,'---ENDO, Ohara Model:               ',nc
  case(OH_MID_mod)   !.Ohara Model
    write(*,10)            str,'---MID, Ohara Model:                ',nc
  case(OH_EPI_mod)   !.Ohara Model
    write(*,10)            str,'---EPI, Ohara Model:                ',nc
  case(OH_NA_ENDO_mod)  !.Ohara Model
    write(*,10)            str,'---ENDO, OharaNa Model:               ',nc
  case(OH_NA_MID_mod)   !.Ohara Model
    write(*,10)            str,'---MID, OharaNa Model:                ',nc
  case(OH_NA_EPI_mod)   !.Ohara Model
    write(*,10)            str,'---EPI, OharaNa Model:                ',nc
  case(Courte_mod)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model:              ',nc
  case(Courte_mod_RA_PM)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model RA/PM:              ',nc
  case(Courte_mod_CT_BB)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model CT/BB:              ',nc
  case(Courte_mod_RAA)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model RAA:              ',nc
  case(Courte_mod_TVR)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model TVR:              ',nc
  case(Courte_mod_LA)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model LA:              ',nc
  case(Courte_mod_LAA)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model LAA:              ',nc
  case(Courte_mod_PV)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model PV:              ',nc
  case(Courte_mod_MVR)   !.Courtemanche Model
    write(*,10)            str,'---Courtemanche Model MVR:              ',nc
  case default
    write(*,20)                  str,'---Unknow cell type:                ', t_cel, nc
  end select
  return
10 format (A,A,I8)
20 format (A,A,I3,I8)
end subroutine PrintNumCellEM
  
end module mod_Iion
