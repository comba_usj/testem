! ------------------------------------------------------------------------------
module mod_UnitUtil
! ------------------------------------------------------------------------------
! #.unidad 5 esta conectada a la entrada estandar
! #.unidad 6 esta conectada a la salida estandar
! #.unidad * esta conectada a la entrada y salida estandar
! ------------------------------------------------------------------------------
! std_i  : standart input
! std_o  : standart output
! ------------------------------------------------------------------------------
  use mod_precision
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter, private :: min_un =   10
  integer(ip), parameter, private :: max_un = 1000
  integer(ip), parameter          :: std_i = 5 !.Standart input
  integer(ip), parameter          :: std_o = 6 !.Standart output
  integer(ip), parameter          :: lu_log= 7
  !.
  logical, private                :: u_unit(min_un:max_un) = .false.
!
  public                          :: a_unit, close_unit, closeall_unit,          &
                                     fun_getarg, file_name, f_filename
  private                         :: free_unit
  save u_unit
!
! ------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! ------------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
subroutine a_unit(lu)
! ------------------------------------------------------------------------------
! Esta funcion asigna un numero libre a la unidad  
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),intent(out)  :: lu
  integer(ip)              :: iost
  logical                  :: used

  do lu = min_un,max_un
    if (u_unit(lu)) cycle
    inquire(unit=lu, opened=used, iostat=iost)
    if (iost == 0 .and. (.not. used)) then
      u_unit(lu) = .true.
      return
    end if
  end do
  write (std_o,10); call  closeall_unit; stop
  return
10 format('###.Error asignando unidad, subroutine a_unit')
end subroutine a_unit
! ------------------------------------------------------------------------------
subroutine free_unit(lu)
! ------------------------------------------------------------------------------
! Esta funcion libera la unidad usada, en el array u_unit  
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: lu

  if (u_unit(lu)) then
    u_unit(lu) = .false.
  else
    write (std_o,10) lu; call  closeall_unit; stop
  end if
  return
10 format('###.Error liberando unidad, subroutine free_unit',I4)
end subroutine free_unit
! ------------------------------------------------------------------------------
subroutine close_unit (lu)
! ------------------------------------------------------------------------------
! Esta funcion cierra la unidad "lu"
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)  :: lu
  logical      :: used

  if (u_unit(lu)) then
    inquire(unit=lu, opened=used)
    if(used) then
      close (lu);
      call free_unit(lu)
    end if
  end if
  return
end subroutine close_unit
! ------------------------------------------------------------------------------
subroutine closeall_unit
! ------------------------------------------------------------------------------
! Esta funcion cierra todas las unidades abiertas
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)  :: lu

  do lu = min_un , max_un
    call close_unit(lu)
  end do
  return
end subroutine closeall_unit
! ------------------------------------------------------------------------------
subroutine fun_getarg (jp,str,fname)  
! -----------------------------------------------------------------------------
! Esta funcion recupera argumentos de la linea de comandos 
! -----------------------------------------------------------------------------
  implicit none
  integer(ip)                :: jp
  character(len=*)           :: str,fname
  character(len=4)           :: buf

  call getarg(jp,buf)
  if (buf(1:2) == str(1:2)) then
    call getarg(jp+1,fname);
  else
    stop '###.Error "RETRIEVE ARGUMENTS" (function fun_getarg)'
  end if
  return
end subroutine fun_getarg
! ------------------------------------------------------------------------------
subroutine file_name (raiz,ext,i,nfile,m)
! ------------------------------------------------------------------------------
! Esta subrutina concatena a una cadena de caracteres un numero entero y le 
! agrega una extension para formar un nombre de archivo.
! ------------------------------------------------------------------------------
  implicit none
  character (len=*),intent(in)  :: raiz,ext
  integer(ip), intent(in)       :: i
  character (len=*),intent(out) :: nfile
  integer(ip), intent(out)      :: m
  integer(ip)                   :: jl,ie
 
  jl=len_trim(raiz);nfile=raiz(1:jl)
  jl = jl + 1 
  select case (i)
  case (0:9)
    write(nfile(jl:jl),  '(I1)') i; m  = jl + 1
  case (10:99)
    write(nfile(jl:jl+1),'(I2)') i; m  = jl + 2
  case (100:999)
    write(nfile(jl:jl+2),'(I3)') i; m  = jl + 3
  case (1000:9999)
    write(nfile(jl:jl+3),'(I4)') i; m  = jl + 4
  case (10000:99999)
    write(nfile(jl:jl+4),'(I5)') i; m  = jl + 5
  case (100000:999999)
    write(nfile(jl:jl+5),'(I6)') i; m  = jl + 6
  case (1000000:9999999)
    write(nfile(jl:jl+6),'(I7)') i; m  = jl + 7
  case (10000000:99999999)
    write(nfile(jl:jl+7),'(I8)') i; m  = jl + 8
  case (100000000:999999999)
    write(nfile(jl:jl+8),'(I9)') i; m  = jl + 9
  case default
    stop '###.ERROR IN FILE NAME'
  end select
!
  ie=len_trim(ext)
  write(nfile(m:m+ie),'(a)') ext(1:ie)
  m=m+ie
  return
end subroutine file_name
! -------------------------------------------------------------------------------
subroutine f_filename (raiz,ext,i,nfile,m)
! -------------------------------------------------------------------------------
! Esta subrutina concatena a una cadena de caracteres un numero entero, 
! agregandole ceros al principio para tener siempre el mismo numero de caracteres
! tambien se le agrega una extension para formar un nombre de archivo.
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: i
  integer(ip), intent(out)      :: m
  character (len=*),intent(in)  :: raiz,ext
  character (len=*),intent(out) :: nfile
  integer(ip)                   :: jl,ie
 
  jl=len_trim(raiz);nfile=raiz(1:jl)
  select case (i)
  case (0:9)
    nfile=nfile(1:jl)//'0000000';   jl = jl + 8 
    write(nfile(jl:jl),  '(I1)') i; m  = jl + 1
  case (10:99)
    nfile=nfile(1:jl)//'000000';    jl = jl + 7
    write(nfile(jl:jl+1),'(I2)') i; m  = jl + 2
  case (100:999)
    nfile=nfile(1:jl)//'00000';     jl = jl + 6
    write(nfile(jl:jl+2),'(I3)') i; m  = jl + 3   
  case (1000:9999)
    nfile=nfile(1:jl)//'0000';      jl = jl + 5
    write(nfile(jl:jl+3),'(I4)') i; m  = jl + 4
  case (10000:99999)
    nfile=nfile(1:jl)//'000';       jl = jl + 4
    write(nfile(jl:jl+4),'(I5)') i; m  = jl + 5
  case (100000:999999)
    nfile=nfile(1:jl)//'00';        jl = jl + 3
    write(nfile(jl:jl+5),'(I6)') i; m  = jl + 6
  case (1000000:9999999)
    nfile=nfile(1:jl)//'0';         jl = jl + 2
    write(nfile(jl:jl+6),'(I7)') i; m  = jl + 7
  case (10000000:99999999)
    write(nfile(jl:jl+7),'(I8)') i; m  = jl + 8
  case (100000000:999999999)
    write(nfile(jl:jl+8),'(I9)') i; m  = jl + 9
  case default
    stop '###.ERROR EN NOM_ARCH'
  end select
!
  ie=len_trim(ext)
  write(nfile(m:m+ie),'(a)') ext(1:ie)
  m=m+ie
  return
end subroutine f_filename
!
!
subroutine get_time(current_time)
!
  implicit none
  real(rp), intent(out) :: current_time
  integer               :: value(8)

  call date_and_time(values=value)
  current_time = 86400.0*value(3) + 3600.0*value(5) + 60.0*value(6) + &
                         value(7) + 0.001*value(8)
end subroutine get_time
! ------------------------------------------------------------------------------  
end module mod_UnitUtil
! ------------------------------------------------------------------------------
