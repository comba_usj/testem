!------------------------------------------------------------------------------
! This are the parameters for the Carro Model (Grandi Modified Model)
! Unpublished
!
! Constants
!
! Parameters that can be modified through the input file
!
  !.--K currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_Gtos_endo =  3.7596e-2;  ! Ito conductance for endo
  real(rp), parameter, private :: p_Gtos_epi  =  1.56e-2;   ! Ito conductance for epi
  real(rp), parameter, private :: p_Gtof_endo =  1.404e-3;  ! Ito conductance for endo
  real(rp), parameter, private :: p_Gtof_epi  =  1.144e-1;   ! Ito conductance for epi
  !.--Concentrations ----------------------------------------------------------
  real(rp), parameter, private :: p_Ko  = 5.4;    ! [mM]
!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_R     = 8314;           !. JK^-1kmol^-1  
  real(rp), parameter, private :: p_T     = 310.0;          !. K
  real(rp), parameter, private :: p_F     = 96485;          !. C/mol
  real(rp), parameter, private :: p_RTF   = p_R*p_T/p_F;    !. 1/mV
  real(rp), parameter, private :: p_iRTF  = p_F/p_R/p_T;    !. mV
  !.--Capacitance --------------------------------------------------------------
  real(rp), parameter, private :: p_Cap   = 1.3810e-10;    ! [F]
  !.--Enviromental parameters --------------------------------------------------
  real(rp), parameter, private :: pi = 2*acos(0.0);
  real(rp), parameter, private :: p_cellLength = 100;     !. cell length [um]
  real(rp), parameter, private :: p_cellRadius = 10.25;   !. cell radius [um]
  real(rp), parameter, private :: p_JuncLength = 160e-3;  !. Junc length [um]
  real(rp), parameter, private :: p_JuncRadius = 15e-3;   !. Junc radius [um]
  real(rp), parameter, private :: p_distSLcyto = 0.45;    !. dist. SL to cytosol [um]
  real(rp), parameter, private :: p_distJuncSL = 0.5;     !. dist. Junc to SL [um]
  real(rp), parameter, private :: p_DcaJuncSL = 1.64e-6;  !. Dca Junc to SL [cm^2/sec]
  real(rp), parameter, private :: p_DcaSLcyto = 1.22e-6;  !. Dca SL to cyto [cm^2/sec]
  real(rp), parameter, private :: p_DnaJuncSL = 1.09e-5;  !. Dna Junc to SL [cm^2/sec]
  real(rp), parameter, private :: p_DnaSLcyto = 1.79e-5;  !. Dna SL to cyto [cm^2/sec] 
  real(rp), parameter, private :: p_Vc = pi*p_cellRadius**2*p_cellLength*1e-15;  ! [L]
  real(rp), parameter, private :: p_VMyo = 0.65*p_Vc;
  real(rp), parameter, private :: p_VSR = 0.035*p_Vc;
  real(rp), parameter, private :: p_VSL = 0.02*p_Vc;
  real(rp), parameter, private :: p_VJunc = 5.39e-4*p_Vc; 
  real(rp), parameter, private :: p_SAJunc = 20150*pi*2*p_JuncLength*p_JuncRadius;  ! [um^2]
  real(rp), parameter, private :: p_SASL = pi*2*p_cellRadius*p_cellLength;          ! [um^2]
  !------------------------------------------------------------------------------
  real(rp), parameter, private :: p_JCaJuncSL = 8.2413e-13;   ! [L/msec]
  real(rp), parameter, private :: p_JCaSLMyo  = 3.7243e-12;   ! [L/msec]
  real(rp), parameter, private :: p_JNaJuncSL = 1.8313e-14;   ! [L/msec]
  real(rp), parameter, private :: p_JNaSLMyo  = 1.6386e-12;   ! [L/msec]
  !.--Fractional Currents--------------------------------------------------------
  real(rp), parameter, private :: p_FJunc = 0.11 !
  real(rp), parameter, private :: p_FSL = 1 - p_FJunc !
  real(rp), parameter, private :: p_FJuncCaL = 0.9 !
  real(rp), parameter, private :: p_FSLCaL = 1 - p_FJuncCaL !
    !.--Concentrations ----------------------------------------------------------
  real(rp), parameter, private :: p_Cli = 15;     ! [mM]
  real(rp), parameter, private :: p_Clo = 150;    ! [mM]
  real(rp), parameter, private :: p_Nao = 140;    ! [mM]
  real(rp), parameter, private :: p_Cao = 1.8;    ! [mM]
  real(rp), parameter, private :: p_Mgi = 1;      ! [mM]
  real(rp), parameter, private :: p_Ki  = 138;    ! [mM]
  !.--K currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_pNaK = 0.01833;             ! nS/pF
  real(rp), parameter, private :: p_GKp = 0.002;                ! mS/uF
  real(rp), parameter, private :: p_GKsJunc = 0.0035;           ! mS/uF
  real(rp), parameter, private :: p_GKsSL = 0.0035;             ! mS/uF
  real(rp), parameter, private :: p_GK1 = 0.57153;              ! mS/uF
  real(rp), parameter, private :: p_GKr = 0.035;              ! mS/uF
  !.--Na Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_GNa   = 18.86;              ! nS/pF
  real(rp), parameter, private :: p_GNaB  = 0.597e-3;        ! nS/pF
  real(rp), parameter, private :: p_IbarNaK = 0.99;    !. [A/F]
  real(rp), parameter, private :: p_KmNaip = 11;           !. [mM]
  real(rp), parameter, private :: p_KmKo = 1.5;            !. [mM]
  !.--Cl Currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_KdClCa = 0.1;    !. [mM]
  real(rp), parameter, private :: p_GClCa  = 0.054813;     !  [mS/uF]
  real(rp), parameter, private :: p_GClB  = 9e-3;               !  [mS/uF]
  !.--Ca Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_pNa = 3.0375e-9;            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_pCa = 1.9887e-4            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_pK  = 5.4675e-8;            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_GCaB = 5.513e-4;      ! [A/F]
  real(rp), parameter, private :: p_IbarNCX = 4.5;           ! [A/F]
  real(rp), parameter, private :: p_KmCai = 3.59e-3;      ! [mM]
  real(rp), parameter, private :: p_KmCao = 1.3;          ! [mM]
  real(rp), parameter, private :: p_KmNai = 12.29;        ! [mM]
  real(rp), parameter, private :: p_KmNao = 87.5;         ! [mM]
  real(rp), parameter, private :: p_ksat = 0.32;           ! [-]
  real(rp), parameter, private :: p_nu = 0.27;              ! [-]
  real(rp), parameter, private :: p_Kdact = 0.150e-3;      ! [mM]
  real(rp), parameter, private :: p_IbarPMCA = 0.0673;        ! [A/F]
  real(rp), parameter, private :: p_KmPCa = 0.5e-3;        ![mM]
  !.--SR Ca Fluxes --------------------------------------------------------------
  real(rp), parameter, private :: p_VmaxSRCaP = 5.3114e-3;  ! [mM/ms]
  real(rp), parameter, private :: p_Kmf = 0.246e-3;            ! [mM]
  real(rp), parameter, private :: p_Kmr = 1.7;                 ! [mM]
  real(rp), parameter, private :: p_hillSRCaP = 1.787;       ! [-]
  real(rp), parameter, private :: p_ks = 25;                    ! [ms^-1]
  real(rp), parameter, private :: p_koCa = 10;                 ! [ms^-1*mM^2]
  real(rp), parameter, private :: p_kom = 0.06;                ! [ms^-1]
  real(rp), parameter, private :: p_kiCa = 0.5;                ! [ms^-1*mM^2]
  real(rp), parameter, private :: p_kim = 0.005;               ! [ms^-1]
  real(rp), parameter, private :: p_ec50SR = 0.45;             ! [mM]
  !.--Buffering -----------------------------------------------------------------
  real(rp), parameter, private :: p_BmaxNaj = 7.561;                         ! [mM]
  real(rp), parameter, private :: p_BmaxNaSL = 1.65;                         ! [mM]
  real(rp), parameter, private :: p_koffNa = 1e-3;                            ! [ms^-1]
  real(rp), parameter, private :: p_konNa = 0.1e-3;                           ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxTnClow = 70e-3;                      ! [mM]
  real(rp), parameter, private :: p_koffTnCl = 19.6e-3;                      ! [ms^-1]
  real(rp), parameter, private :: p_konTnCl = 32.7;                          ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxTnChigh = 140e-3;                    ! [mM]
  real(rp), parameter, private :: p_koffTnChCa = 0.032e-3;                  ! [ms^-1]
  real(rp), parameter, private :: p_konTnChCa = 2.37;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_koffTnChMg = 3.33e-3;                   ! [ms^-1]
  real(rp), parameter, private :: p_konTnChMg = 3e-3;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxCaM = 24e-3;                          ! [mM]
  real(rp), parameter, private :: p_koffCaM = 238e-3;                         ! [ms^-1]
  real(rp), parameter, private :: p_konCaM = 34;                              ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxMyosin = 140e-3;                      ! [mM]
  real(rp), parameter, private :: p_koffMyoCa = 0.46e-3;                     ! [ms^-1]
  real(rp), parameter, private :: p_konMyoCa = 13.8;                         ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_koffMyoMg = 0.057e-3;                    ! [ms^-1]
  real(rp), parameter, private :: p_konMyoMg = 0.0157;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSR = 17.1e-3;                        ! [mM]
  real(rp), parameter, private :: p_koffSR = 60e-3;                           ! [ms^-1]
  real(rp), parameter, private :: p_konSR = 100;                              ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSLlowSL = 37.4e-3*p_VMyo/p_VSL;       ! [mM]
  real(rp), parameter, private :: p_BmaxSLlowj = 4.6e-4*p_VMyo/p_VJunc;       ! [mM]
  real(rp), parameter, private :: p_koffSLl = 1.3;                           ! [ms^-1]
  real(rp), parameter, private :: p_konSLl = 100;                            ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSLhighSL = 13.4e-3*p_VMyo/p_VSL;      ! [mM]
  real(rp), parameter, private :: p_BmaxSLhighj = 1.65e-4*p_VMyo/p_VJunc;    ! [mM]
  real(rp), parameter, private :: p_koffSLh = 30e-3;                         ! [ms^-1]
  real(rp), parameter, private :: p_konSLh = 100;                            ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxCsqn = 140e-3*p_VMyo/p_VSR;             ! [mM]
  real(rp), parameter, private :: p_koffCsqn = 65;                            ! [ms^-1]
  real(rp), parameter, private :: p_konCsqn = 100;                            ! [ms^-1*mM^-1]

!------------------------------------------------------------------------------
!-- INITIAL VALUES ------------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter :: end_m =   2.176608e-03,       epi_m =   2.163678e-03,       &
                         def_m =   2.163678e-03        !
  real(rp), parameter :: end_h =   7.126555e-01,       epi_h =   7.134970e-01,       &
                         def_h =   7.134970e-01        !
  real(rp), parameter :: end_j =   7.119893e-01,       epi_j =   7.128671e-01,       &
                         def_j =   7.128671e-01        !
  real(rp), parameter :: end_d =   1.879996e-06,       epi_d =   1.871177e-06,       &
                         def_d =   1.871177e-06        !
  real(rp), parameter :: end_f =   9.789409e-01,       epi_f =   9.804391e-01,       &
                         def_f =   9.804391e-01        !
  real(rp), parameter :: end_f2 =   9.993986e-01,      epi_f2 =   9.994010e-01,      &
                         def_f2 =   9.994010e-01       !
  real(rp), parameter :: end_fCaBJ =   2.861794e-02,   epi_fCaBJ =   2.847118e-02,   &
                         def_fCaBJ =   2.847118e-02    !
  real(rp), parameter :: end_fCaBSL =   1.698330e-02,  epi_fCaBSL =   1.692189e-02,  &
                         def_fCaBSL =   1.692189e-02   !
  real(rp), parameter :: end_xtos =   3.592503e-04,    epi_xtos =   3.584727e-04,    &
                         def_xtos =   3.584727e-04     !
  real(rp), parameter :: end_ytos =   8.161309e-01,    epi_ytos =   8.087629e-01,    &
                         def_ytos =   8.087629e-01     !
  real(rp), parameter :: end_xtof =   3.592405e-04,    epi_xtof =   3.584625e-04,    &
                         def_xtof =   3.584625e-04     !
  real(rp), parameter :: end_ytof =   9.999976e-01,    epi_ytof =   9.999976e-01,    &
                         def_ytof =   9.999976e-01     !
  real(rp), parameter :: end_xkr =   1.896559e-02,     epi_xkr =   1.516232e-02,     &
                         def_xkr =   1.516232e-02      !
  real(rp), parameter :: end_xks =   3.556360e-03,     epi_xks =   3.549354e-03,     &
                         def_xks =   3.549354e-03      !
  real(rp), parameter :: end_RyRr =   8.888214e-01,    epi_RyRr =   8.886338e-01,    &
                         def_RyRr =   8.886338e-01     !
  real(rp), parameter :: end_RyRo =   1.149876e-06,    epi_RyRo =   1.126209e-06,    &
                         def_RyRo =   1.126209e-06     !
  real(rp), parameter :: end_RyRi =   1.438310e-07,    epi_RyRi =   1.411382e-07,    &
                         def_RyRi =   1.411382e-07     !
  real(rp), parameter :: end_NaBJ =   3.785209e+00,    epi_NaBJ =   3.796195e+00,    &
                         def_NaBJ =   3.796195e+00     !
  real(rp), parameter :: end_NABSL =   8.259271e-01,   epi_NABSL =   8.283308e-01,   &
                         def_NABSL =   8.283308e-01    !
  real(rp), parameter :: end_TnCL =   9.811535e-03,    epi_TnCL =   9.757237e-03,    &
                         def_TnCL =   9.757237e-03     !
  real(rp), parameter :: end_TnCHC =   1.225802e-01,   epi_TnCHC =   1.225914e-01,   &
                         def_TnCHC =   1.225914e-01    !
  real(rp), parameter :: end_TnCHM =   8.128604e-03,   epi_TnCHM =   8.122010e-03,   &
                         def_TnCHM =   8.122010e-03    !
  real(rp), parameter :: end_CaM =   3.288063e-04,     epi_CaM =   3.267494e-04,     &
                         def_CaM =   3.267494e-04      !
  real(rp), parameter :: end_MyoC =   2.522168e-03,    epi_MyoC =   2.520383e-03,    &
                         def_MyoC =   2.520383e-03     !
  real(rp), parameter :: end_MyoM =   1.369514e-01,    epi_MyoM =   1.369529e-01,    &
                         def_MyoM =   1.369529e-01     !
  real(rp), parameter :: end_SRB =   2.386830e-03,     epi_SRB =   2.373753e-03,     &
                         def_SRB =   2.373753e-03      !
  real(rp), parameter :: end_SLLJ =   8.606485e-03,    epi_SLLJ =   8.563314e-03,    &
                         def_SLLJ =   8.563314e-03     !
  real(rp), parameter :: end_SLLSL =   1.101044e-02,   epi_SLLSL =   1.097424e-02,   &
                         def_SLLSL =   1.097424e-02    !
  real(rp), parameter :: end_SLHJ =   8.078504e-02,    epi_SLHJ =   8.053908e-02,    &
                         def_SLHJ =   8.053908e-02     !
  real(rp), parameter :: end_SLHSL =   1.238366e-01,   epi_SLHSL =   1.235381e-01,   &
                         def_SLHSL =   1.235381e-01    !
  real(rp), parameter :: end_CSQNB =   1.262853e+00,   epi_CSQNB =   1.258048e+00,   &
                         def_CSQNB =   1.258048e+00    !
  real(rp), parameter :: end_CaSR =   6.138856e-01,    epi_CaSR =   6.093596e-01,    &
                         def_CaSR =   6.093596e-01     !
  real(rp), parameter :: end_NaJ =   1.002110e+01,     epi_NaJ =   1.007931e+01,     &
                         def_NaJ =   1.007931e+01      !
  real(rp), parameter :: end_NaSL =   1.001974e+01,    epi_NaSL =   1.007810e+01,    &
                         def_NaSL =   1.007810e+01     !
  real(rp), parameter :: end_Nai =   1.001989e+01,     epi_Nai =   1.007825e+01,     &
                         def_Nai =   1.007825e+01      !
  real(rp), parameter :: end_CaJ =   2.048633e-04,     epi_CaJ =   2.038197e-04,     &
                         def_CaJ =   2.038197e-04      !
  real(rp), parameter :: end_CaSL =   1.188246e-04,    epi_CaSL =   1.184305e-04,    &
                         def_CaSL =   1.184305e-04     !
  real(rp), parameter :: end_Cai =   9.719632e-05,     epi_Cai =   9.658067e-05,     &
                         def_Cai =   9.658067e-05      !
  real(rp), parameter :: Vi_CRR_end  =  -8.410546e+01,  Vi_CRR_epi  =  -8.413368e+01   
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: CRR_ENDO_mod = 14;  ! Carro ENDO model         (VH)
  integer(ip), parameter :: CRR_EPI_mod  = 15;  ! Carro EPI model          (VH)
  integer(ip),parameter  :: nvar_crr     = 38;  ! Number of state variables
  integer(ip),parameter  :: ncur_crr     = 24;  ! Number of currents
  integer(ip),parameter  :: np_crr       = 17;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------ 
  integer(ip), parameter    :: m_stpCRR  =    5
  real(rp),    parameter    :: m_dvdtCRR =  1.0
