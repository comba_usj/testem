! This module implements the Grandi Model of 2009
! Grandi E, et al, A novel computational model of the human ventricular action potential and Ca 
! transient, J Mol Cell Cardiol (2009), doi:10.1016/j.yjmcc.2009.09.019
!------------------------------------------------------------------------------
module mod_grandi
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'grandi_parameters.inc'
  !.
  type t_grm
    sequence
    real(rp) :: Cai, CaSR, CaSL, CaJ, Nai, NaSL, NaJ, Ki, m, h, j, d, f,        &
                fCaBJ, fCaBSL, xtos, ytos, xtof, ytof, xkr, xks, RyRr, RyRo,    &
                RyRi, NaBJ, NABSL, TnCL, TnCHC, TnCHM, CaM, MyoC, MyoM, SRB,    &
                SLLJ, SLLSL, SLHJ, SLHSL, CSQNB
  end type t_grm
  !.
  type, private :: t_cur
    sequence
    real(rp) :: INaJunc, INaSL, INa, INaBkJunc, INaBkSL, INaBk, INaKJunc,    &
                INaKSL, INaK, Ikr, IksJunc, IksSL, Iks, IkpJunc, IkpSL, Ikp, &
                Itos, Itof, Ito, Ik1, IClCaJunc, IClCaSL, IclCa, IClBk,      &
                ICaJunc, ICaSL, ICa, ICaK, ICaNaJunc, ICaNaSL, ICaNa, ICaL,  &
                IncxJunc, IncxSL, Incx, IpCaJunc, IpCaSL, IpCa, ICaBkJunc,   &
                ICaBkSL, ICaBk, INatotJunc, INatotSL, IKtot, ICatotJunc,     &
                ICatotSL, INatot, ICltot, ICatot, Itot
  end type t_cur
  
  public  :: ic_Grandi, Grandi_A_P01
  private :: concentrations, gates, currents

  type, public:: t_prm
    private
    real(rp) :: A_GCaL;   !. 1
    real(rp) :: A_GKr;    !. 2
    real(rp) :: A_GKs;    !. 3
    real(rp) :: A_GK1;    !. 4
    real(rp) :: A_GNaK;   !. 5
    real(rp) :: A_Gncx;   !. 6
    real(rp) :: A_Gto;    !. 7
    real(rp) :: A_GpCa;   !. 8
    real(rp) :: A_GKp;    !. 9
    real(rp) :: A_GNa;    !.10
    real(rp) :: A_GClCa;  !.11
    real(rp) :: A_GNabk;  !.12
    real(rp) :: A_GClbk;  !.13
    real(rp) :: A_GCabk;  !.14
    real(rp) :: p_Gtos;   !.15
    real(rp) :: p_Gtof;   !.16
  end type t_prm
  
  
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_GR(lu_st)
!
integer(ip), intent(in)   :: lu_st

write(lu_st,'(A)',advance='no') '% '
write(lu_st,'(A)',advance='no') 'Cai '
write(lu_st,'(A)',advance='no') 'CaSR '
write(lu_st,'(A)',advance='no') 'CaSL '
write(lu_st,'(A)',advance='no') 'CaJ '
write(lu_st,'(A)',advance='no') 'Nai '
write(lu_st,'(A)',advance='no') 'NaSL '
write(lu_st,'(A)',advance='no') 'NaJ '
write(lu_st,'(A)',advance='no') 'Ki '
write(lu_st,'(A)',advance='no') 'm '
write(lu_st,'(A)',advance='no') 'h '
write(lu_st,'(A)',advance='no') 'j '
write(lu_st,'(A)',advance='no') 'd '
write(lu_st,'(A)',advance='no') 'f '
write(lu_st,'(A)',advance='no') 'fCaBJ '
write(lu_st,'(A)',advance='no') 'fCaBSL '
write(lu_st,'(A)',advance='no') 'xtos '
write(lu_st,'(A)',advance='no') 'ytos '
write(lu_st,'(A)',advance='no') 'xtof '
write(lu_st,'(A)',advance='no') 'ytof '
write(lu_st,'(A)',advance='no') 'xkr '
write(lu_st,'(A)',advance='no') 'xks '
write(lu_st,'(A)',advance='no') 'RyRr '
write(lu_st,'(A)',advance='no') 'RyRo '
write(lu_st,'(A)',advance='no') 'RyRi '
write(lu_st,'(A)',advance='no') 'NaBJ '
write(lu_st,'(A)',advance='no') 'NaBSL '
write(lu_st,'(A)',advance='no') 'TnCL '
write(lu_st,'(A)',advance='no') 'TnCHC '
write(lu_st,'(A)',advance='no') 'TnCHM '
write(lu_st,'(A)',advance='no') 'CaM '
write(lu_st,'(A)',advance='no') 'MyoC '
write(lu_st,'(A)',advance='no') 'MyoM '
write(lu_st,'(A)',advance='no') 'SRB '
write(lu_st,'(A)',advance='no') 'SLLJ '
write(lu_st,'(A)',advance='no') 'SLLSL '
write(lu_st,'(A)',advance='no') 'SLHJ '
write(lu_st,'(A)',advance='no') 'SLHSL '
write(lu_st,'(A)',advance='no') 'CSQNB '
end subroutine write_state_GR
!------------------------------------------------------------------------------!
subroutine write_current_GR(lu_cr)
!
integer(ip), intent(in)   :: lu_cr

write(lu_cr,'(A)',advance='no') '% '
write(lu_cr,'(A)',advance='no') 'INa '
write(lu_cr,'(A)',advance='no') 'INaBk '
write(lu_cr,'(A)',advance='no') 'INaK '
write(lu_cr,'(A)',advance='no') 'IKr '
write(lu_cr,'(A)',advance='no') 'IKs '
write(lu_cr,'(A)',advance='no') 'IKp '
write(lu_cr,'(A)',advance='no') 'Itos '
write(lu_cr,'(A)',advance='no') 'Itof '
write(lu_cr,'(A)',advance='no') 'Ito '
write(lu_cr,'(A)',advance='no') 'IK1 '
write(lu_cr,'(A)',advance='no') 'IclCa '
write(lu_cr,'(A)',advance='no') 'IclBk '
write(lu_cr,'(A)',advance='no') 'ICa '
write(lu_cr,'(A)',advance='no') 'ICaK '
write(lu_cr,'(A)',advance='no') 'ICaNa '
write(lu_cr,'(A)',advance='no') 'ICaL '
write(lu_cr,'(A)',advance='no') 'INaCa '
write(lu_cr,'(A)',advance='no') 'IpCa '
write(lu_cr,'(A)',advance='no') 'ICaBk '
write(lu_cr,'(A)',advance='no') 'IKtot '
write(lu_cr,'(A)',advance='no') 'INatot '
write(lu_cr,'(A)',advance='no') 'ICltot '
write(lu_cr,'(A)',advance='no') 'ICatot '
write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_GR
!------------------------------------------------------------------------------!
function get_parameter_GR (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_gr)

  select case (tcell) !12-> ENDO, 13->EPI
  case (GR_ENDO_mod)
  v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             p_Gtos_endo, p_Gtof_endo /)
  
  case (GR_EPI_mod)
  v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             p_Gtos_epi, p_Gtof_epi /)
  case default
    write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_GR
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param
  
  param%A_GCaL   = v_prm( 1)    !. 1
  param%A_GKr    = v_prm( 2)    !. 2
  param%A_GKs    = v_prm( 3)    !. 3
  param%A_GK1    = v_prm( 4)    !. 4
  param%A_GNaK   = v_prm( 5)    !. 5
  param%A_Gncx   = v_prm( 6)    !. 6
  param%A_Gto    = v_prm( 7)    !. 7
  param%A_GpCa   = v_prm( 8)    !. 8
  param%A_GKp    = v_prm( 9)    !. 9
  param%A_GNa    = v_prm(10)    !.10
  param%A_GClCa  = v_prm(11)    !.11
  param%A_GNabk  = v_prm(12)    !.12
  param%A_GClbk  = v_prm(13)    !.13
  param%A_GCabk  = v_prm(14)    !.14
  param%p_Gtos   = v_prm(15)    !.15
  param%p_Gtof   = v_prm(16)    !.16

  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_Grandi(ict) result(gr_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Ten Tusscher Model
! tt_m is the main structure which contains the membrane potential, ioninc 
! concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none
!
  integer (ip), intent(in)  :: ict
  real(rp)                  :: gr_ic(nvar_gr)
  
  select case (ict)
  case(GR_ENDO_mod) !.----ENDO
    gr_ic = (/end_Cai, end_CaSR, end_CaSL, end_CaJ, end_Nai, end_NaSL,  &
      end_NaJ, end_Ki, end_m, end_h, end_j, end_d, end_f, end_fCaBJ,     &
      end_fCaBSL, end_xtos, end_ytos, end_xtof, end_ytof, end_xkr,       &
      end_xks, end_RyRr, end_RyRo, end_RyRi, end_NaBJ, end_NABSL,        &
      end_TnCL, end_TnCHC, end_TnCHM, end_CaM, end_MyoC, end_MyoM,       &
      end_SRB, end_SLLJ, end_SLLSL, end_SLHJ, end_SLHSL, end_CSQNB /)
  case(GR_EPI_mod) !.----EPI
    gr_ic = (/epi_Cai, epi_CaSR, epi_CaSL, epi_CaJ, epi_Nai, epi_NaSL,  &
      epi_NaJ, epi_Ki, epi_m, epi_h, epi_j, epi_d, epi_f,                &
      epi_fCaBJ, epi_fCaBSL, epi_xtos, epi_ytos, epi_xtof, epi_ytof,     &
      epi_xkr, epi_xks, epi_RyRr, epi_RyRo, epi_RyRi, epi_NaBJ,          &
      epi_NABSL, epi_TnCL, epi_TnCHC, epi_TnCHM, epi_CaM, epi_MyoC,      &
      epi_MyoM, epi_SRB, epi_SLLJ, epi_SLLSL, epi_SLHJ, epi_SLHSL,       &
      epi_CSQNB /)
  case default
    gr_ic = (/def_Cai, def_CaSR, def_CaSL, def_CaJ, def_Nai, def_NaSL,  &
      def_NaJ, def_Ki, def_m, def_h, def_j, def_d, def_f,                &
      def_fCaBJ, def_fCaBSL, def_xtos, def_ytos, def_xtof, def_ytof,     &
      def_xkr, def_xks, def_RyRr, def_RyRo, def_RyRi, def_NaBJ,          &
      def_NABSL, def_TnCL, def_TnCHC, def_TnCHM, def_CaM, def_MyoC,      &
      def_MyoM, def_SRB, def_SLLJ, def_SLLSL, def_SLHJ, def_SLHSL,       &
      def_CSQNB /)
  end select
  !.

  return
end function ic_Grandi
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_grm), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_gr)
  v_me( 1) = str_me%Cai
  v_me( 2) = str_me%CaSR
  v_me( 3) = str_me%CaSL
  v_me( 4) = str_me%CaJ
  v_me( 5) = str_me%Nai
  v_me( 6) = str_me%NaSL
  v_me( 7) = str_me%NaJ
  v_me( 8) = str_me%Ki
  v_me( 9) = str_me%m
  v_me(10) = str_me%h
  v_me(11) = str_me%j
  v_me(12) = str_me%d
  v_me(13) = str_me%f
  v_me(14) = str_me%fCaBJ
  v_me(15) = str_me%fCaBSL
  v_me(16) = str_me%xtos
  v_me(17) = str_me%ytos
  v_me(18) = str_me%xtof
  v_me(19) = str_me%ytof
  v_me(20) = str_me%xkr
  v_me(21) = str_me%xks
  v_me(22) = str_me%RyRr
  v_me(23) = str_me%RyRo
  v_me(24) = str_me%RyRi
  v_me(25) = str_me%NaBJ
  v_me(26) = str_me%NABSL
  v_me(27) = str_me%TnCL
  v_me(28) = str_me%TnCHC
  v_me(29) = str_me%TnCHM
  v_me(30) = str_me%CaM
  v_me(31) = str_me%MyoC
  v_me(32) = str_me%MyoM
  v_me(33) = str_me%SRB
  v_me(34) = str_me%SLLJ
  v_me(35) = str_me%SLLSL
  v_me(36) = str_me%SLHJ
  v_me(37) = str_me%SLHSL
  v_me(38) = str_me%CSQNB
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_grm), intent(out) :: str_me
  
  str_me%Cai    =  v_me( 1);
  str_me%CaSR   =  v_me( 2);
  str_me%CaSL   =  v_me( 3);
  str_me%CaJ    =  v_me( 4);
  str_me%Nai    =  v_me( 5);
  str_me%NaSL   =  v_me( 6);
  str_me%NaJ    =  v_me( 7);
  str_me%Ki     =  v_me( 8);
  str_me%m      =  v_me( 9);
  str_me%h      =  v_me(10);
  str_me%j      =  v_me(11);
  str_me%d      =  v_me(12);
  str_me%f      =  v_me(13);
  str_me%fCaBJ  =  v_me(14);
  str_me%fCaBSL =  v_me(15);
  str_me%xtos   =  v_me(16);
  str_me%ytos   =  v_me(17);
  str_me%xtof   =  v_me(18);
  str_me%ytof   =  v_me(19);
  str_me%xkr    =  v_me(20);
  str_me%xks    =  v_me(21);
  str_me%RyRr   =  v_me(22);
  str_me%RyRo   =  v_me(23);
  str_me%RyRi   =  v_me(24);
  str_me%NaBJ   =  v_me(25);
  str_me%NABSL  =  v_me(26);
  str_me%TnCL   =  v_me(27);
  str_me%TnCHC  =  v_me(28);
  str_me%TnCHM  =  v_me(29);
  str_me%CaM    =  v_me(30);
  str_me%MyoC   =  v_me(31);
  str_me%MyoM   =  v_me(32);
  str_me%SRB    =  v_me(33);
  str_me%SLLJ   =  v_me(34);
  str_me%SLLSL  =  v_me(35);
  str_me%SLHJ   =  v_me(36);
  str_me%SLHSL  =  v_me(37);
  str_me%CSQNB  =  v_me(38); 
  return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine concentrations (dt, cur, prm, grm)
!-------------------------------------------------------------------------------
! Function to update Ion concentrations
!-------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)      :: dt
  type (t_cur), intent(in)      :: cur
  type (t_prm), intent(in)      :: prm
  type (t_grm),   intent(inout) :: grm
  
  real(rp):: dNaBJ, dNaBSL, dTnCl, dTnChc, dTnChm, dCaM, &
             dMyoc, dMyom, dSRB, JCaBcytosol, dSLLJ,     &
             dSLLSL, dSLHJ, dSLHSL, JCaBJunction, JCaBSL,&
             dCSQNB, dNaJ, dNaSL, dNai,JSRCarel, JserCa, &
             JSRleak, dCaJ, dCaSL, dCai, dCaSR

  ! Sodium Buffers
  dNaBJ = p_konNa * grm%NaJ * (p_BmaxNaJ - grm%NaBJ) - p_koffNa * grm%NaBJ;
  dNaBSL = p_konNa * grm%NaSL * (p_BmaxNaSL - grm%NaBSL) - p_koffNa * grm%NaBSL;
  
  ! Cytosolic Ca Buffers
  dTnCL = p_konTnCl * grm%Cai * (p_BmaxTnClow - grm%TnCl) - & 
        p_koffTnCl * grm%TnCl;
  dTnChc = p_konTnChCa * grm%Cai * (p_BmaxTnChigh - grm%TnChc - &
        grm%TnChm) - p_koffTnChCa * grm%TnChc;
  dTnChm = p_konTnChMg * p_Mgi * (p_BmaxTnChigh - grm%TnChc - &
        grm%TnChm) - p_koffTnChMg * grm%TnChm;
  dCaM = p_konCaM * grm%Cai * (p_BmaxCaM - grm%CaM) - p_koffCaM * grm%CaM;
  dMyoc = p_konMyoCa * grm%Cai * (p_BmaxMyosin - grm%Myoc - grm%Myom) - &
        p_koffMyoCa * grm%Myoc;
  dMyom = p_konMyoMg * p_Mgi * (p_BmaxMyosin - grm%Myoc - grm%Myom) - &
        p_koffMyoMg * grm%Myom;
  dSRB = p_konSR * grm%Cai * (p_BmaxSR - grm%SRB) - p_koffSR * grm%SRB;
  JCaBcytosol = dTnCl + dTnChc + dTnChm + dCaM + dMyoc + dMyom + dSRB;

  ! Junctional and SL Ca Buffers
  dSLLJ = p_konSLl * grm%CaJ * (p_BmaxSLlowJ - grm%SLLJ) - &
        p_koffSLl * grm%SLLJ;
  dSLLSL = p_konSLl * grm%CaSL * (p_BmaxSLlowSL - grm%SLLSL) - &
        p_koffSLl * grm%SLLSL;
  dSLHJ = p_konSLh * grm%CaJ * (p_BmaxSLhighJ - grm%SLHJ) - &
        p_koffSLh * grm%SLHJ;
  dSLHSL = p_konSLh * grm%CaSL * (p_BmaxSLhighSL - grm%SLHSL) - &
        p_koffSLh * grm%SLHSL;
  JCaBJunction = dSLLJ + dSLHJ;
  JCaBSL = dSLLSL + dSLHSL;
  
  ! SR Ca Buffer
  dCSQNB = p_konCsqn * grm%CaSR * (p_BmaxCsqn - grm%CSQNB) - &
        p_koffCsqn * grm%CSQNB;
    
  ! Sodium Concentrations 
  dNaJ = -cur%INatotJunc * p_Cap / (p_VJunc * p_F) + &
        p_JNaJuncSL / p_VJunc * (grm%NaSL - grm%NaJ) - dNaBJ;
  dNaSL = -cur%INatotSL * p_Cap / (p_VSL * p_F) + &
        p_JNaJuncSL / p_VSL * (grm%NaJ - grm%NASL) + &
        p_JNaSLMyo / p_VSL * (grm%Nai - grm%NaSL) - dNaBSL;
  dNai = p_JNaSLMyo / p_VMyo * (grm%NaSL - grm%Nai); 
  
  ! Calcium concentrations
  JSRCarel = p_ks * grm%RyRo * (grm%CaSR - grm%CaJ);
  JserCa = p_VmaxSRCaP * ((grm%Cai/p_Kmf) ** p_hillSRCaP - &
        (grm%CaSR/p_Kmr)**p_hillSRCaP) / (1 + (grm%Cai/p_Kmf)**p_hillSRCaP + &
        (grm%CaSR/p_Kmr) ** p_hillSRCaP);
  JSRleak = 5.348e-6 * (grm%CaSR - grm%CaJ);
  
  dCaJ = -cur%ICatotJunc * p_Cap / (p_VJunc * 2 * p_F) + &
        p_JCaJuncSL / p_VJunc * (grm%CaSL - grm%CaJ) - JCaBJunction + &
        (JSRCarel) * p_VSR / p_VJunc + JSRleak * p_VMyo / p_VJunc;
  dCaSL = -cur%ICatotSL * p_Cap / (p_VSL * 2 * p_F) + p_JCaJuncSL / &
        p_VSL * (grm%CaJ - grm%CaSL) + p_JCaSLMyo / p_VSL * &
        (grm%Cai - grm%CaSL) - JCaBSL;
  dCai = -JserCa * p_VSR / p_VMyo - JCaBCytosol + p_JCaSLMyo / p_VMyo * &
        (grm%CaSL - grm%Cai);  
  
  dCaSR = JserCa - (JSRleak * p_VMyo / p_VSR + JSRCarel) - dCSQNB;
      
  grm%NaBJ = grm%NaBJ + dNaBJ * dt;
  grm%NaBSL = grm%NaBSL + dNaBSL * dt;
   
  grm%TnCl = grm%TnCl + dTnCl * dt;
  grm%TnChc = grm%TnChc + dTnChc * dt;
  grm%TnChm = grm%TnChm + dTnChm * dt;
  grm%CaM = grm%CaM + dCaM * dt;
  grm%Myoc = grm%Myoc + dMyoc * dt;
  grm%Myom = grm%Myom + dMyom * dt;
  grm%SRB = grm%SRB + dSRB * dt;
  
  grm%SLLJ = grm%SLLJ + dSLLJ * dt;
  grm%SLLSL = grm%SLLSL + dSLLSL * dt;
  grm%SLHJ = grm%SLHJ + dSLHJ * dt;
  grm%SLHSL = grm%SLHSL + dSLHSL * dt;
  
  grm%CSQNB = grm%CSQNB + dCSQNB * dt;
  
  grm%NaJ = grm%NaJ + dNaJ * dt;
  grm%NaSL = grm%NaSL + dNaSL * dt;
  grm%Nai = grm%Nai + dNai * dt;
  
  grm%CaJ = grm%CaJ + dCaJ * dt;
  grm%CaSL = grm%CaSL + dCaSL * dt;
  grm%Cai = grm%Cai + dCai * dt;
  grm%CaSR = grm%CaSR + dCaSR * dt;
  
  return
end subroutine concentrations
!------------------------------------------------------------------------------
subroutine gates(dt, U, grm)
!------------------------------------------------------------------------------
! This function updates the gating variables
!
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: dt, U
  type(t_grm), intent(inout) :: grm
  real (rp):: mss, taum, ah, bh, tauh, hss, aj, bj, tauj,     &
              jss, dm, dh, dj, xrss, tauxr, dxkr, xsss,       &
              tauxs, dxks,xtoss, ytoss, tauxtos, tauytos,     &
              dxtos, dytos, tauxtof, tauytof, dxtof, dytof,   &
              dss, taud, fss, tauf, dd, df, dfCaBJ, dfCaBSL,  &
              MaxSR, MinSR, kCaSR, koSRCa, kiSRCa, RI, dRyRr, &
              dRyRo, dRyRi
   
  ! I_Na: Fast Na Current
  mss = 1 / ((1 + exp( -(56.86 + U) / 9.03))**2);
  taum = 0.1292 * exp(-((U + 45.79) / 15.54)**2) + &
        0.06487 * exp(-((U - 4.823) / 51.12)**2);
  if(U >= -40) then
    ah = 0; 
    bh = 0.77 / (0.13 * (1 + exp(-(U + 10.66) / 11.1 )));
    
    aj = 0;
    bj = (0.6 * exp(0.057 * U)) / (1 + exp(-0.1 * (U + 32)));
  else
    ah = 0.057 * exp(-(U + 80) / 6.8); 
    bh = 2.7 * exp(0.079 * U) + 3.1e5 * exp(0.3485 * U); 
    
    aj = (-2.5428e4 * exp(0.2444 * U) - 6.948e-6 * exp(-0.04391 * U)) * &
        (U + 37.78) / (1 + exp(0.311 * (U + 79.23)));
    bj = (0.02424 * exp(-0.01052 * U )) / (1 + exp(-0.1378 * (U + 40.14)));
  endif
  
  tauh = 1 / (ah + bh); 
  hss = 1 / ((1 + exp((U + 71.55) / 7.43))**2);
 
  tauj = 1 / (aj + bj);
  jss = 1 / ((1 + exp((U + 71.55) / 7.43))**2);         
 
  dm = (mss - grm%m) / taum;
  dh = (hss - grm%h) / tauh;
  dj = (jss - grm%j) / tauj;
    
  ! I_kr: Rapidly Activating K Current
  xrss = 1 / (1 + exp(-(U + 10) / 5));
  tauxr = 550 / (1 + exp((-22 - U) / 9)) * 6 / (1 + exp((U - (-11)) / 9)) + &
        230 / (1 + exp((U - (-40)) / 20));
  dxkr = (xrss - grm%xkr) / tauxr;
  
  ! I_ks: Slowly Activating K Current  
  xsss = 1 / (1 + exp(-(U + 3.8) / 14.25));
  tauxs = 990.1 / (1 + exp(-(U + 2.436) / 14.12));
  dxks = (xsss-grm%xks) / tauxs;
    
  ! I_to: Transient Outward K Current
  !Slow component
  xtoss = 1 / (1 + exp(-(U - 19.0) / 13));
  ytoss = 1 / (1 + exp((U + 19.5) / 5));
  tauxtos = 9 / (1 + exp((U + 3.0) / 15)) + 0.5;
  tauytos = 800 / (1 + exp((U + 60.0) / 10)) + 30;
  dxtos = (xtoss - grm%xtos) / tauxtos;
  dytos = (ytoss - grm%ytos) / tauytos;
  !Fast component
  tauxtof = 8.5 * exp(-((U + 45) / 50)**2) + 0.5;
  tauytof = 85 * exp((-(U + 40)**2 / 220)) + 7;
  dxtof = (xtoss - grm%xtof) / tauxtof;
  dytof = (ytoss - grm%ytof) / tauytof;

  ! I_Ca: L-type Calcium Current
  dss = 1 / (1 + exp(-(U + 5) / 6.0));
  taud = dss * (1 - exp(-(U + 5) / 6.0)) / (0.035 * (U + 5));
  fss = 1 / (1 + exp((U + 35) / 9)) + 0.6 / (1 + exp((50 - U) / 20));
  tauf = 1 / (0.0197 * exp(-(0.0337 * (U + 14.5))**2) + 0.02);
  dd = (dss - grm%d) / taud;
  df = (fss - grm%f) / tauf;
  dfCaBJ = 1.7 * grm%CaJ * (1 - grm%fCaBJ) - 11.9e-3 * grm%fCaBJ;
  dfCaBSL = 1.7 * grm%CaSL * (1 - grm%fCaBSL) - 11.9e-3 * grm%fCaBSL;
  
  ! SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
  MaxSR = 15; 
  MinSR = 1;
  kCaSR = MaxSR - (MaxSR - MinSR) / (1 + (p_ec50SR / grm%CaSR)**2.5);
  koSRCa = p_koCa / kCaSR;
  kiSRCa = p_kiCa * kCaSR;
  RI = 1 - grm%RyRr - grm%RyRo - grm%RyRi;
  dRyRr = (p_kim * RI - kiSRCa * grm%CaJ * grm%RyRr) - &
        (koSRCa * grm%CaJ**2 * grm%RyRr - p_kom * grm%RyRo);
  dRyRo = (koSRCa * grm%CaJ**2 * grm%RyRr - p_kom * grm%RyRo) - &
        (kiSRCa * grm%CaJ * grm%RyRo - p_kim * grm%RyRi);
  dRyRi = (kiSRCa * grm%CaJ * grm%RyRo - p_kim * grm%RyRi) - &
        (p_kom * grm%RyRi - koSRCa * grm%CaJ**2 * RI);
  
  !Update gates
  grm%m = grm%m + dm * dt;
  grm%h = grm%h + dh * dt;
  grm%j = grm%j + dj * dt;
  
  grm%xkr = grm%xkr + dxkr * dt;    
  grm%xks = grm%xks + dxks * dt;
  
  grm%xtof = grm%xtof + dxtof * dt;
  grm%ytof = grm%ytof + dytof * dt;
  grm%xtos = grm%xtos + dxtos * dt;
  grm%ytos = grm%ytos + dytos * dt;

  grm%d = grm%d + dd * dt;
  grm%f = grm%f + df * dt;
  grm%fCaBJ = grm%fCaBJ + dfCaBJ * dt;
  grm%fCaBSL = grm%fCaBSL + dfCaBSL * dt;
  
  grm%RyRr = grm%RyRr + dRyRr * dt;
  grm%RyRo = grm%RyRo + dRyRo * dt;
  grm%RyRi = grm%RyRi + dRyRi * dt;
  
  return
end subroutine gates
!------------------------------------------------------------------------------
subroutine currents ( U, grm, Qion, prm, cur) 
!------------------------------------------------------------------------------
! This function computes currents and concentrations for the Grandi Model
!
!
! XXX:AA-BB, 2008
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: U
  type(t_grm), intent(in)   :: grm
  real(rp),    intent(out)  :: Qion
  type(t_prm), intent(in)   :: prm
  type(t_cur), intent(out)  :: cur
  !.
  real(rp):: ENaJunc, ENaSL, EK, Eks, ECaJunc, ECaSL, ECl, sigma, &
             fNaK, gkr, rkr, kpkp, aK1, bK1, K1ss, KaJunc,        &
             KaSL, s1Junc, s2Junc, s3Junc, s1SL, s2SL, s3SL,      &
             IbarCaJ, IbarCaSL, IbarK, IbarNaJ, IbarNaSL

  !. Nerst Potentials
  ENaJunc = p_RTF * log(p_Nao / grm%NaJ);
  ENaSL = p_RTF * log(p_Nao / grm%NaSL);
  EK = p_RTF * log(p_Ko / grm%Ki);
  Eks = p_RTF * log((p_Ko + p_pNaK * p_Nao) / &
        (grm%Ki + p_pNaK * grm%Nai));
  ECaJunc = p_RTF / 2 * log(p_Cao / grm%CaJ);
  ECaSL = p_RTF / 2 * log(p_Cao / grm%CaSL);
  ECl = p_RTF * log(p_Cli / p_Clo);
  
  ! INa: Fast Na Current
  
  cur%INaJunc = prm%A_GNa * p_FJunc * p_GNa * grm%m**3 * grm%h * grm%j * (U - ENaJunc);
  cur%INaSL = prm%A_GNa * p_FSL * p_GNa * grm%m**3 * grm%h * grm%j * (U - ENaSL);
  cur%INa = cur%INaJunc + cur%INaSL;

  ! I_nabk: Na Background Current
  cur%INaBkJunc = prm%A_GNabk * p_FJunc * p_GNaB * (U - ENaJunc);
  cur%INaBkSL = prm%A_GNabk * p_FSL * p_GNaB * (U - ENaSL);
  cur%INaBk = cur%INaBkJunc + cur%INaBkSL;

  ! I_nak: Na/K Pump current
  sigma = (exp(p_Nao / 67.3) - 1) / 7;
  fNaK = 1 / (1 + 0.1245 * exp(-0.1 * U * p_iRTF) + &
        0.0365 * sigma * exp(-U*p_iRTF));
  cur%INaKJunc = prm%A_GNaK * p_FJunc * p_IbarNaK * fnak * p_Ko / &
        (1 + (p_KmNaip / grm%NaJ)**4) / (p_Ko + p_KmKo);
  cur%INaKSL = prm%A_GNaK * p_FSL * p_IbarNaK * fnak * p_Ko / &
        (1 + (p_KmNaip / grm%NaSL)**4) / (p_Ko + p_KmKo);
  cur%INaK = cur%INaKJunc + cur%INaKSL;

  ! I_kr: Rapidly Activating K Current
  gkr = 1.0 * 0.035 *sqrt(p_Ko / 5.4);
  rkr = 1 / (1 + exp((U + 74) / 24));
  cur%Ikr = prm%A_GKr * gkr * grm%xkr * rkr * (U - EK);
 
  ! I_ks: Slowly Activating K Current
  cur%IksJunc = prm%A_GKs * p_FJunc * p_GKsJunc * grm%xks**2 * (U - Eks);
  cur%IksSL = prm%A_GKs * p_FSL * p_GKsSL * grm%xks**2 * (U - Eks);
  cur%Iks = cur%IksJunc + cur%IksSL;
  
  ! I_kp: Plateau K Current
  kpkp = 1 / (1 + exp(7.488 - U / 5.98));
  cur%IkpJunc = prm%A_GKp * p_FJunc * p_Gkp * kpkp * (U - EK);
  cur%IkpSL = prm%A_GKp * p_FSL * p_Gkp * kpkp * (U - EK);
  cur%Ikp = cur%IkpJunc + cur%IkpSL;
  
  ! I_to: Transient Outward K Current
  ! Slow component
  cur%Itos = prm%A_Gto * prm%p_Gtos * grm%xtos * grm%ytos * (U - EK);
  ! Fast component
  cur%Itof = prm%A_Gto * prm%p_Gtof * grm%xtof * grm%ytof * (U - EK);
    
  cur%Ito = cur%Itos + cur%Itof;

  ! I_K1: Inward Rectifier K Current
  aK1 = 1.02 / (1 + exp(0.2385 * (U - EK - 59.215)));
  bK1 =(0.49124 * exp(0.08032 * (U + 5.476 - EK)) + &
        exp(0.06175 * (U - EK - 594.31))) / (1 + exp(-0.5143 * (U - EK + 4.753)));
  K1ss = aK1/(aK1+bK1);
  cur%IK1 = prm%A_GK1 * 0.35 * sqrt(p_Ko / 5.4) * K1ss * (U - EK);

  ! I_ClCa: Ca-activated Cl Current
  cur%IClCaJunc = prm%A_GClCa * p_FJunc * p_GClCa / (1 + p_KdClCa / grm%CaJ) * (U - ECL);
  cur%IClCaSL = prm%A_GClCa * p_FSL * p_GClCa / (1 + p_KdClCa / grm%CaSL) * (U - ECL);
  cur%IClCa = cur%IClCaJunc + cur%IClCaSL;

  ! I_Clbk: Background Cl Current
  cur%IClBk = prm%A_GClbk * p_GClB * (U - ECl);
  
  ! I_Ca: L-type Calcium Current
     
  IbarCaJ = p_pCa * 4 * (U * p_F * p_iRTF) * (0.341 * grm%CaJ * &
        exp(2 * U * p_iRTF) - 0.341 * p_Cao) / (exp(2 * U * p_iRTF) - 1);
  IbarCaSL = p_pCa * 4 * (U * p_F * p_iRTF) * (0.341 * grm%CaSL * &
        exp(2 * U * p_iRTF) - 0.341 * p_Cao) / (exp(2 * U * p_iRTF) - 1);
  IbarK = p_pK * (U * p_F * p_iRTF) * (0.75 * grm%Ki * &
        exp(U * p_iRTF) - 0.75 * p_Ko) / (exp(U * p_iRTF) - 1);
  IbarNaJ = p_pNa * (U * p_F * p_iRTF) * (0.75 * grm%NaJ * &
        exp(U * p_iRTF) - 0.75 * p_Nao) / (exp(U * p_iRTF) - 1);
  IbarNaSL = p_pNa * (U * p_F * p_iRTF) * (0.75 * grm%NaSL * &
        exp(U * p_iRTF) - 0.75 * p_Nao) / (exp(U * p_iRTF) - 1);

  cur%ICaJunc = prm%A_GCaL * (p_FJuncCaL * IbarCaJ * grm%d * grm%f * &
        (1 - grm%fCaBJ)) * 0.45 * 1;
  cur%ICaSL = prm%A_GCaL * (p_FSLCaL * IbarCaSL * grm%d * grm%f * &
        (1 - grm%fCaBSL)) * 0.45 * 1;
  cur%ICa = cur%ICaJunc + cur%ICaSL;
  
  cur%ICaK = prm%A_GCaL * (IbarK * grm%d * grm%f * (p_FJuncCaL * (1 - grm%fCaBJ) + &
        p_FSLCaL * (1 - grm%fCaBSL))) * 0.45 * 1;
        
  cur%ICaNaJunc = prm%A_GCaL * (p_FJuncCaL * IbarNaJ * grm%d * grm%f * &
        (1 - grm%fCaBJ)) * 0.45 * 1;
  cur%ICaNaSL = prm%A_GCaL * (p_FSLCaL * IbarNaSL * grm%d * grm%f * &
        (1 - grm%fCaBSL)) * 0.45 * 1;  
  cur%ICaNa = cur%ICaNaJunc + cur%ICaNaSL;

  cur%ICaL = cur%ICa + cur%ICaK + cur%ICaNa;

  ! I_ncx: Na/Ca Exchanger current
  KaJunc = 1 / (1 + (p_Kdact / grm%CaJ)**2);
  KaSL = 1 / (1 + (p_Kdact / grm%CaSL)**2);
  s1Junc = exp(p_nu * U * p_iRTF) * grm%NaJ**3 * p_Cao;
  s1SL = exp(p_nu * U * p_iRTF) * grm%NaSL**3 * p_Cao;
  s2Junc = exp((p_nu - 1) * U * p_iRTF) * p_Nao**3 * grm%CaJ;
  s3Junc = p_KmCai * p_Nao**3 * (1 + (grm%NaJ / p_KmNai)**3) + &
        p_KmNao**3 * grm%CaJ * (1 + grm%CaJ / p_KmCai) + &
        p_KmCao * grm%NaJ**3 + grm%NaJ**3 * p_Cao + &
        p_Nao**3 * grm%CaJ;
  s2SL = exp((p_nu - 1) * U * p_iRTF) * p_Nao**3 * grm%CaSL;
  s3SL = p_KmCai * p_Nao**3 * (1 + (grm%NaSL / p_KmNai)**3) + &
        p_KmNao**3 * grm%CaSL * (1 + grm%CaSL / p_KmCai) + &
        p_KmCao * grm%NaSL**3 + grm%NaSL**3 * p_Cao + &
        p_Nao**3 * grm%CaSL;

  cur%IncxJunc = prm%A_Gncx * p_FJunc * p_IbarNCX * KaJunc * (s1Junc - s2Junc) / &
        s3Junc / (1 + p_ksat * exp((p_nu - 1) * U * p_iRTF));
  cur%IncxSL = prm%A_Gncx * p_Fsl * p_IbarNCX * KaSL * (s1SL - s2SL) / s3SL / &
        (1 + p_ksat * exp((p_nu - 1) * U * p_iRTF));
  cur%Incx = cur%IncxJunc + cur%IncxSL;

  ! I_pCa: Sarcolemmal Ca Pump Current
  cur%IpCaJunc = prm%A_GpCa * p_FJunc * p_IbarPMCA * grm%CaJ**1.6 / &
        (p_KmPCa**1.6 + grm%CaJ**1.6);
  cur%IpCaSL = prm%A_GpCa * p_FSL * p_IbarPMCA * grm%CaSL**1.6 / &
        (p_KmPCa**1.6 + grm%CaSL**1.6);
  cur%IpCa = cur%IpCaJunc + cur%IpCaSL;

  ! I_Ca_bk: Background Ca Current
  cur%ICaBkJunc = prm%A_GCabk * p_FJunc * p_GCaB * (U - ECaJunc);
  cur%ICaBkSL = prm%A_GCabk * p_FSL * p_GCaB * (U - ECaSL);
  cur%ICaBk = cur%ICaBkJunc + cur%ICaBkSL;
     
  ! Sodium Concentrations
  cur%INatotJunc = cur%INaJunc + cur%INaBkJunc + 3 * cur%IncxJunc + &
        3 * cur%INaKJunc + cur%ICaNaJunc;
  cur%INatotSL = cur%INaSL + cur%INaBkSL + 3 * cur%IncxSL + &
        3 * cur%INaKSL + cur%ICaNaSL;

  ! Potassium concentration
  cur%IKtot = cur%Ito + cur%Ikr + cur%Iks + cur%Ik1 - 2 * cur%INaK + &
        cur%ICaK + cur%Ikp;
    
  ! Calcium concentrations
  cur%ICatotJunc = cur%ICaJunc + cur%ICaBkJunc + cur%IpCaJunc -  &
                   2 * cur%IncxJunc;
  cur%ICatotSL = cur%ICaSL + cur%ICaBkSL + cur%IpCaSL -  &
                   2 * cur%IncxSL;
  ! Membrane Potential
  cur%INatot = cur%INatotJunc + cur%INatotSL;
  cur%ICltot = cur%IClCa + cur%IClBk;
  cur%ICatot = cur%ICatotJunc + cur%ICatotSL;
  cur%Itot = cur%INatot + cur%ICltot + cur%ICatot + cur%IKtot;
   
  !------------------------------------
  Qion = cur%Itot;

  return
  
end subroutine currents
!-------------------------------------------------------------------------------
subroutine Grandi_A_P01 (ict,dt,U,Iion,v_prm,v_gr,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict   !.Cell Model 
  real(rp),    intent(in)    :: dt, U
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp), intent(inout)    :: v_gr(nvar_gr)
  real(rp),    intent(out)   :: v_cr(:)
  !.
  type(t_grm)                :: grm
  type (t_prm)               :: param
  type(t_cur)                :: crr
  !
  call put_me_struct(v_gr,grm)
  call put_param(v_prm,param)
  !
  call currents ( U, grm, Iion, param, crr)
  call concentrations ( dt, crr, param, grm)
  call gates( dt, U, grm)
  !  
  call get_me_struct(grm,v_gr)
  !.
  v_cr(1:ncur_gr) =(/ crr%INa, crr%INaBk, crr%INaK, crr%Ikr, crr%Iks, crr%Ikp,   &
                 crr%Itos, crr%Itof, crr%Ito, crr%Ik1, crr%IclCa,crr%IClBk, &
                 crr%ICa, crr%ICaK, crr%ICaNa, crr%ICaL, crr%Incx, crr%IpCa,&
                 crr%ICaBk, crr%IKtot, crr%INatot, crr%ICltot, crr%ICatot,  &
                 crr%Itot /)
  
  return
end subroutine Grandi_A_P01
!-------------------------------------------------------------------------------
end module mod_grandi
!-------------------------------------------------------------------------------
