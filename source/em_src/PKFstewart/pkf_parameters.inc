!------------------------------------------------------------------------------
! This are the parameters for the Stewart Model for Purkinje Fibers
! Stewart P, Aslanidi OV, Noble D, Noble PJ, Boyett MR, Zhang H.
! Phil Trans R Soc A 367:2225-2255, 2009.
! Constants
!
! Parameters that can be modified through the input file
!
  real(rp), parameter, private :: p_Gsus  = 0.0227;          ! nS pF^-1
  real(rp), parameter, private :: p_GfK   = 0.0234346;       ! nS pF^-1
  real(rp), parameter, private :: p_GfNa  = 0.0145654;       ! nS pF^-1
  !.--Parameters for IKs -------------------------------------------------------
  real(rp), parameter, private :: p_pKNa  = 0.03;            ! nS/pF
  !.--Parameters for IKr -------------------------------------------------------
  real(rp), parameter, private :: p_GKr   = 0.6*0.153;          ! nS/pF
  !.--Parameters for IK1 -------------------------------------------------------
  real(rp), parameter, private :: p_GK1   = 0.0650;          ! nS/pF
  !.--Parametros de corriente IKs e Ito ----------------------------------------
  real(rp), parameter, private :: p_GKs   =  0.6*0.392;          ! IKs conductance
  real(rp), parameter, private :: p_Gto   =  0.08184;        ! Ito conductance
  !.--Parameters for INa -------------------------------------------------------
  real(rp), parameter, private :: p_GNa   = 130.5744;        ! nS/pF
  !.--Parameters for ICaL ------------------------------------------------------
  real(rp), parameter, private :: p_GCaL  = 3.98E-5;         ! cm ms^-1 micro_F^-1
  !.--Parameter for IbNa -------------------------------------------------------
  real(rp), parameter, private :: p_GbNa  = 2.9E-4;          ! nS/pF
  !.--Parameter for IbCa -------------------------------------------------------
  real(rp), parameter, private :: p_GbCa  = 5.92E-4;         ! nS/pF
  !.--Parameter for IpCa -------------------------------------------------------
  real(rp), parameter, private :: p_GpCa  = 0.1238;          ! nS/pF
  !.--Parameter for IpK --------------------------------------------------------
  real(rp), parameter, private :: p_GpK   = 0.0146;          ! nS/pF
  !.--Parameters for INaK ------------------------------------------------------
  real(rp), parameter, private :: p_PNaK  = 2.724;           ! pA/pF
  !.--Parameter for INaCa ------------------------------------------------------
  real(rp), parameter, private :: p_kNaCa = 1000.0;          ! pA/pF
  !.--External concentrations --------------------------------------------------
  real(rp), parameter, private :: p_Ko    = 5.4;             ! mM
  real(rp), parameter, private :: p_Nao   = 140.0;           ! mM
  real(rp), parameter, private :: p_Cao   = 2.0;             ! mM
!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_R     =  8314.472;       !. JK^-1mol^-1  
  real(rp), parameter, private :: p_T     =   310.0;         !. K
  real(rp), parameter, private :: p_F     = 96485.3415;      !. C/mol
  real(rp), parameter, private :: p_RTF   = p_R*p_T/p_F;     !. J/C
  real(rp), parameter, private :: p_iRTF  = 1.0/p_RTF;
  !.--Capacitance --------------------------------------------------------------
  real(rp), parameter, private :: p_Cap   = 0.185;           !
  !.--Intracellular Volume -----------------------------------------------------
  real(rp), parameter, private :: p_Vc    = 0.016404;        ! micro_m^3
  real(rp), parameter, private :: p_Vsr   = 0.001094;        ! micro_m^3
  real(rp), parameter, private :: p_Vss   = 0.00005468;      ! micro_m^3
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_iVcF  = 1.0/(p_Vc*p_F);
  real(rp), parameter, private :: p_iVcF2 = 0.5*p_iVcF;
  real(rp), parameter, private :: p_iVssF2= 0.5/(p_Vss*p_F);
  !.--Correction factor --------------------------------------------------------
  real(rp),  parameter,private :: corFac  = (p_T/35.0-55.0/7.0)*sqrt(p_Ko/5.4);
  !.--Parameters for INaK ------------------------------------------------------
  real(rp), parameter, private :: p_KmK   = 1.0;             ! mM
  real(rp), parameter, private :: p_KmNa  = 40.0;            ! mM
  !.--Parameter for INaCa ------------------------------------------------------
  real(rp), parameter, private :: p_KmNai = 87.5;            ! mM
  real(rp), parameter, private :: p_KmCa  = 1.38;            ! mM
  real(rp), parameter, private :: p_ksat  = 0.1;             !
  real(rp), parameter, private :: p_gam   = 0.35;            !
  real(rp), parameter, private :: p_alf   = 2.5;             !
  !.--Parameter for IpCa -------------------------------------------------------
  real(rp), parameter, private :: p_KpCa  = 5.0E-4;          ! mM
  !.--Parameter for IpK --------------------------------------------------------
  !.--Intracellular calcium flux dynamics --------------------------------------
  real(rp), parameter, private :: p_Vmxu  = 6.375E-3;        ! mM/ms
  real(rp), parameter, private :: p_Kup   = 2.5E-4;          ! mM
  real(rp), parameter, private :: p_Vrel  = 0.102;           ! mM/ms
  real(rp), parameter, private :: p_k1p   = 0.15;            ! mM^-2 ms^-1
  real(rp), parameter, private :: p_k2p   = 0.045;           ! mM^-1 ms^-1
  real(rp), parameter, private :: p_k3    = 0.06;            ! ms^-1
  real(rp), parameter, private :: p_k4    = 0.005;        ! ms^-1
  real(rp), parameter, private :: p_EC    = 1.5;             ! mM
  real(rp), parameter, private :: p_maxsr = 2.5;             !
  real(rp), parameter, private :: p_minsr = 1.0;             !
  real(rp), parameter, private :: p_Vleak = 3.6E-4;          ! mM/ms
  real(rp), parameter, private :: p_Vxfer = 3.8E-3;          ! mM/ms
  !.--Calcium buffering dynamics -----------------------------------------------
  real(rp), parameter, private :: p_Bufc  = 0.2;             ! mM
  real(rp), parameter, private :: p_Kbufc = 0.001;           ! mM
  real(rp), parameter, private :: p_Bufsr = 10.0;            ! mM
  real(rp), parameter, private :: p_Kbufsr= 0.3;             ! mM
  real(rp), parameter, private :: p_Bufss = 0.4;             ! mM
  real(rp), parameter, private :: p_Kbufss= 2.5E-4;          ! mM
  !.----------------------------------------------------------------------------

!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS 30 minutes at 1Hz --------------------------------------
!------------------------------------------------------------------------------
 real(rp), parameter :: Vi_PKF = -7.1200694011e+01;
 real(rp), parameter :: Cai_i =  1.0708900000e-04;                
 real(rp), parameter :: CaSR_i =  3.3761790149e+00;
 real(rp), parameter :: CaSS_i =  3.9382140000e-04;
 real(rp), parameter :: Nai_i =  9.4802393985e+00;
 real(rp), parameter :: Ki_i =  1.3605969762e+02;
 real(rp), parameter :: m_i =  2.8779879200e-02;
 real(rp), parameter :: h_i =  2.4644027690e-01;
 real(rp), parameter :: j_i =  2.6911387040e-01;
 real(rp), parameter :: xs_i =  8.4696022000e-03;
 real(rp), parameter :: r_i =  8.9144040000e-04;
 real(rp), parameter :: s_i =  9.6839680230e-01;
 real(rp), parameter :: d_i =  2.1879600000e-04;
 real(rp), parameter :: f_i =  9.8129344410e-01;
 real(rp), parameter :: f2_i =  9.9640307760e-01;
 real(rp), parameter :: fcass_i =  9.9995520700e-01;
 real(rp), parameter :: rr_i =  9.8847256070e-01;
 real(rp), parameter :: oo_i =  3.0730000000e-07;
 real(rp), parameter :: xr1_i =  8.1553206000e-03;
 real(rp), parameter :: xr2_i =  3.3188887530e-01;
 real(rp), parameter :: y_i =  4.3360756000e-02;
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: ST_PK_mod = 11;  ! Stewart Purkinje model    (PKH)
  integer(ip),parameter :: nvar_pkf   = 20;  ! Number of state variables
  integer(ip),parameter :: ncur_pkf   = 16;  ! Number of currents
  integer(ip),parameter :: np_pkf     = 19;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.  
  integer(ip), parameter    :: m_stpPKF  =    5
  real(rp),    parameter    :: m_dvdtPKF =  1.0
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS OLD   --------------------------------------------------
!------------------------------------------------------------------------------
!  real(rp), parameter :: Vi_PKF = -71.373756412088_rp
!  real(rp), parameter :: Cai_i  = 0.0001069094_rp         ! mM
!  real(rp), parameter :: CaSR_i = 3.3620576885_rp         ! mM
!  real(rp), parameter :: CaSS_i = 0.0003895806_rp         ! mM
!  real(rp), parameter :: Nai_i  = 9.6215553749_rp;        ! mM
!  real(rp), parameter :: Ki_i   = 135.9207303479_rp;      ! mM
!  real(rp), parameter :: m_i    = 0.0278678365_rp;        !
!  real(rp), parameter :: h_i    = 0.2517545296_rp;        !
!  real(rp), parameter :: j_i    = 0.2740563214_rp;         !
!  real(rp), parameter :: xs_i   = 0.0083801130_rp;         !
!  real(rp), parameter :: r_i    = 0.0008799035_rp;         !
!  real(rp), parameter :: s_i    = 0.9687677986_rp;         !
!  real(rp), parameter :: d_i    = 0.0002137615_rp;         !
!  real(rp), parameter :: f_i    = 0.9818086038_rp;         !
!  real(rp), parameter :: f2_i   = 0.9964813276_rp;         ! 
!  real(rp), parameter :: fcass_i= 0.9999559367_rp;         !    
!  real(rp), parameter :: rr_i   = 0.9885231424_rp;         !   
!  real(rp), parameter :: oo_i   = 0.0000003003_rp;         ! 
!  real(rp), parameter :: xr1_i  = 0.0078265548_rp;         !
!  real(rp), parameter :: xr2_i  = 0.3335012026_rp;         !
!  real(rp), parameter :: y_i    = 0.0444173946_rp;         !
