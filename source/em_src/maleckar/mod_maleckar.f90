! This module implements the Maleckar Model
!
! 01-09-2011: Maleckar_A_P01 first implementation (CST)
!
!------------------------------------------------------------------------------
module mod_maleckar
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'maleck_parameters.inc'
  !.
  type t_maleck
    sequence
    real(rp) :: m, h1, h2, dl, fl1, fl2, rr, s, uakur, uikur, pa, n, nai, ki, cai,  &
                caup, carel, cado, oc, otc, otmgc, otnmgmg, ocalse, f1, f2, nac,    &   
                kc, cac
  end type t_maleck
  !.
  public  :: get_parameter_MLK , ic_Maleck, Maleckar_A_P01
  private :: put_param, f_m, f_h_total, f_na, f_ca, f_It, f_Ikur, f_IKr, f_IKs,  &
             f_IK1, f_IKACh, f_basal, f_ICAP, f_INAK, f_INACA, f_SRCa, f_ICab, f_Icon, &
             f_Cleft
  !.
  type, public:: t_prm
    private
    real(rp)  :: p_pna    ! 1	
    real(rp)  :: p_gcal   ! 2  
    real(rp)  :: p_gt     ! 3
    real(rp)  :: p_gkur   ! 4
    real(rp)  :: p_gkr    ! 5 
    real(rp)  :: p_gks    ! 6 
    real(rp)  :: p_gk1    ! 7 
    real(rp)  :: p_gnab   ! 8
    real(rp)  :: p_gcab   ! 9 
    real(rp)  :: p_taufl1 ! 10
    real(rp)  :: p_Vhm    ! 11
    real(rp)  :: p_Vrm    ! 12
    !real(rp)  :: p_Ddof  ! 13
    real(rp)  :: p_ACh    ! 13
  end type t_prm
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_MALEC(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)', advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'h1 '
    write(lu_st,'(A)',advance='no') 'h2 '
    write(lu_st,'(A)',advance='no') 'dl '
    write(lu_st,'(A)',advance='no') 'fl1 '
    write(lu_st,'(A)',advance='no') 'fl2 '
    write(lu_st,'(A)',advance='no') 'rr '
    write(lu_st,'(A)',advance='no') 's '
    write(lu_st,'(A)',advance='no') 'uakur '
    write(lu_st,'(A)',advance='no') 'uikur '
    write(lu_st,'(A)',advance='no') 'pa '
    write(lu_st,'(A)',advance='no') 'n '
    write(lu_st,'(A)',advance='no') 'Nai '
    write(lu_st,'(A)',advance='no') 'Ki '
    write(lu_st,'(A)',advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'Caup '
    write(lu_st,'(A)',advance='no') 'Carel '
    write(lu_st,'(A)',advance='no') 'Cabo '
    write(lu_st,'(A)',advance='no') 'oc '
    write(lu_st,'(A)',advance='no') 'otc '
    write(lu_st,'(A)',advance='no') 'otmgc '
    write(lu_st,'(A)',advance='no') 'otnmgmg '
    write(lu_st,'(A)',advance='no') 'ocalse '
    write(lu_st,'(A)',advance='no') 'f1 '
    write(lu_st,'(A)',advance='no') 'f2 '
    write(lu_st,'(A)',advance='no') 'Nac '
    write(lu_st,'(A)',advance='no') 'Kc '
    write(lu_st,'(A)',advance='no') 'Cac '
end subroutine write_state_MALEC
!------------------------------------------------------------------------------!
subroutine write_current_MALEC(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)', advance='no') 'INa '
    write(lu_cr,'(A)',advance='no') 'ICaL '
    write(lu_cr,'(A)',advance='no') 'ek '
    write(lu_cr,'(A)',advance='no') 'It '
    write(lu_cr,'(A)',advance='no') 'Ikur '
    write(lu_cr,'(A)',advance='no') 'IKr '
    write(lu_cr,'(A)',advance='no') 'IKs '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'IKACh '
    write(lu_cr,'(A)',advance='no') 'ena '
    write(lu_cr,'(A)',advance='no') 'IbNa '
    write(lu_cr,'(A)',advance='no') 'IbCa '
    write(lu_cr,'(A)',advance='no') 'Ib '
    write(lu_cr,'(A)',advance='no') 'ICap '
    write(lu_cr,'(A)',advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'INaCa '
    write(lu_cr,'(A)',advance='no') 'Iup '
    write(lu_cr,'(A)',advance='no') 'Irel '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_MALEC
!------------------------------------------------------------------------------!
function get_parameter_MLK (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_maleck)

  select case (tcell)
  case (MALEC_mod)
    v_prm = (/ p_pna, p_gcal, p_gt, p_gkur, p_gkr, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!
  case (MALEC_RA_PM_mod)
    v_prm = (/ p_pna, p_gcal_RA_PM, p_gt_RA_PM, p_gkur, p_gkr_RA_PM, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!		   
  case (MALEC_CT_BB_mod)
    v_prm = (/ p_pna, p_gcal_CT_BB, p_gt_CT_BB, p_gkur, p_gkr_CT_BB, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case (MALEC_RAA_mod)
    v_prm = (/ p_pna, p_gcal_RAA, p_gt_RAA, p_gkur, p_gkr_RAA, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case (MALEC_TVR_mod)
    v_prm = (/ p_pna, p_gcal_TVR, p_gt_TVR, p_gkur, p_gkr_TVR, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case (MALEC_LA_mod)
    v_prm = (/ p_pna, p_gcal_LA, p_gt_LA, p_gkur, p_gkr_LA, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case (MALEC_PV_mod)
    v_prm = (/ p_pna, p_gcal_PV, p_gt_PV, p_gkur, p_gkr_PV, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case (MALEC_LAA_mod)
    v_prm = (/ p_pna, p_gcal_LAA, p_gt_LAA, p_gkur, p_gkr_LAA, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case (MALEC_MVR_mod)
    v_prm = (/ p_pna, p_gcal_MVR, p_gt_MVR, p_gkur, p_gkr_MVR, p_gks, p_gk1, &
               p_gnab, p_gcab, p_taufl1, p_Vhm, p_Vrm, p_ACh/)!			   
  case default
    write (*,10) tcell; stop
  end select
  !.
  return
  10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_MLK
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: v_prm(:)
  type (t_prm)            :: param
  param%p_pna    = v_prm(1)  ! 1	
  param%p_gcal   = v_prm(2)  ! 2  
  param%p_gt     = v_prm(3)  ! 3
  param%p_gkur   = v_prm(4)  ! 4
  param%p_gkr    = v_prm(5)  ! 5 
  param%p_gks    = v_prm(6)  ! 6 
  param%p_gk1    = v_prm(7)  ! 7 
  param%p_gnab   = v_prm(8)  ! 8
  param%p_gcab   = v_prm(9)  ! 9 
  param%p_taufl1 = v_prm(10) ! 10 
  param%p_Vhm    = v_prm(11) ! 11 
  param%p_Vrm    = v_prm(12) ! 12 
  !param%p_Ddof   = v_prm(13) ! 13
  param%p_ACh    = v_prm(13) ! 13 
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_Maleck(ict) result(maleck_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Maleckar Model
!------------------------------------------------------------------------------
  implicit none

  !.
  integer (ip), intent(in)  :: ict
  real(rp)                  :: maleck_ic(nvar_maleck)
  
  select case (ict)
  case(MALEC_mod) !.----ATRIA
    maleck_ic = (/ mic, h1ic, h2ic, dlic, fl1ic, fl2ic, rric, sic, uakuric, uikuric, &
                paic, nic, naiic, kiic, caiic, caupic, carelic, cadoic, ocic,   &
                otcic, otmgcic, otnmgmgic, ocalseic, f1ic, f2ic, nacic, kcic,   &
                cacic/)!
  case(MALEC_RA_PM_mod) !.----_RA_PM
    maleck_ic = (/ mic_RA_PM, h1ic_RA_PM, h2ic_RA_PM, dlic_RA_PM, fl1ic_RA_PM, fl2ic_RA_PM, rric_RA_PM, sic_RA_PM, uakuric_RA_PM, uikuric_RA_PM, &
                paic_RA_PM, nic_RA_PM, naiic_RA_PM, kiic_RA_PM, caiic_RA_PM, caupic_RA_PM, carelic_RA_PM, cadoic_RA_PM, ocic_RA_PM,   &
                otcic_RA_PM, otmgcic_RA_PM, otnmgmgic_RA_PM, ocalseic_RA_PM, f1ic_RA_PM, f2ic_RA_PM, nacic_RA_PM, kcic_RA_PM,   &
                cacic_RA_PM/)!				
  case(MALEC_CT_BB_mod) !.----_CT_BB
    maleck_ic = (/ mic_CT_BB, h1ic_CT_BB, h2ic_CT_BB, dlic_CT_BB, fl1ic_CT_BB, fl2ic_CT_BB, rric_CT_BB, sic_CT_BB, uakuric_CT_BB, uikuric_CT_BB, &
                paic_CT_BB, nic_CT_BB, naiic_CT_BB, kiic_CT_BB, caiic_CT_BB, caupic_CT_BB, carelic_CT_BB, cadoic_CT_BB, ocic_CT_BB,   &
                otcic_CT_BB, otmgcic_CT_BB, otnmgmgic_CT_BB, ocalseic_CT_BB, f1ic_CT_BB, f2ic_CT_BB, nacic_CT_BB, kcic_CT_BB,   &
                cacic_CT_BB/)!				
  case(MALEC_RAA_mod) !.----_RAA
    maleck_ic = (/ mic_RAA, h1ic_RAA, h2ic_RAA, dlic_RAA, fl1ic_RAA, fl2ic_RAA, rric_RAA, sic_RAA, uakuric_RAA, uikuric_RAA, &
                paic_RAA, nic_RAA, naiic_RAA, kiic_RAA, caiic_RAA, caupic_RAA, carelic_RAA, cadoic_RAA, ocic_RAA,   &
                otcic_RAA, otmgcic_RAA, otnmgmgic_RAA, ocalseic_RAA, f1ic_RAA, f2ic_RAA, nacic_RAA, kcic_RAA,   &
                cacic_RAA/)!				
  case(MALEC_TVR_mod) !.----_TVR
    maleck_ic = (/ mic_TVR, h1ic_TVR, h2ic_TVR, dlic_TVR, fl1ic_TVR, fl2ic_TVR, rric_TVR, sic_TVR, uakuric_TVR, uikuric_TVR, &
                paic_TVR, nic_TVR, naiic_TVR, kiic_TVR, caiic_TVR, caupic_TVR, carelic_TVR, cadoic_TVR, ocic_TVR,   &
                otcic_TVR, otmgcic_TVR, otnmgmgic_TVR, ocalseic_TVR, f1ic_TVR, f2ic_TVR, nacic_TVR, kcic_TVR,   &
                cacic_TVR/)!				
  case(MALEC_LA_mod) !.----_LA
    maleck_ic = (/ mic_LA, h1ic_LA, h2ic_LA, dlic_LA, fl1ic_LA, fl2ic_LA, rric_LA, sic_LA, uakuric_LA, uikuric_LA, &
                paic_LA, nic_LA, naiic_LA, kiic_LA, caiic_LA, caupic_LA, carelic_LA, cadoic_LA, ocic_LA,   &
                otcic_LA, otmgcic_LA, otnmgmgic_LA, ocalseic_LA, f1ic_LA, f2ic_LA, nacic_LA, kcic_LA,   &
                cacic_LA/)!				
  case(MALEC_PV_mod) !.----_PV
    maleck_ic = (/ mic_PV, h1ic_PV, h2ic_PV, dlic_PV, fl1ic_PV, fl2ic_PV, rric_PV, sic_PV, uakuric_PV, uikuric_PV, &
                paic_PV, nic_PV, naiic_PV, kiic_PV, caiic_PV, caupic_PV, carelic_PV, cadoic_PV, ocic_PV,   &
                otcic_PV, otmgcic_PV, otnmgmgic_PV, ocalseic_PV, f1ic_PV, f2ic_PV, nacic_PV, kcic_PV,   &
                cacic_PV/)!				
  case(MALEC_LAA_mod) !.----_LAA
    maleck_ic = (/ mic_LAA, h1ic_LAA, h2ic_LAA, dlic_LAA, fl1ic_LAA, fl2ic_LAA, rric_LAA, sic_LAA, uakuric_LAA, uikuric_LAA, &
                paic_LAA, nic_LAA, naiic_LAA, kiic_LAA, caiic_LAA, caupic_LAA, carelic_LAA, cadoic_LAA, ocic_LAA,   &
                otcic_LAA, otmgcic_LAA, otnmgmgic_LAA, ocalseic_LAA, f1ic_LAA, f2ic_LAA, nacic_LAA, kcic_LAA,   &
                cacic_LAA/)!				
  case(MALEC_MVR_mod) !.----_MVR
    maleck_ic = (/ mic_MVR, h1ic_MVR, h2ic_MVR, dlic_MVR, fl1ic_MVR, fl2ic_MVR, rric_MVR, sic_MVR, uakuric_MVR, uikuric_MVR, &
                paic_MVR, nic_MVR, naiic_MVR, kiic_MVR, caiic_MVR, caupic_MVR, carelic_MVR, cadoic_MVR, ocic_MVR,   &
                otcic_MVR, otmgcic_MVR, otnmgmgic_MVR, ocalseic_MVR, f1ic_MVR, f2ic_MVR, nacic_MVR, kcic_MVR,   &
                cacic_MVR/)!				
  case default
    maleck_ic = (/ mic, h1ic, h2ic, dlic, fl1ic, fl2ic, rric, sic, uakuric, uikuric, &
                paic, nic, naiic, kiic, caiic, caupic, carelic, cadoic, ocic,   &
                otcic, otmgcic, otnmgmgic, ocalseic, f1ic, f2ic, nacic, kcic,   &
                cacic/)!
  end select
  !.

  return
end function ic_Maleck
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_maleck), intent(in) :: str_me
  real(rp), intent(out)       :: v_me(nvar_maleck)
  
  v_me(1)  =   str_me%m 
  v_me(2)  =   str_me%h1 
  v_me(3)  =   str_me%h2
  v_me(4)  =   str_me%dl
  v_me(5)  =   str_me%fl1
  v_me(6)  =   str_me%fl2
  v_me(7)  =   str_me%rr
  v_me(8)  =   str_me%s
  v_me(9)  =   str_me%uakur
  v_me(10) =   str_me%uikur
  v_me(11) =   str_me%pa
  v_me(12) =   str_me%n
  v_me(13) =   str_me%nai
  v_me(14) =   str_me%ki
  v_me(15) =   str_me%cai
  v_me(16) =   str_me%caup
  v_me(17) =   str_me%carel
  v_me(18) =   str_me%cado
  v_me(19) =   str_me%oc
  v_me(20) =   str_me%otc
  v_me(21) =   str_me%otmgc
  v_me(22) =   str_me%otnmgmg
  v_me(23) =   str_me%ocalse
  v_me(24) =   str_me%f1
  v_me(25) =   str_me%f2
  v_me(26) =   str_me%nac
  v_me(27) =   str_me%kc
  v_me(28) =   str_me%cac
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)         :: v_me(:)
  type (t_maleck), intent(out) :: str_me
  str_me%m       = v_me(1)
  str_me%h1      = v_me(2)
  str_me%h2      = v_me(3)
  str_me%dl      = v_me(4)
  str_me%fl1     = v_me(5)
  str_me%fl2     = v_me(6)
  str_me%rr      = v_me(7)
  str_me%s       = v_me(8)
  str_me%uakur   = v_me(9)
  str_me%uikur   = v_me(10)
  str_me%pa      = v_me(11)
  str_me%n       = v_me(12)
  str_me%nai     = v_me(13)
  str_me%ki      = v_me(14)
  str_me%cai     = v_me(15)
  str_me%caup    = v_me(16)
  str_me%carel   = v_me(17)
  str_me%cado    = v_me(18)
  str_me%oc      = v_me(19)
  str_me%otc     = v_me(20)
  str_me%otmgc   = v_me(21)
  str_me%otnmgmg = v_me(22)
  str_me%ocalse  = v_me(23)
  str_me%f1      = v_me(24)
  str_me%f2      = v_me(25)
  str_me%nac     = v_me(26)
  str_me%kc      = v_me(27)
  str_me%cac     = v_me(28)
  return
end subroutine put_me_struct
! ======================================================================================
! ------------------------------------------------------------------------------------- 
! ===================================================================================== 
subroutine f_m(v,dt,m)
! ===================================================================================== 
! m activation gate  ----Ecuacion 4
! ===================================================================================== 
  implicit none
  real (rp),intent(in)   :: v,dt
  real (rp),intent(inout):: m
  real (rp)              ::taum,mm

  taum = (0.000042*exp(-((v+25.57)/28.8)**2))+0.000024;
  mm = 1.0/(1+exp((v+27.12)/(-8.21)));
  !m = m + (dt*((mm-m)/taum)); !<-----
  m=mm+(m-mm)*exp(-dt/(1000.0*taum));
  return
end subroutine f_m
! ------------------------------------------------------------------------------------- 
subroutine f_h_total(v,dt,prm,h1,h2)
! ===================================================================================== 
! h total Sodium Gate ----Ecuacion 5
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt
  type (t_prm)           :: prm
  real(rp),intent(inout) :: h1,h2
  real(rp)               :: tauh1,tauh2,hm

  tauh1 = (0.03_rp/(1.0_rp+exp((v+35.1)/(3.2))))+0.0003;
  tauh2 = (0.12_rp/(1.0_rp+exp((v+35.1)/(3.2))))+0.003; 
  hm = 1.0_rp/(1.0_rp+exp((v+prm%p_Vhm)/(5.3)));
  !h1 = h1 + (dt*((hm-h1)/tauh1)); 
  !h2 = h2 + (dt*((hm-h2)/tauh2)); 
  h1=hm+(h1-hm)*exp(-dt/(1000.0*tauh1));
  h2=hm+(h2-hm)*exp(-dt/(1000.0*tauh2));
  return
end subroutine f_h_total
! ------------------------------------------------------------------------------------- 
subroutine f_na(v,m,h1,h2,nac,nai,prm,INA,ena)
! ===================================================================================== 
! Na current  ----Ecuacion 3
! ===================================================================================== 
  implicit none
  real(rp),intent(in)   :: v,m,h1,h2,nac,nai
  type (t_prm)          :: prm
  real(rp),intent(out)  :: INA,ena
  real(rp)              :: aux1, aux2, FRT, invFRT

  FRT = (p_R*p_Te)/p_FA;
  invFRT = 1.0/FRT;
  ena = FRT*log(nac/nai);

  aux2 = (p_R*p_Te*(exp(v*invFRT)-1.0));
  aux1 = prm%p_pna*((m**3))*(0.9*h1 + 0.1*h2)*nac*v*(p_FA**2);
  INA = aux1*(exp((v-ena)*invFRT)-1.0)/aux2;
  return
end subroutine f_na
! ------------------------------------------------------------------------------------- 
subroutine f_ca(v,dt,cado,prm,dl,fl1,fl2,ICAL)
! ===================================================================================== 
! Ca current    ----- Ecuacion 7
! ===================================================================================== 
! Ca,L current 
! ------------------------------------------------------------------------------------- 
  implicit none
  real(rp),intent(in)    :: v,dt,cado
  type (t_prm)           :: prm
  real(rp),intent(inout) :: dl,fl1,fl2
  real(rp),intent(out)   :: ICAL
  real(rp)               :: taudl,dml,taufl1,taufl2,fml,fca

  fca   = cado/(cado+p_kca);
  ICAL = prm%p_gcal*dl*((fca*fl1)+((1.0-fca)*fl2))*(v-p_ecaapp);
  !
  taudl = (0.0027*exp(-(((v+35.0)/30.0)**2)))+0.002;
  dml = 1.0 /(1+exp((v+9.0)/(-5.8)));
  taufl1= (0.161*exp(-(((v+40.0)/14.4)**2)))+0.01;
  taufl2= (1.3323*exp(-(((v+40.0)/14.2)**2)))+0.0626;
  fml   = 1.0/(1+exp((v+27.4)/(7.1)));
 ! dl    = dl + (dt*((dml-dl)/taudl)); 	
 !fl1   = fl1 + dt*(fml-fl1)/(prm%p_taufl1*taufl1);
  !fl2   = fl2 + (dt*((fml-fl2)/taufl2));
  dl=dml+(dl-dml)*exp(-dt/(1000.0*taudl));
  fl1=fml+(fl1-fml)*exp(-dt/(1000.0*taufl1));
  fl2=fml+(fl2-fml)*exp(-dt/(1000.0*taufl2));
  return
end subroutine f_ca
! ------------------------------------------------------------------------------------- 
subroutine f_It(v,dt,kc,ki,prm,rr,s,ek,It)
! ===================================================================================== 
! It current ----- Ecuacion 13
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt,kc,ki
  type (t_prm)           :: prm
  real(rp),intent(inout) :: rr,s
  real(rp),intent(out)   :: ek,It
  real(rp)               :: taus,taur,rm,sm

  ek  = ((p_R*p_Te)/p_FA)* log(kc/ki);
  It  = prm%p_gt*rr*s*(v-ek); !It ajustado
  !
  taus= (0.025635*exp(-(((v+52.45)/(15.8827))**2)))+0.01414;
  taur= (0.0035*exp(-((v/30.0)**2)))+0.0015;
  rm  = 1.0/(1+exp((v-prm%p_Vrm)/(-11.0)));
  sm  = 1.0/(1+exp((v+40.5)/(11.5))); 
  !rr  = rr + (dt*((rm -rr)/taur)); 
  !s   = s + (dt*((sm-s)/taus));
  rr=rm+(rr-rm)*exp(-dt/(1000.0*taur));
  s=sm+(s-sm)*exp(-dt/(1000.0*taus));
  return
end subroutine f_It
! ------------------------------------------------------------------------------------- 
subroutine f_Ikur(v,dt,ek,prm,uakur,uikur,Ikur)
! ===================================================================================== 
! Ikur CURRENT           ------------Ecuacion 15
! =====================================================================================
  implicit none
  real(rp),intent(in)    :: v,dt,ek
  type (t_prm)           :: prm
  real(rp),intent(inout) :: uakur,uikur
  real(rp),intent(out)   :: Ikur
  real(rp)               :: tauuakur,tauuikur,uakurm,uikurm

  Ikur = prm%p_gkur*uakur*uikur*(v-ek);! Ikur ajustado gkur cte
  !
  tauuikur = (0.59/(1+exp((v+60.0)/(10.0))))+3.05;
  tauuakur = (0.009/(1+exp((v+5.0)/(12.0))))+0.0005;
  uakurm  = 1.0/(1+exp((v+6.0)/(-8.6)));
  uikurm = 1.0/(1+exp((v+7.5)/(10.0))); 
  !uakur = uakur + (dt*((uakurm -uakur)/tauuakur)); 
  !uikur = uikur + (dt*((uikurm -uikur)/tauuikur));
  uakur=uakurm+(uakur-uakurm)*exp(-dt/(1000.0*tauuakur));
  uikur=uikurm+(uikur-uikurm)*exp(-dt/(1000.0*tauuikur));
  return
end subroutine f_Ikur
! ------------------------------------------------------------------------------------- 
subroutine f_IKr(v,dt,ek,prm,pa,IKr)!,bdof)
! ===================================================================================== 
! IK delayed rectifier current ------------Ecuacion 19 y 20
! K,fast current
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt,ek
  type (t_prm)           :: prm
  real(rp),intent(inout) :: pa!, bdof
  real(rp),intent(out)   :: IKr
  real(rp)               :: taupa,pam,pi!,bdofpam1,bdofpam2
  !real(rp)               :: papi,unompi

  pi   = 1.0/(1+exp((v+55.0)/(24.0)));
  IKr  = prm%p_gkr*pa*pi*(v-ek);!*(1.0-bdof);
  !
  taupa= 0.03118+(0.21718*exp(-(((v+20.1376)/(22.1996))**2)));
  pam  = 1.0/(1+exp((v+15.0)/(-6.0)));
  !pa   = pa + (dt*((pam-pa)/taupa));
  pa=pam+(pa-pam)*exp(-dt/(1000.0*taupa));
  !papi = pa*pi; 
  !unompi = 1.0-pi;
  !bdofpam1  = (0.467*papi+unompi*0.073429)*prm%p_Ddof;
  !bdofpam2 = 0.003273*papi+0.000514*unompi;
  !bdof = bdof + dt*(bdofpam1*(1.0-bdof)-bdofpam2*bdof);
  return 
end subroutine f_IKr
! ------------------------------------------------------------------------------------- 
subroutine f_IKs(v,dt,ek,prm,n,IKs)
! ===================================================================================== 
! K,slow current -----Ecuacion 18
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: v,dt,ek
  type (t_prm)           :: prm
  real(rp),intent(inout) :: n
  real(rp),intent(out)   :: IKs
  real(rp)               :: taun,nm

  IKs = prm%p_gks*n*(v-ek); 
  !
  taun= 0.7+(0.4*exp(-(((v-20.0)/(20.0))**2)));
  nm  = 1.0/(1+exp((v-19.9)/(-12.7)));
  !n   = n + (dt*((nm-n)/taun));
  n=nm+(n-nm)*exp(-dt/(1000.0*taun));              
! ------------------------------------------------------------------------------------- 
!  IK = IKr + IKs;
  return 
end subroutine f_IKs
! ------------------------------------------------------------------------------------- 
subroutine f_IK1(v,ek,kc,prm,IK1)
! ===================================================================================== 
! IK1 inward rectifier current -----Ecuacion 21
! ===================================================================================== 
  implicit none
  real(rp),intent(in) :: v,ek,kc
  type (t_prm)        :: prm
  real(rp),intent(out):: IK1
  real(rp)            :: ik1max1

  ik1max1 = 1.0+exp((1.5*(v-ek+3.6)*p_FA)/(p_R*p_Te)); 
  IK1 = ((prm%p_gk1*(kc**(0.4457)))*(v-ek))/(ik1max1); 
  return
end subroutine f_IK1
! ------------------------------------------------------------------------------------- 
subroutine f_basal(v,cac,cai,ena,prm,IBNA,IBCA,IB)
! ===================================================================================== 
! Basal currents  -----Ecuacion  22
! ===================================================================================== 
  implicit none
  real(rp),intent(in)  :: v,cac,cai,ena
  type (t_prm)         :: prm
  real(rp),intent(out) :: IBNA,IBCA,IB
  real(rp)             :: eca
  
  eca = ((p_R*p_Te)/(2.0*p_FA))*log(cac/cai);
  IBNA = prm%p_gnab*(v-ena);
  IBCA = prm%p_gcab*(v-eca);
  IB = IBNA+IBCA;
  return
end subroutine f_basal
! ------------------------------------------------------------------------------------- 
subroutine f_ICAP(cai,prm,ICAP)
! ===================================================================================== 
! Ca pump current -----Ecuacion 24
! ===================================================================================== 
  implicit none
  real(rp),intent(in)  :: cai
  type (t_prm)         :: prm
  real(rp),intent(out) :: ICAP

  ICAP = p_icapbar*(cai/(cai+p_kcap));
  return 
end subroutine f_ICAP
! ------------------------------------------------------------------------------------- 
subroutine f_INAK(v,nai,kc,prm,INAK)
! ===================================================================================== 
! Na-K pump current -----Ecuacion 23
! =====================================================================================
  implicit none
  real(rp),intent(in) :: v,nai,kc
  type (t_prm)        :: prm
  real(rp),intent(out):: INAK
  real(rp)            :: inakmax

  inakmax = ((nai**1.5)/((nai**1.5)+(p_knakna**1.5))); 
  INAK = p_inakbar*(kc/(kc+p_knakk))*inakmax*((v+150.0)/((v+200.0))); 
  return
end subroutine f_INAK
! ------------------------------------------------------------------------------------- 
subroutine f_INACA(v,nac,nai,cac,cai,prm,INACA)
! ===================================================================================== 
! Na-Ca exchanger current -----Ecuacion 25
! ===================================================================================== 
  implicit none
  real(rp),intent(in) :: v,nac,nai,cac,cai
  type (t_prm)        :: prm
  real(rp),intent(out):: INACA
  real(rp)            :: phif,phir,nmr,dnm
  
  phif  = exp((p_lambda*v*p_FA)/(p_R*p_Te));
  phir  = exp(((p_lambda-1.0)*v*p_FA)/(p_R*p_Te));
  nmr   = (((nai**3)*cac)*phif)-(((nac**3)*cai)*phir); 
  dnm   = 1.0+(p_dnaca*(((nac**3)*(cai))+((nai**3)*cac))); 
  INACA = p_knaca*(nmr/dnm);
  return
end subroutine f_INACA
! ------------------------------------------------------------------------------------- 
subroutine f_IKACh(v,ek,prm,IKACh)
! ===================================================================================== 
! ACh activated K current -----Ecuacion 30
! ===================================================================================== 
  implicit none
  real(rp),intent(in) :: v,ek
  type (t_prm)        :: prm
  real(rp),intent(out):: IKACh
  
  IKACh =50.0*(10.0/(1.0+(9.13652/(p_ACh**0.477811))))*(0.0517+(0.4516/(1.0+exp((v+59.53)/17.18))))*(v-ek);
  return
end subroutine f_IKACh
! ------------------------------------------------------------------------------------- 
subroutine f_SRCa(dt,cai,cado,prm,f1,f2,ocalse,caup,carel,IUP,IREL)
! ===================================================================================== 
! SR Ca handling -----Ecuacion  29
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: dt,cai,cado
  type (t_prm)           :: prm
  real(rp),intent(inout) :: f1,f2,ocalse,caup,carel
  real(rp)               :: kinact,kact,itr,num2,den2,IUP,IREL,aux

  num2  = (cai/p_kcyca)-(((p_kxcs**2)*caup)/p_ksrca); 
  den2  = ((cai+p_kcyca)/p_kcyca)+(p_kxcs*((caup+p_ksrca)/p_ksrca)); 
  IUP   = p_iupbar * (num2/den2);
  IREL  = p_alprel*((f2/(f2+0.25))**2)*(carel-cai);      
  !
  kinact = 33.96+(339.6*((cai/(cai+p_kreli))**4));
  kact = 203.8*(((cai/(cai+p_kreli))**4)+((cado/(cado+p_kreldo))**4));
  f1 = f1 + (dt*(p_krecov*(1.0-f1-f2)-(kact*f1)))/1000.0; 
  f2 = f2 + (dt*((kact*f1)-(kinact*f2)))/1000.0;
  ocalse = ocalse + (dt*((480.0*carel*(1.0-ocalse))-(400.0*ocalse)))/1000.0; 

  itr   = ((caup-carel))*((2.0*p_FA*p_volrel)/p_tautr);
   
  caup  = caup + (dt*((IUP-itr)/(2.0*p_volup*p_FA)))/1000.0;
  aux   = (31.0*((480.0*carel*(1.0-ocalse))-(400.0*ocalse)));
  carel = carel + (dt*(((itr-IREL)/(2.0*p_volrel*p_FA))-aux))/1000.0;
   
  return
end subroutine f_SRCa
! ------------------------------------------------------------------------------------- 
subroutine f_ICab(dt,cai,prm,oc,otc,otmgc,otnmgmg,ocd,otcd,otmgcd)
! ===================================================================================== 
! Intracellular Ca buffering  -----Ecuacion 27
! =====================================================================================  
  implicit none
  real(rp),intent(in)    :: dt,cai
  type (t_prm)           :: prm
  real(rp),intent(inout) :: oc,otc,otmgc,otnmgmg,ocd,otcd,otmgcd

  ocd     = (200000.0*cai*(1.0-oc))-(476.0*oc);
  otcd    = (78400.0*cai*(1.0-otc))-(392.0*otc);
  otmgcd  = (200000.0*cai*(1.0-otmgc-otnmgmg))-(6.6*otmgc);
  oc      = oc  + (dt*(ocd))/1000.0;
  otc     = otc + (dt*(otcd))/1000.0;
  otmgc   = otmgc   + (dt*(otmgcd))/1000.0;
  otnmgmg = otnmgmg + (dt*((2000.0*p_mgi*(1.0-otmgc-otnmgmg))-(666.0*otnmgmg)))/1000.0; 
  return
end subroutine f_ICab
! ------------------------------------------------------------------------------------- 
subroutine f_Icon(dt,Istm,INA,IBNA,INAK,INACA,ICAL,IBCA,ICAP,IUP,IREL,It,Ikur,IK1,IKs, &
                  IKr,IKACh,otcd,otmgcd,ocd,prm,nai,ki,cado,cai)
! ===================================================================================== 
! Intracellular ion concentration  -----Ecuacion 26
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: dt,Istm,INA,IBNA,INAK,INACA,ICAL,IBCA,ICAP,IUP,IREL,It,  &
                            Ikur,IK1,IKs,IKr,IKACh,otcd,otmgcd,ocd
  type (t_prm)           :: prm
  real(rp),intent(inout) :: nai,ki,cado,cai
  real(rp)               :: phicai,IDI,aux1!,aux2

  phicai= (0.08*otcd)+(0.16*otmgcd)+(0.045*ocd); 
  aux1  = (p_FA*p_voli);
  nai   = nai + (dt*(-((INA+IBNA+(3.0*INAK)+(3.0*INACA))+p_phinaen)/aux1))/1000.0;
  ki    = ki +  (dt*(-(50.0*Istm+It+Ikur+IK1+IKs+IKr+IKACh-(2.0*INAK))/aux1))/1000.0; !Include Istim?
  IDI   = ((cado-cai))*(2.0*p_FA*p_voldo/p_taudi);
  cado  = cado + (dt*(-((ICAL + IDI)/(2.0*p_voldo*p_FA))))/1000.0; !Error in paper:(iCAL - IDI)
  !aux2  = 2.0*aux1-phicai; !OJO, es "+" para luego restarse con el dt
  cai   = cai - dt*(((IBCA+ICAP+IUP)-(IDI+IREL+2.0*INACA))/(2.0*aux1)+phicai)/1000.0; 
  return
end subroutine f_Icon
! ------------------------------------------------------------------------------------- 
subroutine f_Cleft(dt,INA,IBNA,INAK,INACA,It,Ikur,IKACh,IK1,IKs,IKr,ICAL,IBCA,    &
                   ICAP,prm,nac,kc,cac)
! =====================================================================================
! Cleft Spase ion concentration -----Ecuacion 28
! =====================================================================================
  implicit none
  real(rp),intent(in)    :: dt,INA,IBNA,INAK,INACA,It,Ikur,IKACh,IK1,IKs,IKR,     &
                            ICAL,IBCA,ICAP
  type (t_prm)           :: prm
  real(rp),intent(inout) :: nac,kc,cac
  real(rp)               :: aux1,aux2

  aux1 = (p_volo*p_FA);
  aux2 = INA+IBNA+(3.0*INAK)+(3.0*INACA);
  nac  = nac + (dt*(((p_nab-nac)/p_tauna)+((aux2+p_phinaen)/aux1)))/1000.0;
  kc   = kc  + (dt*(((p_kb-kc)/p_tauk)+((It+Ikur+IK1+IKs+IKr+IKACh-(2.0*INAK))/aux1)))/1000.0;
  cac  = cac + (dt*(((p_cab-cac)/p_tauca)+((ICAL+IBCA+ICAP-(2.0*INACA))/(2.0*aux1))))/1000.0;
  return
end subroutine f_Cleft
!-------------------------------------------------------------------------------
subroutine Maleckar_A_P01 (ict, dt, v, Istm, Iion, v_prm, v_maleck, v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict
  real(rp),    intent(in)    :: dt, Istm, v
  real(rp),    intent(in)    :: v_prm(:)
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(inout) :: v_maleck(nvar_maleck)
  real(rp),    intent(out)   :: v_cr(ncur_maleck)
  type(t_maleck)             :: maleck
  type (t_prm)               :: param
  real(rp)                   :: INA,ICAL,ek,It,Ikur,IKr,IKs,IK1,IKACh,ena,    &
                                IBNA,IBCA,IB,ICAP,INAK,INACA,IUP,IREL
  real(rp)                   :: otcd, otmgcd, ocd
  !
  call put_me_struct(v_maleck,maleck)
  call put_param(v_prm,param)
  !
  call f_na(v,maleck%m,maleck%h1,maleck%h2,maleck%nac,maleck%nai,param,INA,ena)
  call f_ca(v,dt,maleck%cado,param,maleck%dl,maleck%fl1,maleck%fl2,ICAL)
  call f_It(v,dt,maleck%kc,maleck%ki,param,maleck%rr,maleck%s,ek,It)
  call f_Ikur(v,dt,ek,param,maleck%uakur,maleck%uikur,Ikur)
  call f_IKr(v,dt,ek,param,maleck%pa,IKr)!,maleck%bdof)
  call f_IKs(v,dt,ek,param,maleck%n,IKs)
  call f_IK1(v,ek,maleck%kc,param,IK1)
  call f_basal(v,maleck%cac,maleck%cai,ena,param,IBNA,IBCA,IB)
  call f_ICAP(maleck%cai,param,ICAP)
  call f_INAK(v,maleck%nai,maleck%kc,param,INAK)
  call f_INACA(v,maleck%nac,maleck%nai,maleck%cac,maleck%cai,param,INACA)
  call f_IKACh(v,ek,param,IKACh)
  call f_SRCa(dt,maleck%cai,maleck%cado,param,maleck%f1,maleck%f2,maleck%ocalse,      &
              maleck%caup,maleck%carel,IUP,IREL)
  call f_ICab(dt,maleck%cai,param,maleck%oc,maleck%otc,maleck%otmgc,maleck%otnmgmg,   &
              ocd,otcd,otmgcd)
  call f_Icon(dt,Istm,INA,IBNA,INAK,INACA,ICAL,IBCA,ICAP,IUP,IREL,It,Ikur,IK1,IKs,   &
              IKr,IKACh,otcd,otmgcd,ocd,param,maleck%nai,maleck%ki,maleck%cado, &
              maleck%cai)
  call f_Cleft(dt,INA,IBNA,INAK,INACA,It,Ikur,IKACh,IK1,IKs,IKr,ICAL,IBCA,    &
               ICAP,param,maleck%nac,maleck%kc,maleck%cac)
  call f_m(v,dt,maleck%m)  !....
  call f_h_total(v,dt,param,maleck%h1,maleck%h2)
  Iion = (INA+It+Ikur+ICAL+IKr+IKs+IK1+IKACh+IB+ICAP+INAK+INACA)/50.0;
  !
  call get_me_struct(maleck,v_maleck)

  !.
  v_cr(1:ncur_maleck) = (/ INA, ICAL, ek, It, Ikur, IKr, IKs, IK1, IKACh, ena,    &
                  IBNA, IBCA, IB, ICAP, INAK, INACA, IUP, IREL, Iion /)
  v_cr = v_cr/50.0
  !.

  return
end subroutine Maleckar_A_P01
!-------------------------------------------------------------------------------
end module mod_maleckar
!-------------------------------------------------------------------------------
