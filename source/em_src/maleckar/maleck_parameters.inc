!------------------------------------------------------------------------------
! This are the parameters for the Maleckar Model
! Constants
!------------------------------------------------------------------------------
! Model Constants
!
 real(rp), parameter, private :: p_R        = 8314.0;      ! J/mol??K 
 real(rp), parameter, private :: p_FA       = 96487.0;     ! ??C/mmol  
 real(rp), parameter, private :: p_Te       = 306.15;      ! K 
!
 real(rp), parameter, private :: p_voli     = 0.005884;    ! nl
 real(rp), parameter, private :: p_volo     = 0.000800224; ! nl
 real(rp), parameter, private :: p_voldo    = 0.00011768;  ! nl
 real(rp), parameter, private :: p_volrel   = 0.0000441;   ! nl
 real(rp), parameter, private :: p_volup    = 0.0003969;   ! nl
! 
 real(rp), parameter, private :: p_nab      = 130.0;       ! mM/l
 real(rp), parameter, private :: p_kb       = 5.4;         ! mM/l
 real(rp), parameter, private :: p_cab      = 1.8;         ! mM/l
 real(rp), parameter, private :: p_mgi      = 2.5;         ! mM/l
 real(rp), parameter, private :: p_kca      = 0.025;       ! mM/l
 real(rp), parameter, private :: p_ecaapp   = 60.0;        ! mV
!
 real(rp), parameter, private :: p_tauna    = 14.3;	   	   ! s
 real(rp), parameter, private :: p_tauk     = 10.0;        ! s
 real(rp), parameter, private :: p_tauca    = 24.7;        ! s
 real(rp), parameter, private :: p_taudi    = 0.010;       ! s
 real(rp), parameter, private :: p_inakbar  = 68.55;	   ! pA 
 real(rp), parameter, private :: p_knakk    = 1.0          ! mM/l
 real(rp), parameter, private :: p_knakna   = 11.0;	       ! mM/l
 real(rp), parameter, private :: p_icapbar  = 4.0;	       ! pA 
 real(rp), parameter, private :: p_kcap     = 0.0002;	   ! Mm/l
 real(rp), parameter, private :: p_knaca    = 0.0374842    ! pA/(mM/l)**4
 real(rp), parameter, private :: p_lambda   = 0.45;           
 real(rp), parameter, private :: p_dnaca    = 0.0003;	   ! (mM/l)**(-4) 
 real(rp), parameter, private :: p_phinaen  = 0.0;	       ! pA 
 real(rp), parameter, private :: p_iupbar   = 2800.0;	   ! pA 
 real(rp), parameter, private :: p_kcyca    = 0.0003; 	   ! mM/l 
 real(rp), parameter, private :: p_ksrca    = 0.5;         ! mM/l 
 real(rp), parameter, private :: p_kxcs     = 0.4; 	       ! mM/l
 real(rp), parameter, private :: p_tautr    = 0.01;	       ! s 
 real(rp), parameter, private :: p_alprel   = 200000.0;    ! pA/mM 
 real(rp), parameter, private :: p_kreli    = 0.0003;	   ! mM/l
 real(rp), parameter, private :: p_kreldo   = 0.003;	   ! nM/l 
 real(rp), parameter, private :: p_krecov   = 0.815;	   ! 1/s
!
! Variable Parameters of the model
!  
 real(rp), parameter, private :: p_pna      = 0.0018;	   ! nl/s	
!
!---------Paramaters for gCaL depending on the heterogeneity
 real(rp), parameter, private :: p_gcal     	= 6.75;	   ! nS 
 real(rp), parameter, private :: p_gcal_RA_PM   = 6.75;	   ! nS 
 real(rp), parameter, private :: p_gcal_CT_BB   = 9.45;	   ! nS 
 real(rp), parameter, private :: p_gcal_RAA     = 6.75;	   ! nS 
 real(rp), parameter, private :: p_gcal_TVR     = 5.40;	   ! nS 
 real(rp), parameter, private :: p_gcal_LA      = 4.52;	   ! nS 
 real(rp), parameter, private :: p_gcal_PV      = 4.52;	   ! nS 
 real(rp), parameter, private :: p_gcal_LAA     = 4.52;	   ! nS 
 real(rp), parameter, private :: p_gcal_MVR     = 3.57;	   ! nS 
!
!---------Paramaters for gto depending on the heterogeneity
 real(rp), parameter, private :: p_gt       	= 8.25;	   ! ns
 real(rp), parameter, private :: p_gt_RA_PM   	= 8.25;	   ! nS 
 real(rp), parameter, private :: p_gt_CT_BB   	= 4.12;	   ! nS 
 real(rp), parameter, private :: p_gt_RAA     	= 11.13;   ! nS 
 real(rp), parameter, private :: p_gt_TVR     	= 11.13;   ! nS 
 real(rp), parameter, private :: p_gt_LA      	= 8.25;	   ! nS 
 real(rp), parameter, private :: p_gt_PV      	= 11.13;   ! nS 
 real(rp), parameter, private :: p_gt_LAA     	= 11.13;   ! nS 
 real(rp), parameter, private :: p_gt_MVR     	= 11.13;   ! nS 
!
 real(rp), parameter, private :: p_gkur     	= 2.25;	   ! ns
!
!---------Paramaters for gkr depending on the heterogeneity
 real(rp), parameter, private :: p_gkr      	= 0.50;	   ! nS 
 real(rp), parameter, private :: p_gkr_RA_PM   	= 0.50;	   ! nS 
 real(rp), parameter, private :: p_gkr_CT_BB   	= 0.35;	   ! nS 
 real(rp), parameter, private :: p_gkr_RAA     	= 0.50;    ! nS 
 real(rp), parameter, private :: p_gkr_TVR     	= 1.00;    ! nS 
 real(rp), parameter, private :: p_gkr_LA      	= 0.80;	   ! nS 
 real(rp), parameter, private :: p_gkr_PV      	= 1.60;    ! nS 
 real(rp), parameter, private :: p_gkr_LAA     	= 0.80;    ! nS 
 real(rp), parameter, private :: p_gkr_MVR     	= 1.60;    ! nS 
! 
 real(rp), parameter, private :: p_gks      	= 1.0;	   ! nS 
 real(rp), parameter, private :: p_gk1      	= 3.1;	   ! nS 
 real(rp), parameter, private :: p_gnab     	= 0.060599;! nS
 real(rp), parameter, private :: p_gcab     	= 0.078681;! nS 
 real(rp), parameter, private :: p_taufl1   	= 1.0;     ! reducing factor
 real(rp), parameter, private :: p_Vhm      	= 63.6;    ! mV
 real(rp), parameter, private :: p_Vrm      	= 1.0;     ! mV
!real(rp), parameter, private :: p_Ddof     	= 0.0;     ! microM
!
 real(rp), parameter, private :: p_ACh      = 1.0E-24;	   ! mM	
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS --------------------------------------------------------
!------------------------------------------------------------------------------
!
! 60 minutes at BCL of 1sec
!
! Vi
  real(rp), parameter :: Vi_Maleck  	= -73.6019087;
  real(rp), parameter :: Vi_Maleck_RA_PM= -73.6751745;
  real(rp), parameter :: Vi_Maleck_CT_BB= -72.6891912;  !-69.8631564;
  real(rp), parameter :: Vi_Maleck_RAA 	= -73.9957587;
  real(rp), parameter :: Vi_Maleck_TVR 	= -74.2391957;
  real(rp), parameter :: Vi_Maleck_LA 	= -74.0465926;
  real(rp), parameter :: Vi_Maleck_PV 	= -74.3689385;
  real(rp), parameter :: Vi_Maleck_LAA 	= -74.2780812;
  real(rp), parameter :: Vi_Maleck_MVR 	= -74.4113410;
! Nac
  real(rp), parameter :: nacic      	= 1.30020750e+02;
  real(rp), parameter :: nacic_RA_PM    = 1.30020128e+02;
  real(rp), parameter :: nacic_CT_BB    = 1.29971377e+02;
  real(rp), parameter :: nacic_RAA      = 1.30029442e+02;
  real(rp), parameter :: nacic_TVR      = 1.30039110e+02;
  real(rp), parameter :: nacic_LA       = 1.30034748e+02;
  real(rp), parameter :: nacic_PV       = 1.30044790e+02;
  real(rp), parameter :: nacic_LAA      = 1.30042137e+02;
  real(rp), parameter :: nacic_MVR      = 1.30047990e+02;
! Nai
  real(rp), parameter :: naiic      	= 8.35059362e+00;
  real(rp), parameter :: naiic_RA_PM    = 8.36937257e+00;
  real(rp), parameter :: naiic_CT_BB   	= 8.91587832e+00;
  real(rp), parameter :: naiic_RAA     	= 8.34256656e+00;
  real(rp), parameter :: naiic_TVR     	= 8.06499329e+00;
  real(rp), parameter :: naiic_LA      	= 7.90754761e+00;
  real(rp), parameter :: naiic_PV      	= 7.91546867e+00;
  real(rp), parameter :: naiic_LAA     	= 7.90686376e+00;
  real(rp), parameter :: naiic_MVR     	= 7.76690620e+00;
! m
  real(rp), parameter :: mic        	= 3.46483453e-03;
  real(rp), parameter :: mic_RA_PM  	= 3.43415870e-03;
  real(rp), parameter :: mic_CT_BB  	= 5.45143692e-03;
  real(rp), parameter :: mic_RAA    	= 3.30308136e-03;
  real(rp), parameter :: mic_TVR  	= 3.20688866e-03;
  real(rp), parameter :: mic_LA	  	= 3.28276052e-03;
  real(rp), parameter :: mic_PV	  	= 3.15676681e-03;
  real(rp), parameter :: mic_LAA  	= 3.19178375e-03;
  real(rp), parameter :: mic_MVR  	= 3.14055560e-03;
! h1
  real(rp), parameter :: h1ic       	= 8.68134339e-01;
  real(rp), parameter :: h1ic_RA_PM	= 8.69725793e-01;
  real(rp), parameter :: h1ic_CT_BB     = 7.87811087e-01;
  real(rp), parameter :: h1ic_RAA	= 8.76389973e-01;
  real(rp), parameter :: h1ic_TVR	= 8.81257490e-01;
  real(rp), parameter :: h1ic_LA	= 8.77396766e-01;
  real(rp), parameter :: h1ic_PV	= 8.83811326e-01;
  real(rp), parameter :: h1ic_LAA	= 8.82032896e-01;
  real(rp), parameter :: h1ic_MVR	= 8.84647949e-01;
! h2
  real(rp), parameter :: h2ic       	= 8.64649615e-01;
  real(rp), parameter :: h2ic_RA_PM     = 8.66314135e-01;
  real(rp), parameter :: h2ic_CT_BB     = 8.07996948e-01;
  real(rp), parameter :: h2ic_RAA       = 8.73137264e-01;
  real(rp), parameter :: h2ic_TVR       = 8.78210472e-01;
  real(rp), parameter :: h2ic_LA	= 8.74179869e-01;
  real(rp), parameter :: h2ic_PV	= 8.80973299e-01;
  real(rp), parameter :: h2ic_LAA       = 8.79058381e-01;
  real(rp), parameter :: h2ic_MVR       = 8.81916833e-01;
! cado
  real(rp), parameter :: cadoic         = 6.92672380e-05;
  real(rp), parameter :: cadoic_RA_PM  	= 6.97552793e-05;
  real(rp), parameter :: cadoic_CT_BB  	= 1.30135866e-04;
  real(rp), parameter :: cadoic_RAA  	= 6.60681864e-05;
  real(rp), parameter :: cadoic_TVR  	= 5.69179433e-05;
  real(rp), parameter :: cadoic_LA  	= 5.48221800e-05;
  real(rp), parameter :: cadoic_PV  	= 5.21158431e-05;
  real(rp), parameter :: cadoic_LAA  	= 5.27825227e-05;
  real(rp), parameter :: cadoic_MVR  	= 4.82140348e-05;
! dl
  real(rp), parameter :: dlic		= 1.45472824e-05;
  real(rp), parameter :: dlic_RA_PM    	= 1.43645210e-05;
  real(rp), parameter :: dlic_CT_BB    	= 2.72628722e-05;
  real(rp), parameter :: dlic_RAA    	= 1.35924774e-05;
  real(rp), parameter :: dlic_TVR    	= 1.30340203e-05;
  real(rp), parameter :: dlic_LA    	= 1.34741556e-05;
  real(rp), parameter :: dlic_PV    	= 1.27455509e-05;
  real(rp), parameter :: dlic_LAA    	= 1.29468393e-05;
  real(rp), parameter :: dlic_MVR    	= 1.26525498e-05;
! fl1
  real(rp), parameter :: fl1ic      	= 9.98508829e-01;
  real(rp), parameter :: fl1ic_RA_PM    = 9.98524174e-01;
  real(rp), parameter :: fl1ic_CT_BB	= 9.97600130e-01;
  real(rp), parameter :: fl1ic_RAA	= 9.98589108e-01;
  real(rp), parameter :: fl1ic_TVR	= 9.98636518e-01;
  real(rp), parameter :: fl1ic_LA	= 9.98599058e-01;
  real(rp), parameter :: fl1ic_PV	= 9.98661229e-01;
  real(rp), parameter :: fl1ic_LAA	= 9.98643988e-01;
  real(rp), parameter :: fl1ic_MVR	= 9.98669249e-01;
! fl2
  real(rp), parameter :: fl2ic		= 9.98496839e-01;
  real(rp), parameter :: fl2ic_RA_PM   	= 9.98512769e-01;
  real(rp), parameter :: fl2ic_CT_BB   	= 9.97839096e-01;
  real(rp), parameter :: fl2ic_RAA   	= 9.98579225e-01;
  real(rp), parameter :: fl2ic_TVR   	= 9.98627709e-01;
  real(rp), parameter :: fl2ic_LA   	= 9.98589252e-01;
  real(rp), parameter :: fl2ic_PV   	= 9.98653392e-01;
  real(rp), parameter :: fl2ic_LAA   	= 9.98635653e-01;
  real(rp), parameter :: fl2ic_MVR   	= 9.98661994e-01;
! kc
  real(rp), parameter :: kcic           = 5.67489862e+00;
  real(rp), parameter :: kcic_RA_PM    	= 5.66751115e+00;
  real(rp), parameter :: kcic_CT_BB    	= 5.66895260e+00;
  real(rp), parameter :: kcic_RAA    	= 5.66572791e+00;
  real(rp), parameter :: kcic_TVR    	= 5.67017279e+00;
  real(rp), parameter :: kcic_LA    	= 5.67527905e+00;
  real(rp), parameter :: kcic_PV    	= 5.67258840e+00;
  real(rp), parameter :: kcic_LAA    	= 5.67343888e+00;
  real(rp), parameter :: kcic_MVR    	= 5.67565606e+00;
! ki
  real(rp), parameter :: kiic       	= 1.29658235e+02;
  real(rp), parameter :: kiic_RA_PM     = 1.30021455e+02;
  real(rp), parameter :: kiic_CT_BB     = 1.29373047e+02;
  real(rp), parameter :: kiic_RAA	= 1.30046452e+02;
  real(rp), parameter :: kiic_TVR	= 1.30302274e+02;
  real(rp), parameter :: kiic_LA	= 1.30450005e+02;
  real(rp), parameter :: kiic_PV	= 1.30437376e+02;
  real(rp), parameter :: kiic_LAA	= 1.30447692e+02;
  real(rp), parameter :: kiic_MVR	= 1.30572634e+02;
! rr
  real(rp), parameter :: rric       	= 1.13278947e-03;
  real(rp), parameter :: rric_RA_PM	= 1.12527413e-03;
  real(rp), parameter :: rric_CT_BB	= 1.58258474e-03;
  real(rp), parameter :: rric_RAA	= 1.09299763e-03;
  real(rp), parameter :: rric_TVR	= 1.06910650e-03;
  real(rp), parameter :: rric_LA	= 1.08797119e-03;
  real(rp), parameter :: rric_PV	= 1.05658019e-03;
  real(rp), parameter :: rric_LAA	= 1.06533555e-03;
  real(rp), parameter :: rric_MVR       = 1.05251515e-03;
! s
  real(rp), parameter :: sic        	= 9.46735363e-01;
  real(rp), parameter :: sic_RA_PM	= 9.47057937e-01;
  real(rp), parameter :: sic_CT_BB	= 9.30911723e-01;
  real(rp), parameter :: sic_RAA	= 9.48433933e-01;
  real(rp), parameter :: sic_TVR	= 9.49456746e-01;
  real(rp), parameter :: sic_LA		= 9.48646079e-01;
  real(rp), parameter :: sic_PV		= 9.49997572e-01;
  real(rp), parameter :: sic_LAA	= 9.49620031e-01;
  real(rp), parameter :: sic_MVR	= 9.50174692e-01;
! uakur
  real(rp), parameter :: uakuric        = 3.85630904e-04;
  real(rp), parameter :: uakuric_RA_PM	= 3.82349929e-04;
  real(rp), parameter :: uakuric_CT_BB	= 5.75603960e-04;
  real(rp), parameter :: uakuric_RAA	= 3.68390678e-04;
  real(rp), parameter :: uakuric_TVR	= 3.58129197e-04;
  real(rp), parameter :: uakuric_LA	= 3.66240093e-04;
  real(rp), parameter :: uakuric_PV	= 3.52758671e-04;
  real(rp), parameter :: uakuric_LAA	= 3.56507888e-04;
  real(rp), parameter :: uakuric_MVR	= 3.51012808e-04;
! uikur
  real(rp), parameter :: uikuric	= 9.66772253e-01;
  real(rp), parameter :: uikuric_RA_PM  = 9.67034907e-01;
  real(rp), parameter :: uikuric_CT_BB	= 9.47612869e-01;
  real(rp), parameter :: uikuric_RAA	= 9.72547886e-01;
  real(rp), parameter :: uikuric_TVR	= 9.77099199e-01;
  real(rp), parameter :: uikuric_LA	= 9.74427488e-01;
  real(rp), parameter :: uikuric_PV	= 9.79741186e-01;
  real(rp), parameter :: uikuric_LAA	= 9.78824501e-01;
  real(rp), parameter :: uikuric_MVR	= 9.81475103e-01;
! n
  real(rp), parameter :: nic   		= 4.49938831e-03;
  real(rp), parameter :: nic_RA_PM      = 4.47492596e-03;
  real(rp), parameter :: nic_CT_BB      = 7.11470123e-03;
  real(rp), parameter :: nic_RAA        = 3.76997458e-03;
  real(rp), parameter :: nic_TVR        = 3.31133164e-03;
  real(rp), parameter :: nic_LA	        = 3.70570500e-03;
  real(rp), parameter :: nic_PV	        = 3.04507262e-03;
  real(rp), parameter :: nic_LAA        = 3.14248690e-03;
  real(rp), parameter :: nic_MVR        = 2.87440082e-03;
! pa
  real(rp), parameter :: paic		= 5.74488817e-05;
  real(rp), parameter :: paic_RA_PM     = 5.67437385e-05;
  real(rp), parameter :: paic_CT_BB     = 9.48740907e-05;
  real(rp), parameter :: paic_RAA   	= 5.38084977e-05;
  real(rp), parameter :: paic_TVR	= 5.16793625e-05;
  real(rp), parameter :: paic_LA	= 5.33675762e-05;
  real(rp), parameter :: paic_PV	= 5.05664966e-05;
  real(rp), parameter :: paic_LAA	= 5.13410164e-05;
  real(rp), parameter :: paic_MVR	= 5.02023746e-05;
! Cac
  real(rp), parameter :: cacic      	= 1.81537485e+00;
  real(rp), parameter :: cacic_RA_PM    = 1.82512376e+00;
  real(rp), parameter :: cacic_CT_BB    = 1.84806393e+00;
  real(rp), parameter :: cacic_RAA      = 1.82092537e+00;
  real(rp), parameter :: cacic_TVR	= 1.81275877e+00;
  real(rp), parameter :: cacic_LA	= 1.81226120e+00;
  real(rp), parameter :: cacic_PV	= 1.80800996e+00;
  real(rp), parameter :: cacic_LAA	= 1.80909790e+00;
  real(rp), parameter :: cacic_MVR	= 1.80433993e+00;
! Cai
  real(rp), parameter :: caiic		= 6.34901953e-05;
  real(rp), parameter :: caiic_RA_PM	= 6.40500719e-05;
  real(rp), parameter :: caiic_CT_BB	= 1.20538678e-04;
  real(rp), parameter :: caiic_RAA	= 6.06487358e-05;
  real(rp), parameter :: caiic_TVR	= 5.27428882e-05;
  real(rp), parameter :: caiic_LA	= 5.12104767e-05;
  real(rp), parameter :: caiic_PV	= 4.86909306e-05;
  real(rp), parameter :: caiic_LAA	= 4.93071895e-05;
  real(rp), parameter :: caiic_MVR	= 4.55213840e-05;
! oc
  real(rp), parameter :: ocic 		= 2.59836984e-02;
  real(rp), parameter :: ocic_RA_PM	= 2.62066364e-02;
  real(rp), parameter :: ocic_CT_BB	= 4.77979717e-02;
  real(rp), parameter :: ocic_RAA	= 2.48501108e-02;
  real(rp), parameter :: ocic_TVR	= 2.16817274e-02;
  real(rp), parameter :: ocic_LA	= 2.10652534e-02;
  real(rp), parameter :: ocic_PV	= 2.00494881e-02;
  real(rp), parameter :: ocic_LAA	= 2.02981474e-02;
  real(rp), parameter :: ocic_MVR	= 1.87688553e-02;
! otc
  real(rp), parameter :: otcic      	= 1.25390011e-02;
  real(rp), parameter :: otcic_RA_PM	= 1.26480612e-02;
  real(rp), parameter :: otcic_CT_BB	= 2.32896684e-02;
  real(rp), parameter :: otcic_RAA	= 1.19847909e-02;
  real(rp), parameter :: otcic_TVR	= 1.04392515e-02;
  real(rp), parameter :: otcic_LA	= 1.01391432e-02;
  real(rp), parameter :: otcic_PV	= 9.64502776e-03;
  real(rp), parameter :: otcic_LAA	= 9.76593996e-03;
  real(rp), parameter :: otcic_MVR	= 9.02284552e-03;
! otmg
  real(rp), parameter :: otmgcic    	= 1.85908189e-01;
  real(rp), parameter :: otmgcic_RA_PM  = 1.87156552e-01;
  real(rp), parameter :: otmgcic_CT_BB  = 2.65686072e-01;
  real(rp), parameter :: otmgcic_RAA	= 1.79230716e-01;
  real(rp), parameter :: otmgcic_TVR	= 1.60005685e-01;
  real(rp), parameter :: otmgcic_LA	= 1.56215248e-01;
  real(rp), parameter :: otmgcic_PV	= 1.49666538e-01;
  real(rp), parameter :: otmgcic_LAA	= 1.51287136e-01;
  real(rp), parameter :: otmgcic_MVR    = 1.41385972e-01;
! otnmg
  real(rp), parameter :: otnmgmgic  	= 7.18398968e-01;
  real(rp), parameter :: otnmgmgic_RA_PM= 7.17297634e-01;
  real(rp), parameter :: otnmgmgic_CT_BB= 6.48045177e-01;
  real(rp), parameter :: otnmgmgic_RAA	= 7.24291651e-01;
  real(rp), parameter :: otnmgmgic_TVR	= 7.41256658e-01;
  real(rp), parameter :: otnmgmgic_LA	= 7.44601461e-01;
  real(rp), parameter :: otnmgmgic_PV	= 7.50380516e-01;
  real(rp), parameter :: otnmgmgic_LAA	= 7.48950373e-01;
  real(rp), parameter :: otnmgmgic_MVR	= 7.57687778e-01;
! carel
  real(rp), parameter :: carelic    	= 6.20194763e-01;
  real(rp), parameter :: carelic_RA_PM	= 6.24059571e-01;
  real(rp), parameter :: carelic_CT_BB	= 6.48921850e-01;
  real(rp), parameter :: carelic_RAA	= 5.98041963e-01;
  real(rp), parameter :: carelic_TVR	= 5.28127559e-01;
  real(rp), parameter :: carelic_LA	= 5.14151679e-01;
  real(rp), parameter :: carelic_PV	= 4.87587421e-01;
  real(rp), parameter :: carelic_LAA	= 4.94235307e-01;
  real(rp), parameter :: carelic_MVR	= 4.54666282e-01;
! caup
  real(rp), parameter :: caupic     	= 6.34167071e-01;
  real(rp), parameter :: caupic_RA_PM	= 6.38691111e-01;
  real(rp), parameter :: caupic_CT_BB	= 8.13954904e-01;
  real(rp), parameter :: caupic_RAA	= 6.09364213e-01;
  real(rp), parameter :: caupic_TVR	= 5.35007895e-01;
  real(rp), parameter :: caupic_LA	= 5.20400354e-01;
  real(rp), parameter :: caupic_PV	= 4.93763940e-01;
  real(rp), parameter :: caupic_LAA	= 5.00382651e-01;
  real(rp), parameter :: caupic_MVR	= 4.60746266e-01;
! ocalse
  real(rp), parameter :: ocalseic   	= 4.26687567e-01;
  real(rp), parameter :: ocalseic_RA_PM	= 4.28209047e-01;
  real(rp), parameter :: ocalseic_CT_BB	= 4.38408416e-01;
  real(rp), parameter :: ocalseic_RAA	= 4.17809068e-01;
  real(rp), parameter :: ocalseic_TVR	= 3.87900199e-01;
  real(rp), parameter :: ocalseic_LA	= 3.81550663e-01;
  real(rp), parameter :: ocalseic_PV	= 3.69108611e-01;
  real(rp), parameter :: ocalseic_LAA	= 3.72269086e-01;
  real(rp), parameter :: ocalseic_MVR	= 3.52978838e-01;
! f1
  real(rp), parameter :: f1ic       	= 4.75414477e-01;
  real(rp), parameter :: f1ic_RA_PM	= 4.74334205e-01;
  real(rp), parameter :: f1ic_CT_BB	= 3.86685782e-01;
  real(rp), parameter :: f1ic_RAA	= 4.83456982e-01;
  real(rp), parameter :: f1ic_TVR	= 5.02673945e-01;
  real(rp), parameter :: f1ic_LA	= 5.04959822e-01;
  real(rp), parameter :: f1ic_PV	= 5.12267716e-01;
  real(rp), parameter :: f1ic_LAA	= 5.10260030e-01;
  real(rp), parameter :: f1ic_MVR	= 5.19217918e-01;
! f2
  real(rp), parameter :: f2ic       	= 2.57931051e-03;
  real(rp), parameter :: f2ic_RA_PM	= 2.64760820e-03;
  real(rp), parameter :: f2ic_CT_BB	= 1.18167904e-02;
  real(rp), parameter :: f2ic_RAA	= 2.25829238e-03;
  real(rp), parameter :: f2ic_TVR	= 1.47434215e-03;
  real(rp), parameter :: f2ic_LA	= 1.34059568e-03;
  real(rp), parameter :: f2ic_PV	= 1.14468358e-03;
  real(rp), parameter :: f2ic_LAA	= 1.19043287e-03;
  real(rp), parameter :: f2ic_MVR	= 9.20223804e-04;
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: MALEC_mod       = 19;  ! Maleckar model            (AH)
  integer(ip), parameter :: MALEC_RA_PM_mod = 191;  ! Maleckar model RA_PM     (AH)
  integer(ip), parameter :: MALEC_CT_BB_mod = 192;  ! Maleckar model CT_BB     (AH)
  integer(ip), parameter :: MALEC_RAA_mod   = 193;  ! Maleckar model RAA       (AH)
  integer(ip), parameter :: MALEC_TVR_mod   = 194;  ! Maleckar model TVR       (AH)
  integer(ip), parameter :: MALEC_LA_mod    = 195;  ! Maleckar model LA        (AH)
  integer(ip), parameter :: MALEC_PV_mod    = 196;  ! Maleckar model PV        (AH)
  integer(ip), parameter :: MALEC_LAA_mod   = 197;  ! Maleckar model LAA       (AH)
  integer(ip), parameter :: MALEC_MVR_mod   = 198;  ! Maleckar model MVR       (AH)
  integer(ip),parameter  :: nvar_maleck     = 28;  ! Number of state variables
  integer(ip),parameter  :: ncur_maleck     = 19;  ! Number of currents
  integer(ip),parameter  :: np_maleck       = 13;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.
  integer(ip), parameter    :: m_stpML   =    5
  real(rp),    parameter    :: m_dvdtML  =  1.0
