! This module implements the Grandi Model with Heart Failure of 2010
! Based on Grandi E, et al, A novel computational model of the human ventricular action potential and Ca 
! transient, J Mol Cell Cardiol (2009), doi:10.1016/j.yjmcc.2009.09.019
!
! 21-12-2010: GrandiHF_A_P01 returns currents vector for output (EAH)
!------------------------------------------------------------------------------
module mod_grandiHF
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'grandiHF_parameters.inc'
  !.
  type t_grmHF
    sequence
    real(rp) :: Cai, CaSR, CaSL, CaJ, Nai, NaSL, NaJ, Ki, m, h, ml, hl, j, d, f,&
                fCaBJ, fCaBSL, xtos, ytos, xtof, ytof, xkr, xks, RyRr, RyRo,    &
                RyRi, NaBJ, NABSL, TnCL, TnCHC, TnCHM, CaM, MyoC, MyoM, SRB,    &
                SLLJ, SLLSL, SLHJ, SLHSL, CSQNB
  end type t_grmHF
  !.
  type, private :: t_cur
    sequence
    real(rp) :: INaJunc, INaSL, INa, INaLJunc, INaLSL, INaL, INaBkJunc,      &
                INaBkSL, INaBk, INaKJunc, INaKSL, INaK, Ikr, IksJunc, IksSL, &
                Iks, IkpJunc, IkpSL, Ikp, Itos, Itof, Ito, Ik1, IClCaJunc,   &
                IClCaSL, IclCa, IClBk, ICaJunc, ICaSL, ICa, ICaK, ICaNaJunc, &
                ICaNaSL, ICaNa, ICaL, IncxJunc, IncxSL, Incx, IpCaJunc,      &
                IpCaSL, IpCa, ICaBkJunc, ICaBkSL, ICaBk, INatotJunc,         &
                INatotSL, IKtot, ICatotJunc, ICatotSL, INatot, ICltot,       &
                ICatot, Itot
  end type t_cur
  
  public  :: ic_GrandiHF, GrandiHF_A_P01
  private :: concentrations, gates, currents

  type, public:: t_prm
    private
    real(rp) :: A_INaL;    !. 1
    real(rp) :: A_Ikr;     !. 2
    real(rp) :: A_Iks;     !. 3
    real(rp) :: A_ICaL;    !. 4
    real(rp) :: A_JSRleak; !. 5
    real(rp) :: A_Incx;    !. 6
    real(rp) :: A_ICab;     !. 7
    real(rp) :: A_IK1;     !. 8
    real(rp) :: A_Ito;     !. 9
    real(rp) :: A_INaK;    !.10
    real(rp) :: A_Jserca;  !.11
    real(rp) :: A_tauINaL; !.12
    real(rp) :: p_Gtos;   !.13
    real(rp) :: p_Gtof;   !.14
  end type t_prm
  
  
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_GRHF(lu_st)
!
integer(ip), intent(in)   :: lu_st

write(lu_st,'(A)',advance='no') '% '
write(lu_st,'(A)',advance='no') 'Cai '
write(lu_st,'(A)',advance='no') 'CaSR '
write(lu_st,'(A)',advance='no') 'CaSL '
write(lu_st,'(A)',advance='no') 'CaJ '
write(lu_st,'(A)',advance='no') 'Nai '
write(lu_st,'(A)',advance='no') 'NaSL '
write(lu_st,'(A)',advance='no') 'NaJ '
write(lu_st,'(A)',advance='no') 'Ki '
write(lu_st,'(A)',advance='no') 'm '
write(lu_st,'(A)',advance='no') 'h '
write(lu_st,'(A)',advance='no') 'j '
write(lu_st,'(A)',advance='no') 'ml '
write(lu_st,'(A)',advance='no') 'hl '
write(lu_st,'(A)',advance='no') 'd '
write(lu_st,'(A)',advance='no') 'f '
write(lu_st,'(A)',advance='no') 'fCaBJ '
write(lu_st,'(A)',advance='no') 'fCaBSL '
write(lu_st,'(A)',advance='no') 'xtos '
write(lu_st,'(A)',advance='no') 'ytos '
write(lu_st,'(A)',advance='no') 'xtof '
write(lu_st,'(A)',advance='no') 'ytof '
write(lu_st,'(A)',advance='no') 'xkr '
write(lu_st,'(A)',advance='no') 'xks '
write(lu_st,'(A)',advance='no') 'RyRr '
write(lu_st,'(A)',advance='no') 'RyRo '
write(lu_st,'(A)',advance='no') 'RyRi '
write(lu_st,'(A)',advance='no') 'NaBJ '
write(lu_st,'(A)',advance='no') 'NaBSL '
write(lu_st,'(A)',advance='no') 'TnCL '
write(lu_st,'(A)',advance='no') 'TnCHC '
write(lu_st,'(A)',advance='no') 'TnCHM '
write(lu_st,'(A)',advance='no') 'CaM '
write(lu_st,'(A)',advance='no') 'MyoC '
write(lu_st,'(A)',advance='no') 'MyoM '
write(lu_st,'(A)',advance='no') 'SRB '
write(lu_st,'(A)',advance='no') 'SLLJ '
write(lu_st,'(A)',advance='no') 'SLLSL '
write(lu_st,'(A)',advance='no') 'SLHJ '
write(lu_st,'(A)',advance='no') 'SLHSL '
write(lu_st,'(A)',advance='no') 'CSQNB '
end subroutine write_state_GRHF
!------------------------------------------------------------------------------!
subroutine write_current_GRHF(lu_cr)
!
integer(ip), intent(in)   :: lu_cr

write(lu_cr,'(A)',advance='no') '% '
write(lu_cr,'(A)',advance='no') 'INa '
write(lu_cr,'(A)',advance='no') 'INaL '
write(lu_cr,'(A)',advance='no') 'INaBk '
write(lu_cr,'(A)',advance='no') 'INaK '
write(lu_cr,'(A)',advance='no') 'IKr '
write(lu_cr,'(A)',advance='no') 'IKs '
write(lu_cr,'(A)',advance='no') 'IKp '
write(lu_cr,'(A)',advance='no') 'Itos '
write(lu_cr,'(A)',advance='no') 'Itof '
write(lu_cr,'(A)',advance='no') 'Ito '
write(lu_cr,'(A)',advance='no') 'IK1 '
write(lu_cr,'(A)',advance='no') 'IclCa '
write(lu_cr,'(A)',advance='no') 'IclBk '
write(lu_cr,'(A)',advance='no') 'ICa '
write(lu_cr,'(A)',advance='no') 'ICaK '
write(lu_cr,'(A)',advance='no') 'ICaNa '
write(lu_cr,'(A)',advance='no') 'ICaL '
write(lu_cr,'(A)',advance='no') 'INaCa '
write(lu_cr,'(A)',advance='no') 'IpCa '
write(lu_cr,'(A)',advance='no') 'ICaBk '
write(lu_cr,'(A)',advance='no') 'IKtot '
write(lu_cr,'(A)',advance='no') 'INatot '
write(lu_cr,'(A)',advance='no') 'ICltot '
write(lu_cr,'(A)',advance='no') 'ICatot '
write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_GRHF
!------------------------------------------------------------------------------!
!------------------------------------------------------------------------------!
function get_parameter_GRHF (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_grHF)

  select case (tcell) !16-> ENDO, 17->EPI
  case (GRHF_ENDO_mod)
  v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, p_Gtos_endo, p_Gtof_endo /)    
  case (GRHF_EPI_mod)
  v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, p_Gtos_epi, p_Gtof_epi /)    
  case default
    write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_GRHF
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param

  param%A_INaL     = v_prm( 1)   !. 1
  param%A_Ikr      = v_prm( 2)   !. 2
  param%A_Iks      = v_prm( 3)   !. 3
  param%A_ICaL     = v_prm( 4)   !. 4
  param%A_JSRleak  = v_prm( 5)   !. 5
  param%A_Incx     = v_prm( 6)   !. 6
  param%A_ICab      = v_prm( 7)   !. 7
  param%A_IK1      = v_prm( 8)   !. 8
  param%A_Ito      = v_prm( 9)   !. 9
  param%A_INaK     = v_prm(10)   !. 10
  param%A_Jserca   = v_prm(11)   !. 11
  param%A_tauINaL  = v_prm(12)   !. 12
  param%p_Gtos    = v_prm(13)   !. 13
  param%p_Gtof    = v_prm(14)   !. 14
  
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_GrandiHF(ict) result(grHF_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Ten Tusscher Model
! tt_m is the main structure which contains the membrane potential, ioninc 
! concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none
!
! Heart Failure Grandi2009
!  
  integer (ip), intent(in)  :: ict
  real(rp)                  :: grHF_ic(nvar_grHF)
  
  select case (ict)
  case(GRHF_ENDO_mod) !.----ENDO
    grHF_ic = (/ Cai, CaSR, CaSL, CaJ, Nai, NaSL, NaJ, Ki, m, h, j, ml, hl, d, f, &
               fCaBJ, fCaBSL, xtos, ytos, xtof, ytof, xkr, xks, RyRr, RyRo,     &
               RyRi, NaBJ, NABSL, TnCL, TnCHC, TnCHM, CaM, MyoC, MyoM, SRB,     &
               SLLJ, SLLSL, SLHJ, SLHSL, CSQNB /)
  case(GRHF_EPI_mod) !.----EPI
    grHF_ic = (/ Cai, CaSR, CaSL, CaJ, Nai, NaSL, NaJ, Ki, m, h, j, ml, hl, d, f, &
               fCaBJ, fCaBSL, xtos, ytos, xtof, ytof, xkr, xks, RyRr, RyRo,     &
               RyRi, NaBJ, NABSL, TnCL, TnCHC, TnCHM, CaM, MyoC, MyoM, SRB,     &
               SLLJ, SLLSL, SLHJ, SLHSL, CSQNB /)
  case default
    grHF_ic = (/ Cai, CaSR, CaSL, CaJ, Nai, NaSL, NaJ, Ki, m, h, j, ml, hl, d, f, &
               fCaBJ, fCaBSL, xtos, ytos, xtof, ytof, xkr, xks, RyRr, RyRo,     &
               RyRi, NaBJ, NABSL, TnCL, TnCHC, TnCHM, CaM, MyoC, MyoM, SRB,     &
               SLLJ, SLLSL, SLHJ, SLHSL, CSQNB /)
  end select
  !.

  return
end function ic_GrandiHF
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_grmHF), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_grHF)
  v_me( 1) = str_me%Cai
  v_me( 2) = str_me%CaSR
  v_me( 3) = str_me%CaSL
  v_me( 4) = str_me%CaJ
  v_me( 5) = str_me%Nai
  v_me( 6) = str_me%NaSL
  v_me( 7) = str_me%NaJ
  v_me( 8) = str_me%Ki
  v_me( 9) = str_me%m
  v_me(10) = str_me%h
  v_me(11) = str_me%j
  v_me(12) = str_me%ml
  v_me(13) = str_me%hl
  v_me(14) = str_me%d
  v_me(15) = str_me%f
  v_me(16) = str_me%fCaBJ
  v_me(17) = str_me%fCaBSL
  v_me(18) = str_me%xtos
  v_me(19) = str_me%ytos
  v_me(20) = str_me%xtof
  v_me(21) = str_me%ytof
  v_me(22) = str_me%xkr
  v_me(23) = str_me%xks
  v_me(24) = str_me%RyRr
  v_me(25) = str_me%RyRo
  v_me(26) = str_me%RyRi
  v_me(27) = str_me%NaBJ
  v_me(28) = str_me%NABSL
  v_me(29) = str_me%TnCL
  v_me(30) = str_me%TnCHC
  v_me(31) = str_me%TnCHM
  v_me(32) = str_me%CaM
  v_me(33) = str_me%MyoC
  v_me(34) = str_me%MyoM
  v_me(35) = str_me%SRB
  v_me(36) = str_me%SLLJ
  v_me(37) = str_me%SLLSL
  v_me(38) = str_me%SLHJ
  v_me(39) = str_me%SLHSL
  v_me(40) = str_me%CSQNB
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_grmHF), intent(out) :: str_me
  
  str_me%Cai    =  v_me( 1);
  str_me%CaSR   =  v_me( 2);
  str_me%CaSL   =  v_me( 3);
  str_me%CaJ    =  v_me( 4);
  str_me%Nai    =  v_me( 5);
  str_me%NaSL   =  v_me( 6);
  str_me%NaJ    =  v_me( 7);
  str_me%Ki     =  v_me( 8);
  str_me%m      =  v_me( 9);
  str_me%h      =  v_me(10);
  str_me%j      =  v_me(11);
  str_me%ml     =  v_me(12);
  str_me%hl     =  v_me(13);
  str_me%d      =  v_me(14);
  str_me%f      =  v_me(15);
  str_me%fCaBJ  =  v_me(16);
  str_me%fCaBSL =  v_me(17);
  str_me%xtos   =  v_me(18);
  str_me%ytos   =  v_me(19);
  str_me%xtof   =  v_me(20);
  str_me%ytof   =  v_me(21);
  str_me%xkr    =  v_me(22);
  str_me%xks    =  v_me(23);
  str_me%RyRr   =  v_me(24);
  str_me%RyRo   =  v_me(25);
  str_me%RyRi   =  v_me(26);
  str_me%NaBJ   =  v_me(27);
  str_me%NABSL  =  v_me(28);
  str_me%TnCL   =  v_me(29);
  str_me%TnCHC  =  v_me(30);
  str_me%TnCHM  =  v_me(31);
  str_me%CaM    =  v_me(32);
  str_me%MyoC   =  v_me(33);
  str_me%MyoM   =  v_me(34);
  str_me%SRB    =  v_me(35);
  str_me%SLLJ   =  v_me(36);
  str_me%SLLSL  =  v_me(37);
  str_me%SLHJ   =  v_me(38);
  str_me%SLHSL  =  v_me(39);
  str_me%CSQNB  =  v_me(40); 
  return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine concentrations (dt, cur, prm, grmHF)
!-------------------------------------------------------------------------------
! Function to update Ion concentrations
!-------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)      :: dt
  type (t_cur), intent(in)      :: cur
  type (t_prm), intent(in)      :: prm
  type (t_grmHF),   intent(inout) :: grmHF
  
  real(rp):: dNaBJ, dNaBSL, dTnCl, dTnChc, dTnChm, dCaM, &
             dMyoc, dMyom, dSRB, JCaBcytosol, dSLLJ,     &
             dSLLSL, dSLHJ, dSLHSL, JCaBJunction, JCaBSL,&
             dCSQNB, dNaJ, dNaSL, dNai,JSRCarel, JserCa, &
             JSRleak, dCaJ, dCaSL, dCai, dCaSR

  ! Sodium Buffers
  dNaBJ = p_konNa * grmHF%NaJ * (p_BmaxNaJ - grmHF%NaBJ) - p_koffNa * grmHF%NaBJ;
  dNaBSL = p_konNa * grmHF%NaSL * (p_BmaxNaSL - grmHF%NaBSL) - p_koffNa * grmHF%NaBSL;
  
  ! Cytosolic Ca Buffers
  dTnCL = p_konTnCl * grmHF%Cai * (p_BmaxTnClow - grmHF%TnCl) - & 
          p_koffTnCl * grmHF%TnCl;
  dTnChc = p_konTnChCa * grmHF%Cai * (p_BmaxTnChigh - grmHF%TnChc - &
           grmHF%TnChm) - p_koffTnChCa * grmHF%TnChc;
  dTnChm = p_konTnChMg * p_Mgi * (p_BmaxTnChigh - grmHF%TnChc - &
           grmHF%TnChm) - p_koffTnChMg * grmHF%TnChm;
  dCaM = p_konCaM * grmHF%Cai * (p_BmaxCaM - grmHF%CaM) - p_koffCaM * grmHF%CaM;
  dMyoc = p_konMyoCa * grmHF%Cai * (p_BmaxMyosin - grmHF%Myoc - grmHF%Myom) - &
          p_koffMyoCa * grmHF%Myoc;
  dMyom = p_konMyoMg * p_Mgi * (p_BmaxMyosin - grmHF%Myoc - grmHF%Myom) - &
          p_koffMyoMg * grmHF%Myom;
  dSRB = p_konSR * grmHF%Cai * (p_BmaxSR - grmHF%SRB) - p_koffSR * grmHF%SRB;
  JCaBcytosol = dTnCl + dTnChc + dTnChm + dCaM + dMyoc + dMyom + dSRB;

  ! Junctional and SL Ca Buffers
  dSLLJ = p_konSLl * grmHF%CaJ * (p_BmaxSLlowJ - grmHF%SLLJ) - &
          p_koffSLl * grmHF%SLLJ;
  dSLLSL = p_konSLl * grmHF%CaSL * (p_BmaxSLlowSL - grmHF%SLLSL) - &
           p_koffSLl * grmHF%SLLSL;
  dSLHJ = p_konSLh * grmHF%CaJ * (p_BmaxSLhighJ - grmHF%SLHJ) - &
          p_koffSLh * grmHF%SLHJ;
  dSLHSL = p_konSLh * grmHF%CaSL * (p_BmaxSLhighSL - grmHF%SLHSL) - &
           p_koffSLh * grmHF%SLHSL;
  JCaBJunction = dSLLJ + dSLHJ;
  JCaBSL = dSLLSL + dSLHSL;
  
  ! SR Ca Buffer
  dCSQNB = p_konCsqn * grmHF%CaSR * (p_BmaxCsqn - grmHF%CSQNB) - &
           p_koffCsqn * grmHF%CSQNB;
    
  ! Sodium Concentrations 
  dNaJ = -cur%INatotJunc * p_Cap / (p_VJunc * p_F) + &
          p_JNaJuncSL / p_VJunc * (grmHF%NaSL - grmHF%NaJ) - dNaBJ;
  dNaSL = -cur%INatotSL * p_Cap / (p_VSL * p_F) + &
          p_JNaJuncSL / p_VSL * (grmHF%NaJ - grmHF%NASL) + &
          p_JNaSLMyo / p_VSL * (grmHF%Nai - grmHF%NaSL) - dNaBSL;
  dNai = p_JNaSLMyo / p_VMyo * (grmHF%NaSL - grmHF%Nai); 
  
  ! Calcium concentrations
  JSRCarel = p_ks * grmHF%RyRo * (grmHF%CaSR - grmHF%CaJ);
  JserCa = prm%A_Jserca*p_VmaxSRCaP * ((grmHF%Cai/p_Kmf) ** p_hillSRCaP - &
           (grmHF%CaSR/p_Kmr)**p_hillSRCaP) / (1 + (grmHF%Cai/p_Kmf)**p_hillSRCaP + &
           (grmHF%CaSR/p_Kmr) ** p_hillSRCaP);
  JSRleak = prm%A_JSRleak*p_GSRleak * (grmHF%CaSR - grmHF%CaJ);
  
  dCaJ = -cur%ICatotJunc * p_Cap / (p_VJunc * 2 * p_F) + &
         p_JCaJuncSL / p_VJunc * (grmHF%CaSL - grmHF%CaJ) - JCaBJunction + &
         (JSRCarel) * p_VSR / p_VJunc + JSRleak * p_VMyo / p_VJunc;
  dCaSL = -cur%ICatotSL * p_Cap / (p_VSL * 2 * p_F) + p_JCaJuncSL / &
         p_VSL * (grmHF%CaJ - grmHF%CaSL) + p_JCaSLMyo / p_VSL * &
         (grmHF%Cai - grmHF%CaSL) - JCaBSL;
  dCai = -JserCa * p_VSR / p_VMyo - JCaBCytosol + p_JCaSLMyo / p_VMyo * &
         (grmHF%CaSL - grmHF%Cai);  
  
  dCaSR = JserCa - (JSRleak * p_VMyo / p_VSR + JSRCarel) - dCSQNB;
      
  grmHF%NaBJ = grmHF%NaBJ + dNaBJ * dt;
  grmHF%NaBSL = grmHF%NaBSL + dNaBSL * dt;
   
  grmHF%TnCl = grmHF%TnCl + dTnCl * dt;
  grmHF%TnChc = grmHF%TnChc + dTnChc * dt;
  grmHF%TnChm = grmHF%TnChm + dTnChm * dt;
  grmHF%CaM = grmHF%CaM + dCaM * dt;
  grmHF%Myoc = grmHF%Myoc + dMyoc * dt;
  grmHF%Myom = grmHF%Myom + dMyom * dt;
  grmHF%SRB = grmHF%SRB + dSRB * dt;
  
  grmHF%SLLJ = grmHF%SLLJ + dSLLJ * dt;
  grmHF%SLLSL = grmHF%SLLSL + dSLLSL * dt;
  grmHF%SLHJ = grmHF%SLHJ + dSLHJ * dt;
  grmHF%SLHSL = grmHF%SLHSL + dSLHSL * dt;
  
  grmHF%CSQNB = grmHF%CSQNB + dCSQNB * dt;
  
  grmHF%NaJ = grmHF%NaJ + dNaJ * dt;
  grmHF%NaSL = grmHF%NaSL + dNaSL * dt;
  grmHF%Nai = grmHF%Nai + dNai * dt;
  
  grmHF%CaJ = grmHF%CaJ + dCaJ * dt;
  grmHF%CaSL = grmHF%CaSL + dCaSL * dt;
  grmHF%Cai = grmHF%Cai + dCai * dt;
  grmHF%CaSR = grmHF%CaSR + dCaSR * dt;
  
  return
end subroutine concentrations
!------------------------------------------------------------------------------
subroutine gates(dt, U, prm, grmHF)
!------------------------------------------------------------------------------
! This function updates the gating variables
!
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)      :: dt, U
  type(t_grmHF), intent(inout) :: grmHF
  type (t_prm), intent(in)     :: prm
  real (rp):: mss, taum, ah, bh, tauh, hss, aj, bj, tauj,      &
              jss, dm, dh, dj, xrss, tauxr, dxkr, xsss,        &
              tauxs, dxks,xtoss, ytoss, tauxtos, tauytos,      &
              dxtos, dytos, tauxtof, tauytof, dxtof, dytof,    &
              dss, taud, fss, tauf, dd, df, dfCaBJ, dfCaBSL,   &
              MaxSR, MinSR, kCaSR, koSRCa, kiSRCa, RI, dRyRr,  &
              dRyRo, dRyRi, aml, bml, mlss, hlss, tauml, tauhl,&
              dml, dhl
   
  ! I_Na: Fast Na Current
  mss = 1 / ((1 + exp( -(56.86 + U) / 9.03))**2);
  taum = 0.1292 * exp(-((U + 45.79) / 15.54)**2) + &
         0.06487 * exp(-((U - 4.823) / 51.12)**2);
  if(U >= -40) then
    ah = 0; 
    bh = 0.77 / (0.13 * (1 + exp(-(U + 10.66) / 11.1 )));

    aj = 0;
    bj = (0.6 * exp(0.057 * U)) / (1 + exp(-0.1 * (U + 32)));
  else
    ah = 0.057 * exp(-(U + 80) / 6.8); 
    bh = 2.7 * exp(0.079 * U) + 3.1e5 * exp(0.3485 * U); 

    aj = (-2.5428e4 * exp(0.2444 * U) - 6.948e-6 * exp(-0.04391 * U)) * &
         (U + 37.78) / (1 + exp(0.311 * (U + 79.23)));
    bj = (0.02424 * exp(-0.01052 * U )) / (1 + exp(-0.1378 * (U + 40.14)));
  endif
  
  tauh = 1 / (ah + bh); 
  hss = 1 / ((1 + exp((U + 71.55) / 7.43))**2);
 
  tauj = 1 / (aj + bj);
  jss = 1 / ((1 + exp((U + 71.55) / 7.43))**2);         
 
  dm = (mss - grmHF%m) / taum;
  dh = (hss - grmHF%h) / tauh;
  dj = (jss - grmHF%j) / tauj;

  ! I_NaL: Slow Na Current
  aml=0.32*(U+47.13)/(1-exp(-0.1*(U+47.13))); 
  bml=0.08*exp(-U/11.0); 

  mlss=aml/(aml + bml); 
  tauml = 1 / (aml + bml); 
 
  hlss=1/(1+exp((U+91)/6.1));
  tauhl=prm%A_tauINaL*p_tauhl;   

  dml = (mlss - grmHF%ml) / taum;
  dhl = (hlss - grmHF%hl) / tauhl;
    
  ! I_kr: Rapidly Activating K Current
  xrss = 1 / (1 + exp(-(U + 10) / 5));
  tauxr = 550 / (1 + exp((-22 - U) / 9)) * 6 / (1 + exp((U - (-11)) / 9)) + &
          230 / (1 + exp((U - (-40)) / 20));
  dxkr = (xrss - grmHF%xkr) / tauxr;
  
  ! I_ks: Slowly Activating K Current  
  xsss = 1 / (1 + exp(-(U + 3.8) / 14.25));
  tauxs = 990.1 / (1 + exp(-(U + 2.436) / 14.12));
  dxks = (xsss-grmHF%xks) / tauxs;
    
  ! I_to: Transient Outward K Current
  !Slow component
  xtoss = 1 / (1 + exp(-(U - 19.0) / 13));
  ytoss = 1 / (1 + exp((U + 19.5) / 5));
  tauxtos = 9 / (1 + exp((U + 3.0) / 15)) + 0.5;
  tauytos = 800 / (1 + exp((U + 60.0) / 10)) + 30;
  dxtos = (xtoss - grmHF%xtos) / tauxtos;
  dytos = (ytoss - grmHF%ytos) / tauytos;
  !Fast component
  tauxtof = 8.5 * exp(-((U + 45) / 50)**2) + 0.5;
  tauytof = 85 * exp((-(U + 40)**2 / 220)) + 7;
  dxtof = (xtoss - grmHF%xtof) / tauxtof;
  dytof = (ytoss - grmHF%ytof) / tauytof;

  ! I_Ca: L-type Calcium Current
  dss = 1 / (1 + exp(-(U + 5) / 6.0));
  taud = dss * (1 - exp(-(U + 5) / 6.0)) / (0.035 * (U + 5));
  fss = 1 / (1 + exp((U + 35) / 9)) + 0.6 / (1 + exp((50 - U) / 20));
  tauf = 1 / (0.0197 * exp(-(0.0337 * (U + 14.5))**2) + 0.02);
  dd = (dss - grmHF%d) / taud;
  df = (fss - grmHF%f) / tauf;
  dfCaBJ = 1.7 * grmHF%CaJ * (1 - grmHF%fCaBJ) - 11.9e-3 * grmHF%fCaBJ;
  dfCaBSL = 1.7 * grmHF%CaSL * (1 - grmHF%fCaBSL) - 11.9e-3 * grmHF%fCaBSL;
  
  ! SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
  MaxSR = 15; 
  MinSR = 1;
  kCaSR = MaxSR - (MaxSR - MinSR) / (1 + (p_ec50SR / grmHF%CaSR)**2.5);
  koSRCa = p_koCa / kCaSR;
  kiSRCa = p_kiCa * kCaSR;
  RI = 1 - grmHF%RyRr - grmHF%RyRo - grmHF%RyRi;
  dRyRr = (p_kim * RI - kiSRCa * grmHF%CaJ * grmHF%RyRr) - &
          (koSRCa * grmHF%CaJ**2 * grmHF%RyRr - p_kom * grmHF%RyRo);
  dRyRo = (koSRCa * grmHF%CaJ**2 * grmHF%RyRr - p_kom * grmHF%RyRo) - &
          (kiSRCa * grmHF%CaJ * grmHF%RyRo - p_kim * grmHF%RyRi);
  dRyRi = (kiSRCa * grmHF%CaJ * grmHF%RyRo - p_kim * grmHF%RyRi) - &
          (p_kom * grmHF%RyRi - koSRCa * grmHF%CaJ**2 * RI);
  
  !Update gates
  grmHF%m = grmHF%m + dm * dt;
  grmHF%h = grmHF%h + dh * dt;
  grmHF%j = grmHF%j + dj * dt;
  
  grmHF%ml = grmHF%ml + dml * dt;
  grmHF%hl = grmHF%hl + dhl * dt;

  grmHF%xkr = grmHF%xkr + dxkr * dt;    
  grmHF%xks = grmHF%xks + dxks * dt;
  
  grmHF%xtof = grmHF%xtof + dxtof * dt;
  grmHF%ytof = grmHF%ytof + dytof * dt;
  grmHF%xtos = grmHF%xtos + dxtos * dt;
  grmHF%ytos = grmHF%ytos + dytos * dt;

  grmHF%d = grmHF%d + dd * dt;
  grmHF%f = grmHF%f + df * dt;
  grmHF%fCaBJ = grmHF%fCaBJ + dfCaBJ * dt;
  grmHF%fCaBSL = grmHF%fCaBSL + dfCaBSL * dt;
  
  grmHF%RyRr = grmHF%RyRr + dRyRr * dt;
  grmHF%RyRo = grmHF%RyRo + dRyRo * dt;
  grmHF%RyRi = grmHF%RyRi + dRyRi * dt;
  
  return
end subroutine gates
!------------------------------------------------------------------------------
subroutine currents ( U, grmHF, Iion, prm, cur) 
!------------------------------------------------------------------------------
! This function computes currents and concentrations for the Grandi Model
!
!
! XXX:AA-BB, 2008
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: U
  type(t_grmHF), intent(in)   :: grmHF
  real(rp),    intent(out)  :: Iion
  type(t_prm), intent(in)   :: prm
  type(t_cur), intent(out)  :: cur
  !.
  real(rp):: ENaJunc, ENaSL, EK, Eks, ECaJunc, ECaSL, ECl, sigma, &
             fNaK, gkr, rkr, kpkp, aK1, bK1, K1ss, KaJunc,        &
             KaSL, s1Junc, s2Junc, s3Junc, s1SL, s2SL, s3SL,      &
             IbarCaJ, IbarCaSL, IbarK, IbarNaJ, IbarNaSL

  !. Nerst Potentials
  ENaJunc = p_RTF * log(p_Nao / grmHF%NaJ);
  ENaSL = p_RTF * log(p_Nao / grmHF%NaSL);
  EK = p_RTF * log(p_Ko / grmHF%Ki);
  Eks = p_RTF * log((p_Ko + p_pNaK * p_Nao) / &
        (grmHF%Ki + p_pNaK * grmHF%Nai));
  ECaJunc = p_RTF / 2 * log(p_Cao / grmHF%CaJ);
  ECaSL = p_RTF / 2 * log(p_Cao / grmHF%CaSL);
  ECl = p_RTF * log(p_Cli / p_Clo);
  
  ! INa: Fast Na Current
  
  cur%INaJunc =  p_FJunc * p_GNa * grmHF%m**3 * grmHF%h * grmHF%j * (U - ENaJunc);
  cur%INaSL =  p_FSL * p_GNa * grmHF%m**3 * grmHF%h * grmHF%j * (U - ENaSL);
  cur%INa = cur%INaJunc + cur%INaSL;

  ! INaL: Slow Na Current
  
  cur%INaLJunc = prm%A_INaL * p_FJunc * p_GNaL * grmHF%ml**3 * grmHF%hl * (U - ENaJunc);
  cur%INaLSL = prm%A_INaL * p_FSL * p_GNaL * grmHF%ml**3 * grmHF%hl * (U - ENaSL);
  cur%INaL = cur%INaLJunc + cur%INaLSL;

  ! I_nabk: Na Background Current
  cur%INaBkJunc = p_FJunc * p_GNaB * (U - ENaJunc);
  cur%INaBkSL = p_FSL * p_GNaB * (U - ENaSL);
  cur%INaBk = cur%INaBkJunc + cur%INaBkSL;

  ! I_nak: Na/K Pump current
  sigma = (exp(p_Nao / 67.3) - 1) / 7;
  fNaK = 1 / (1 + 0.1245 * exp(-0.1 * U * p_iRTF) + &
         0.0365 * sigma * exp(-U*p_iRTF));
  cur%INaKJunc = prm%A_INaK * p_FJunc * p_IbarNaK * fnak * p_Ko / &
         (1 + (p_KmNaip / grmHF%NaJ)**4) / (p_Ko + p_KmKo);
  cur%INaKSL = prm%A_INaK * p_FSL * p_IbarNaK * fnak * p_Ko / &
         (1 + (p_KmNaip / grmHF%NaSL)**4) / (p_Ko + p_KmKo);
  cur%INaK = cur%INaKJunc + cur%INaKSL;

  ! I_kr: Rapidly Activating K Current
  gkr = 1.0 * 0.035 *sqrt(p_Ko / 5.4);
  rkr = 1 / (1 + exp((U + 74) / 24));
  cur%Ikr = prm%A_Ikr * gkr * grmHF%xkr * rkr * (U - EK);
 
  ! I_ks: Slowly Activating K Current
  cur%IksJunc = prm%A_Iks * p_FJunc * p_GKsJunc * grmHF%xks**2 * (U - Eks);
  cur%IksSL = prm%A_Iks * p_FSL * p_GKsSL * grmHF%xks**2 * (U - Eks);
  cur%Iks = cur%IksJunc + cur%IksSL;
  
  ! I_kp: Plateau K Current
  kpkp = 1 / (1 + exp(7.488 - U / 5.98));
  cur%IkpJunc = p_FJunc * p_Gkp * kpkp * (U - EK);
  cur%IkpSL = p_FSL * p_Gkp * kpkp * (U - EK);
  cur%Ikp = cur%IkpJunc + cur%IkpSL;
  
  ! I_to: Transient Outward K Current
  ! Slow component
  cur%Itos = prm%A_Ito * prm%p_Gtos * grmHF%xtos * grmHF%ytos * (U - EK);
  ! Fast component
  cur%Itof = prm%A_Ito * prm%p_Gtof * grmHF%xtof * grmHF%ytof * (U - EK);
    
  cur%Ito = cur%Itos + cur%Itof;

  ! I_K1: Inward Rectifier K Current
  aK1 = 1.02 / (1 + exp(0.2385 * (U - EK - 59.215)));
  bK1 =(0.49124 * exp(0.08032 * (U + 5.476 - EK)) + &
       exp(0.06175 * (U - EK - 594.31))) / (1 + exp(-0.5143 * (U - EK + 4.753)));
  K1ss = aK1/(aK1+bK1);
  cur%IK1 =prm%A_IK1 * p_GK1 * sqrt(p_Ko / 5.4) * K1ss * (U - EK);

  ! I_ClCa: Ca-activated Cl Current
  cur%IClCaJunc = p_FJunc * p_GClCa / (1 + p_KdClCa / grmHF%CaJ) * (U - ECL);
  cur%IClCaSL = p_FSL * p_GClCa / (1 + p_KdClCa / grmHF%CaSL) * (U - ECL);
  cur%IClCa = cur%IClCaJunc + cur%IClCaSL;

  ! I_Clbk: Background Cl Current
  cur%IClBk = p_GClB * (U - ECl);
  
  ! I_Ca: L-type Calcium Current
     
  IbarCaJ = p_pCa * 4 * (U * p_F * p_iRTF) * (0.341 * grmHF%CaJ * &
            exp(2 * U * p_iRTF) - 0.341 * p_Cao) / (exp(2 * U * p_iRTF) - 1);
  IbarCaSL = p_pCa * 4 * (U * p_F * p_iRTF) * (0.341 * grmHF%CaSL * &
            exp(2 * U * p_iRTF) - 0.341 * p_Cao) / (exp(2 * U * p_iRTF) - 1);
  IbarK = p_pK * (U * p_F * p_iRTF) * (0.75 * grmHF%Ki * &
            exp(U * p_iRTF) - 0.75 * p_Ko) / (exp(U * p_iRTF) - 1);
  IbarNaJ = p_pNa * (U * p_F * p_iRTF) * (0.75 * grmHF%NaJ * &
            exp(U * p_iRTF) - 0.75 * p_Nao) / (exp(U * p_iRTF) - 1);
  IbarNaSL = p_pNa * (U * p_F * p_iRTF) * (0.75 * grmHF%NaSL * &
            exp(U * p_iRTF) - 0.75 * p_Nao) / (exp(U * p_iRTF) - 1);

  cur%ICaJunc = prm%A_ICaL * (p_FJuncCaL * IbarCaJ * grmHF%d * grmHF%f * &
            (1 - grmHF%fCaBJ)) * 0.45;
  cur%ICaSL = prm%A_ICaL * (p_FSLCaL * IbarCaSL * grmHF%d * grmHF%f * &
            (1 - grmHF%fCaBSL)) * 0.45;
  cur%ICa = cur%ICaJunc + cur%ICaSL;
  
  cur%ICaK = (IbarK * grmHF%d * grmHF%f * (p_FJuncCaL * (1 - grmHF%fCaBJ) + &
            p_FSLCaL * (1 - grmHF%fCaBSL))) * 0.45;
    
  cur%ICaNaJunc = (p_FJuncCaL * IbarNaJ * grmHF%d * grmHF%f * &
                 (1 - grmHF%fCaBJ)) * 0.45;
  cur%ICaNaSL = (p_FSLCaL * IbarNaSL * grmHF%d * grmHF%f * &
                 (1 - grmHF%fCaBSL)) * 0.45;  
  cur%ICaNa = cur%ICaNaJunc + cur%ICaNaSL;

  cur%ICaL = cur%ICa + cur%ICaK + cur%ICaNa;

  ! I_ncx: Na/Ca Exchanger current
  KaJunc = 1 / (1 + (p_Kdact / grmHF%CaJ)**2);
  KaSL = 1 / (1 + (p_Kdact / grmHF%CaSL)**2);
  s1Junc = exp(p_nu * U * p_iRTF) * grmHF%NaJ**3 * p_Cao;
  s1SL = exp(p_nu * U * p_iRTF) * grmHF%NaSL**3 * p_Cao;
  s2Junc = exp((p_nu - 1) * U * p_iRTF) * p_Nao**3 * grmHF%CaJ;
  s3Junc = p_KmCai * p_Nao**3 * (1 + (grmHF%NaJ / p_KmNai)**3) + &
           p_KmNao**3 * grmHF%CaJ * (1 + grmHF%CaJ / p_KmCai) + &
           p_KmCao * grmHF%NaJ**3 + grmHF%NaJ**3 * p_Cao + &
           p_Nao**3 * grmHF%CaJ;
  s2SL = exp((p_nu - 1) * U * p_iRTF) * p_Nao**3 * grmHF%CaSL;
  s3SL = p_KmCai * p_Nao**3 * (1 + (grmHF%NaSL / p_KmNai)**3) + &
         p_KmNao**3 * grmHF%CaSL * (1 + grmHF%CaSL / p_KmCai) + &
         p_KmCao * grmHF%NaSL**3 + grmHF%NaSL**3 * p_Cao + &
         p_Nao**3 * grmHF%CaSL;

  cur%IncxJunc = prm%A_Incx * p_FJunc * p_IbarNCX * KaJunc * (s1Junc - s2Junc) / &
         s3Junc / (1 + p_ksat * exp((p_nu - 1) * U * p_iRTF));
  cur%IncxSL = prm%A_Incx * p_Fsl * p_IbarNCX * KaSL * (s1SL - s2SL) / s3SL / &
         (1 + p_ksat * exp((p_nu - 1) * U * p_iRTF));
  cur%Incx = cur%IncxJunc + cur%IncxSL;

  ! I_pCa: Sarcolemmal Ca Pump Current
  cur%IpCaJunc = p_FJunc * p_IbarPMCA * grmHF%CaJ**1.6 / &
       (p_KmPCa**1.6 + grmHF%CaJ**1.6);
  cur%IpCaSL = p_FSL * p_IbarPMCA * grmHF%CaSL**1.6 / &
       (p_KmPCa**1.6 + grmHF%CaSL**1.6);
  cur%IpCa = cur%IpCaJunc + cur%IpCaSL;

  ! I_Ca_bk: Background Ca Current
  cur%ICaBkJunc = prm%A_ICab * p_FJunc * p_GCaB * (U - ECaJunc);
  cur%ICaBkSL = prm%A_ICab * p_FSL * p_GCaB * (U - ECaSL);
  cur%ICaBk = cur%ICaBkJunc + cur%ICaBkSL;
     
  ! Sodium Concentrations
  cur%INatotJunc = cur%INaJunc + cur%INaBkJunc + 3 * cur%IncxJunc + &
       3 * cur%INaKJunc + cur%ICaNaJunc + cur%INaLJunc;
  cur%INatotSL = cur%INaSL + cur%INaBkSL + 3 * cur%IncxSL + &
       3 * cur%INaKSL + cur%ICaNaSL + cur%INaLSL;

  ! Potassium concentration
  cur%IKtot = cur%Ito + cur%Ikr + cur%Iks + cur%Ik1 - 2 * cur%INaK + &
              cur%ICaK + cur%Ikp;
    
  ! Calcium concentrations
  cur%ICatotJunc = cur%ICaJunc + cur%ICaBkJunc + cur%IpCaJunc -  &
                   2 * cur%IncxJunc;
  cur%ICatotSL = cur%ICaSL + cur%ICaBkSL + cur%IpCaSL -  &
                   2 * cur%IncxSL;
  ! Membrane Potential
  cur%INatot = cur%INatotJunc + cur%INatotSL;
  cur%ICltot = cur%IClCa + cur%IClBk;
  cur%ICatot = cur%ICatotJunc + cur%ICatotSL;
  cur%Itot = cur%INatot + cur%ICltot + cur%ICatot + cur%IKtot;
   
  !------------------------------------
  Iion = cur%Itot;

  return
  
end subroutine currents
!-------------------------------------------------------------------------------
subroutine GrandiHF_A_P01 (ict,dt,U,Iion,v_prm,v_gr,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict   !.Cell Model 
  real(rp),    intent(in)    :: dt, U
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp), intent(inout)    :: v_gr(nvar_grHF)
  real(rp),    intent(out)   :: v_cr(:)
  !.
  type(t_grmHF)                :: grmHF
  type (t_prm)               :: param
  type(t_cur)                :: crr
  !
  call put_me_struct(v_gr,grmHF)
  call put_param(v_prm,param)
  !
  call currents ( U, grmHF, Iion, param, crr)
  call concentrations ( dt, crr, param, grmHF)
  call gates( dt, U, param, grmHF)
  !  
  call get_me_struct(grmHF,v_gr)
  !.
  v_cr(1:ncur_grHF) =(/ crr%INa, crr%INaL, crr%INaBk, crr%INaK, crr%Ikr, crr%Iks, crr%Ikp,   &
                 crr%Itos, crr%Itof, crr%Ito, crr%Ik1, crr%IclCa,crr%IClBk, &
                 crr%ICa, crr%ICaK, crr%ICaNa, crr%ICaL, crr%Incx, crr%IpCa,&
                 crr%ICaBk, crr%IKtot, crr%INatot, crr%ICltot, crr%ICatot,  &
                 crr%Itot /)
  
  return
end subroutine GrandiHF_A_P01
!-------------------------------------------------------------------------------
end module mod_grandiHF
!-------------------------------------------------------------------------------
