!------------------------------------------------------------------------------
! This are the parameters for the Grandi Model
! Grandi E, et al, A novel computational model of the human ventricular action potential and Ca 
! transient, J Mol Cell Cardiol (2009), doi:10.1016/j.yjmcc.2009.09.019

! Constants
!
! Parameters that can be modified through the input file
!
  !.--K currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_pNaK = 0.01833;               ! nS/pF
  real(rp), parameter, private :: p_GKp = 0.002;		  ! mS/uF
  real(rp), parameter, private :: p_GKsJunc = 0.0035;		  ! mS/uF
  real(rp), parameter, private :: p_GKsSL = 0.0035;		  ! mS/uF
  real(rp), parameter, private :: p_Gtos_endo =  0.4*0.037596;    ! Ito conductance for endo (mod for HF)
  real(rp), parameter, private :: p_Gtos_epi  =  0.4*0.0156;      ! Ito conductance for epi (mod for HF)
  real(rp), parameter, private :: p_Gtof_endo =  0.4*0.001404;    ! Ito conductance for endo (mod for HF)
  real(rp), parameter, private :: p_Gtof_epi  =  0.4*0.1144;      ! Ito conductance for epi (mod for HF)
  real(rp), parameter, private :: p_GK1  =  0.68*0.35;            ! IK1 conductance  (mod for HF)
  !.--Na transport -------------------------------------------------------------
  real(rp), parameter, private :: p_GNa   = 23;                   ! nS/pF
  real(rp), parameter, private :: p_GNaB   = 0.0*0.597e-3;        ! nS/pF (mod for HF)
  real(rp), parameter, private :: p_tauhl = 2*233;                ! ms    (mod for HF)
  real(rp), parameter, private :: p_GNaL   = 2*0.015;             ! nS/pF (mod for HF)
  !.--Ca transport -------------------------------------------------------------
  real(rp), parameter, private :: p_pNa  = 0.50*1.5e-8;           ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_pCa  = 0.50*5.4e-4;           ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_pK  = 0.50*2.7e-7;            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_GCaB = 1.53*5.513e-4;         ! [A/F] (mod for HF)
  !.--Cl currents --------------------------------------------------------------
  real(rp), parameter, private :: p_GClCa  = 0.5* 0.109625;       !  [mS/uF]
  real(rp), parameter, private :: p_GClB  = 9e-3;                 !  [mS/uF]
  !.--Concentrations ----------------------------------------------------------
  real(rp), parameter, private :: p_Cli = 15;	! [mM]
  real(rp), parameter, private :: p_Clo = 150;	! [mM]
  real(rp), parameter, private :: p_Ko = 5.4;	! [mM]
  real(rp), parameter, private :: p_Nao = 140;	! [mM]
  real(rp), parameter, private :: p_Cao = 1.8;	! [mM]
  real(rp), parameter, private :: p_Mgi = 1;	! [mM]
!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_R     = 8314;			!. JK^-1kmol^-1  
  real(rp), parameter, private :: p_T     = 310.0;          !. K
  real(rp), parameter, private :: p_F     = 96485;          !. C/mol
  real(rp), parameter, private :: p_RTF   = p_R*p_T/p_F;    !. 1/mV
  real(rp), parameter, private :: p_iRTF  = p_F/p_R/p_T;    !. mV
  !.--Capacitance --------------------------------------------------------------
  real(rp), parameter, private :: p_Cap   = 1.3810e-10;	! [F]
  !.--Enviromental parameters --------------------------------------------------
  real(rp), parameter, private :: pi = 2*acos(0.0);
  real(rp), parameter, private :: p_cellLength = 100;     !. cell length [um]
  real(rp), parameter, private :: p_cellRadius = 10.25;   !. cell radius [um]
  real(rp), parameter, private :: p_JuncLength = 160e-3;  !. Junc length [um]
  real(rp), parameter, private :: p_JuncRadius = 15e-3;   !. Junc radius [um]
  real(rp), parameter, private :: p_distSLcyto = 0.45;    !. dist. SL to cytosol [um]
  real(rp), parameter, private :: p_distJuncSL = 0.5;     !. dist. Junc to SL [um]
  real(rp), parameter, private :: p_DcaJuncSL = 1.64e-6;  !. Dca Junc to SL [cm^2/sec]
  real(rp), parameter, private :: p_DcaSLcyto = 1.22e-6;  !. Dca SL to cyto [cm^2/sec]
  real(rp), parameter, private :: p_DnaJuncSL = 1.09e-5;  !. Dna Junc to SL [cm^2/sec]
  real(rp), parameter, private :: p_DnaSLcyto = 1.79e-5;  !. Dna SL to cyto [cm^2/sec] 
  real(rp), parameter, private :: p_Vc = pi*p_cellRadius**2*p_cellLength*1e-15;  ! [L]
  real(rp), parameter, private :: p_VMyo = 0.65*p_Vc;
  real(rp), parameter, private :: p_VSR = 0.035*p_Vc;
  real(rp), parameter, private :: p_VSL = 0.02*p_Vc;
  real(rp), parameter, private :: p_VJunc = 0.0539*0.01*p_Vc; 
  real(rp), parameter, private :: p_SAJunc = 20150*pi*2*p_JuncLength*p_JuncRadius;  ! [um^2]
  real(rp), parameter, private :: p_SASL = pi*2*p_cellRadius*p_cellLength;          ! [um^2]
  !------------------------------------------------------------------------------
  real(rp), parameter, private :: p_JCaJuncSL =1/1.2134e12; ! [L/msec] = 8.2413e-13
  real(rp), parameter, private :: p_JCaSLMyo = 1/2.68510e11; ! [L/msec] = 3.2743e-12
  real(rp), parameter, private :: p_JNaJuncSL = 1/(1.6382e12/3*100); ! [L/msec] = 6.1043e-13
  real(rp), parameter, private :: p_JNaSLMyo = 1/(1.8308e10/3*100);  ! [L/msec] = 5.4621e-11
  !.--Fractional Currents--------------------------------------------------------
  real(rp), parameter, private :: p_FJunc = 0.11 !
  real(rp), parameter, private :: p_FSL = 1 - p_FJunc !
  real(rp), parameter, private :: p_FJuncCaL = 0.9 !
  real(rp), parameter, private :: p_FSLCaL = 1 - p_FJuncCaL !
  !.--Na Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_IbarNaK = 0.9*1.8;		!. [A/F] (mod for HF)
  real(rp), parameter, private :: p_KmNaip = 11;		!. [mM]
  real(rp), parameter, private :: p_KmKo = 1.5;			!. [mM]
  !.--Cl Currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_KdClCa = 100e-3;	!. [mM]
  !.--Ca Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_IbarNCX = 1.75*4.5;           ! [A/F] (mod for HF)
  real(rp), parameter, private :: p_KmCai = 3.59e-3;      ! [mM]
  real(rp), parameter, private :: p_KmCao = 1.3;          ! [mM]
  real(rp), parameter, private :: p_KmNai = 12.29;        ! [mM]
  real(rp), parameter, private :: p_KmNao = 87.5;         ! [mM]
  real(rp), parameter, private :: p_ksat = 0.32;           ! [-]
  real(rp), parameter, private :: p_nu = 0.27;              ! [-]
  real(rp), parameter, private :: p_Kdact = 0.150e-3;      ! [mM]
  real(rp), parameter, private :: p_IbarPMCA = 0.0673;        ! [A/F]
  real(rp), parameter, private :: p_KmPCa = 0.5e-3;        ![mM]
  !.--SR Ca Fluxes --------------------------------------------------------------
  real(rp), parameter, private :: p_VmaxSRCaP = 0.5*5.3114e-3;  ! [mM/ms] (mod for HF)
  real(rp), parameter, private :: p_Kmf = 0.246e-3;            ! [mM]
  real(rp), parameter, private :: p_Kmr = 1.7;                 ! [mM]
  real(rp), parameter, private :: p_hillSRCaP = 1.787;       ! [-]
  real(rp), parameter, private :: p_ks = 25;                    ! [ms^-1]
  real(rp), parameter, private :: p_koCa = 10;                 ! [ms^-1*mM^2]
  real(rp), parameter, private :: p_kom = 0.06;                ! [ms^-1]
  real(rp), parameter, private :: p_kiCa = 0.5;                ! [ms^-1*mM^2]
  real(rp), parameter, private :: p_kim = 0.005;               ! [ms^-1]
  real(rp), parameter, private :: p_ec50SR = 0.4;             ! [mM]   (mod for HF)
  real(rp), parameter, private :: p_GSRleak= 3*5.348e-6;         !     (mod for HF)
  !.--Buffering -----------------------------------------------------------------
  real(rp), parameter, private :: p_BmaxNaj = 7.561;                         ! [mM]
  real(rp), parameter, private :: p_BmaxNaSL = 1.65;                         ! [mM]
  real(rp), parameter, private :: p_koffNa = 1e-3;                            ! [ms^-1]
  real(rp), parameter, private :: p_konNa = 0.1e-3;                           ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxTnClow = 70e-3;                      ! [mM]
  real(rp), parameter, private :: p_koffTnCl = 19.6e-3;                      ! [ms^-1]
  real(rp), parameter, private :: p_konTnCl = 32.7;                          ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxTnChigh = 140e-3;                    ! [mM]
  real(rp), parameter, private :: p_koffTnChCa = 0.032e-3;                  ! [ms^-1]
  real(rp), parameter, private :: p_konTnChCa = 2.37;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_koffTnChMg = 3.33e-3;                   ! [ms^-1]
  real(rp), parameter, private :: p_konTnChMg = 3e-3;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxCaM = 24e-3;                          ! [mM]
  real(rp), parameter, private :: p_koffCaM = 238e-3;                         ! [ms^-1]
  real(rp), parameter, private :: p_konCaM = 34;                              ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxMyosin = 140e-3;                      ! [mM]
  real(rp), parameter, private :: p_koffMyoCa = 0.46e-3;                     ! [ms^-1]
  real(rp), parameter, private :: p_konMyoCa = 13.8;                         ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_koffMyoMg = 0.057e-3;                    ! [ms^-1]
  real(rp), parameter, private :: p_konMyoMg = 0.0157;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSR = 19*.9e-3;                        ! [mM]
  real(rp), parameter, private :: p_koffSR = 60e-3;                           ! [ms^-1]
  real(rp), parameter, private :: p_konSR = 100;                              ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSLlowSL = 37.4e-3*p_VMyo/p_VSL;       ! [mM]
  real(rp), parameter, private :: p_BmaxSLlowj = 4.6e-3*p_VMyo/p_VJunc*0.1;     ! [mM]
  real(rp), parameter, private :: p_koffSLl = 1300e-3;                       ! [ms^-1]
  real(rp), parameter, private :: p_konSLl = 100;                            ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSLhighSL = 13.4e-3*p_VMyo/p_VSL;      ! [mM]
  real(rp), parameter, private :: p_BmaxSLhighj = 1.65e-3*p_VMyo/p_VJunc*0.1;   ! [mM]
  real(rp), parameter, private :: p_koffSLh = 30e-3;                         ! [ms^-1]
  real(rp), parameter, private :: p_konSLh = 100;                            ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxCsqn = 140e-3*p_VMyo/p_VSR;             ! [mM]
  real(rp), parameter, private :: p_koffCsqn = 65;                            ! [ms^-1]
  real(rp), parameter, private :: p_konCsqn = 100;                            ! [ms^-1*mM^-1]
!
!------------------------------------------------------------------------------
!-- INITIAL VALUES ------------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter :: Cai = 0.000100; 
  real(rp), parameter :: CaSR = 0.489428;
  real(rp), parameter :: CaSL = 0.000133; 
  real(rp), parameter :: CaJ = 0.000365;
  real(rp), parameter :: Nai = 8.893689;
  real(rp), parameter :: NaSL = 8.893754;
  real(rp), parameter :: NaJ = 8.910610;
  real(rp), parameter :: Ki = 120;
  real(rp), parameter :: m = 0.005088;
  real(rp), parameter :: h = 0.574376;
  real(rp), parameter :: j = 0.56150; 
  real(rp), parameter :: d = 0.000004; 
  real(rp), parameter :: f = 0.994215;
  real(rp), parameter :: fCaBJ =0.049933;
  real(rp), parameter :: fCaBSL = 0.018962;
  real(rp), parameter :: ml = 0.003510;
  real(rp), parameter :: hl = 0.130704;
  real(rp), parameter :: xtos = 0.000491;
  real(rp), parameter :: ytos = 0.818112; 
  real(rp), parameter :: xtof = 0.000491; 
  real(rp), parameter :: ytof = 0.999994; 
  real(rp), parameter :: xkr = 0.039839; 
  real(rp), parameter :: xks = 0.004727;
  real(rp), parameter :: RyRr = 0.784464;
  real(rp), parameter :: RyRo = 2.403922e-06; 
  real(rp), parameter :: RyRi = 6.604817e-07; 
  real(rp), parameter :: NaBJ = 3.562098;
  real(rp), parameter :: NABSL = 0.776555; 
  real(rp), parameter :: TnCL = 0.010151; 
  real(rp), parameter :: TnCHC = 0.118362; 
  real(rp), parameter :: TnCHM = 0.010190; 
  real(rp), parameter :: CaM = 0.000339; 
  real(rp), parameter :: MyoC = 0.002066; 
  real(rp), parameter :: MyoM = 0.137420;
  real(rp), parameter :: SRB = 2.455203e-03; 
  real(rp), parameter :: SLLJ = 1.513645e-02; 
  real(rp), parameter :: SLLSL = 0.012285; 
  real(rp), parameter :: SLHJ = 0.109245; 
  real(rp), parameter :: SLHSL = 0.133953; 
  real(rp), parameter :: CSQNB = 1.116799;
  real(rp),parameter   :: Vi_GRHF_end = -81.386313_rp;
  real(rp),parameter   :: Vi_GRHF_epi = -81.386313_rp;
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: GRHF_ENDO_mod = 16;  ! Grandi Heart Failure ENDO (VH)
  integer(ip), parameter :: GRHF_EPI_mod  = 17;  ! Grandi Heart Failure EPI  (VH)
  integer(ip),parameter  :: nvar_grHF     = 40;  ! Number of State Variables
  integer(ip),parameter  :: ncur_grHF     = 25;  ! Number of currents
  integer(ip),parameter  :: np_grHF       = 14;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.  
  integer(ip), parameter    :: m_stpGRHF   =    5
  real(rp),    parameter    :: m_dvdtGRHF  =  1.0
