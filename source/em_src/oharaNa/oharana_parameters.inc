!------------------------------------------------------------------------------
! These are the parameters for the O'Hara-Rudy Model 
! Thomas O'Hara, L�szl� Vir�g, Adr�s Varr�, Yoram Rudy
! Simulation of the Undiseased Human Cardiac Ventricula Action Potential: Model
!   Formulation and Experimental Validation. PLoS Computational Biology (2011)
!   doi:10.1371/journal.pcbi.1002061
!

!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  
  ! Extracellular ionic concentrations
  real(rp), parameter, private :: nao=140.0;
  real(rp), parameter, private :: cao=1.8;
  real(rp), parameter, private :: ko=5.4;

  ! Physical constants
  real(rp), parameter, private :: R=8314.0;
  real(rp), parameter, private :: T=310.0;
  real(rp), parameter, private :: Frdy=96485.0;

  ! Cell geometry
  real(rp), parameter, private :: L=0.01;
  real(rp), parameter, private :: rad=0.0011;
  real(rp), parameter, private :: vcell=1000*3.14*rad*rad*L;
  real(rp), parameter, private :: Ageo=2*3.14*rad*rad+2*3.14*rad*L;
  real(rp), parameter, private :: Acap=2*Ageo;
  real(rp), parameter, private :: vmyo=0.68*vcell;
  real(rp), parameter, private :: vnsr=0.0552*vcell;
  real(rp), parameter, private :: vjsr=0.0048*vcell;
  real(rp), parameter, private :: vss=0.02*vcell;
  
  ! CaMK constants
  real(rp), parameter, private :: KmCaMK=0.15;
  real(rp), parameter, private :: aCaMK=0.05;
  real(rp), parameter, private :: bCaMK=0.00068;
  real(rp), parameter, private :: CaMKo=0.05;
  real(rp), parameter, private :: KmCaM=0.0015;
  
  ! Buffer paramaters
  real(rp), parameter, private :: BSRmax=0.047;
  real(rp), parameter, private :: KmBSR=0.00087;
  real(rp), parameter, private :: BSLmax=1.124;
  real(rp), parameter, private :: KmBSL=0.0087;
  real(rp), parameter, private :: cmdnmax=0.05;
  real(rp), parameter, private :: kmcmdn=0.00238;
  real(rp), parameter, private :: trpnmax=0.07;
  real(rp), parameter, private :: kmtrpn=0.0005;
  real(rp), parameter, private :: csqnmax=10.0;
  real(rp), parameter, private :: kmcsqn=0.8;

!------------------------------------------------------------------------------
!-- VARIABLE PARAMETERS -------------------------------------------------------
!------------------------------------------------------------------------------

  real(rp), parameter, private :: GNaL_end=0.0075;
  real(rp), parameter, private :: GNaL_mid=GNaL_end;
  real(rp), parameter, private :: GNaL_epi=0.6*GNaL_end;
  real(rp), parameter, private :: delta_factor_end=0;
  real(rp), parameter, private :: delta_factor_mid=0;
  real(rp), parameter, private :: delta_factor_epi=1;
  
  real(rp), parameter, private :: Gto_end=0.02;
  real(rp), parameter, private :: Gto_mid=Gto_end*4.0;
  real(rp), parameter, private :: Gto_epi=Gto_end*4.0;
  
  real(rp), parameter, private :: PCa_end=0.0001;
  real(rp), parameter, private :: PCa_epi=0.0001*1.2;
  real(rp), parameter, private :: PCa_mid=0.0001*2.5;

  real(rp), parameter, private :: GKr_end=0.046;
  real(rp), parameter, private :: GKr_mid=0.046*0.8;
  real(rp), parameter, private :: GKr_epi=0.046*1.3;

  real(rp), parameter, private :: GKs_end=0.0034;
  real(rp), parameter, private :: GKs_mid=0.0034;
  real(rp), parameter, private :: GKs_epi=0.0034*1.4;
  
  real(rp), parameter, private :: GK1_end=0.1908;
  real(rp), parameter, private :: GK1_mid=0.1908*1.3;
  real(rp), parameter, private :: GK1_epi=0.1908*1.2;

  real(rp), parameter, private :: Gncx_end=0.0008;
  real(rp), parameter, private :: Gncx_mid=0.0008*1.4;
  real(rp), parameter, private :: Gncx_epi=0.0008*1.1;
  
  real(rp), parameter, private :: Pnak_end=30;
  real(rp), parameter, private :: Pnak_mid=21;
  real(rp), parameter, private :: Pnak_epi=27;
  
  real(rp), parameter, private :: Jrel_0_end=1 
  real(rp), parameter, private :: Jrel_0_mid=1.7 
  real(rp), parameter, private :: Jrel_0_epi=1

  real(rp), parameter, private :: Jrelp_0_end=1 
  real(rp), parameter, private :: Jrelp_0_mid=1.7 
  real(rp), parameter, private :: Jrelp_0_epi=1  

  real(rp), parameter, private :: Jupnp_0_end=0.004375
  real(rp), parameter, private :: Jupnp_0_mid=0.004375
  real(rp), parameter, private :: Jupnp_0_epi=0.004375*1.3

  real(rp), parameter, private :: Jupp_0_end=2.75*0.004375
  real(rp), parameter, private :: Jupp_0_mid=2.75*0.004375
  real(rp), parameter, private :: Jupp_0_epi=2.75*0.004375*1.3

  real(rp), parameter, private :: Bcai_factor_end=1
  real(rp), parameter, private :: Bcai_factor_mid=1
  real(rp), parameter, private :: Bcai_factor_epi=1.3

!------------------------------------------------------------------------------
!-- INITIAL VALUES ------------------------------------------------------------
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS ------------------------------------------------------
! Value corresponding to 30 minutes pacing at BCL=1000ms
!------------------------------------------------------------------------------
real(rp), parameter :: Vi_OHRNA_end=-8.7176900000e+01, Vi_OHRNA_epi=-8.7050400000e+01,&
                       Vi_OHRNA_mid=-8.6638800000e+01, Vi_OHRNA_def=-8.7500000000e+01!
real(rp), parameter :: end_nai= 7.2565290000e+00, epi_nai= 7.8746330000e+00,&
                       mid_nai= 8.8258530000e+00, def_nai= 7.0000000000e+00!
real(rp), parameter :: end_nass= 7.2566100000e+00, epi_nass= 7.8747100000e+00,&
                       mid_nass= 8.8259800000e+00, def_nass= 7.0000000000e+00!
real(rp), parameter :: end_ki= 1.4451600000e+02, epi_ki= 1.4347200000e+02,&
                       mid_ki= 1.4150100000e+02, def_ki= 1.4500000000e+02!
real(rp), parameter :: end_kss= 1.4451600000e+02, epi_kss= 1.4347200000e+02,&
                       mid_kss= 1.4150100000e+02, def_kss= 1.4500000000e+02!
real(rp), parameter :: end_cai= 8.5473800000e-05, epi_cai= 7.5459200000e-05,&
                       mid_cai= 9.3678800000e-05, def_cai= 1.0000000000e-04!
real(rp), parameter :: end_cass= 8.4366700000e-05, epi_cass= 7.4479600000e-05,&
                       mid_cass= 9.1515900000e-05, def_cass= 1.0000000000e-04!
real(rp), parameter :: end_cansr= 1.6115300000e+00, epi_cansr= 1.9692100000e+00,&
                       mid_cansr= 1.9882200000e+00, def_cansr= 1.2000000000e+00!
real(rp), parameter :: end_cajsr= 1.5651100000e+00, epi_cajsr= 1.9124800000e+00,&
                       mid_cajsr= 1.9193900000e+00, def_cajsr= 1.2000000000e+00!
real(rp), parameter :: end_m= 7.3625900000e-03, epi_m= 7.4568400000e-03,&
                       mid_m= 7.7719300000e-03, def_m= 0.0000000000e+00!
real(rp), parameter :: end_h= 6.9724100000e-01, epi_h= 6.9283600000e-01,&
                       mid_h= 6.7825600000e-01, def_h= 1.0000000000e+00!
real(rp), parameter :: end_j= 6.9713000000e-01, epi_j= 6.9276700000e-01,&
                       mid_j= 6.7809500000e-01, def_j= 1.0000000000e+00!
real(rp), parameter :: end_mL= 1.8915700000e-04, epi_mL= 1.9375700000e-04,&
                       mid_mL= 2.0951400000e-04, def_mL= 0.0000000000e+00!
real(rp), parameter :: end_hL= 5.0058300000e-01, epi_hL= 4.9938000000e-01,&
                       mid_hL= 4.7750500000e-01, def_hL= 1.0000000000e+00!
real(rp), parameter :: end_hLp= 2.6936200000e-01, epi_hLp= 2.7446100000e-01,&
                       mid_hLp= 2.4348100000e-01, def_hLp= 1.0000000000e+00!
real(rp), parameter :: end_a= 1.0027800000e-03, epi_a= 1.0113700000e-03,&
                       mid_a= 1.0398300000e-03, def_a= 0.0000000000e+00!
real(rp), parameter :: end_iF= 9.9955200000e-01, epi_iF= 9.9954200000e-01,&
                       mid_iF= 9.9950800000e-01, def_iF= 1.0000000000e+00!
real(rp), parameter :: end_iS= 5.9105300000e-01, epi_iS= 9.9953500000e-01,&
                       mid_iS= 5.5673900000e-01, def_iS= 1.0000000000e+00!
real(rp), parameter :: end_ap= 5.1094600000e-04, epi_ap= 5.1532300000e-04,&
                       mid_ap= 5.2983200000e-04, def_ap= 0.0000000000e+00!
real(rp), parameter :: end_iFp= 9.9955200000e-01, epi_iFp= 9.9954200000e-01,&
                       mid_iFp= 9.9950800000e-01, def_iFp= 1.0000000000e+00!
real(rp), parameter :: end_iSp= 6.4377000000e-01, epi_iSp= 9.9954100000e-01,&
                       mid_iSp= 6.0309000000e-01, def_iSp= 1.0000000000e+00!
real(rp), parameter :: end_d= 2.3546600000e-09, epi_d= 2.4261200000e-09,&
                       mid_d= 2.6741300000e-09, def_d= 0.0000000000e+00!
real(rp), parameter :: end_ff= 1.0000000000e+00, epi_ff= 1.0000000000e+00,&
                       mid_ff= 1.0000000000e+00, def_ff= 1.0000000000e+00!
real(rp), parameter :: end_fs= 9.1094700000e-01, epi_fs= 9.2635500000e-01,&
                       mid_fs= 8.7697800000e-01, def_fs= 1.0000000000e+00!
real(rp), parameter :: end_fcaf= 1.0000000000e+00, epi_fcaf= 1.0000000000e+00,&
                       mid_fcaf= 1.0000000000e+00, def_fcaf= 1.0000000000e+00!
real(rp), parameter :: end_fcas= 9.9982200000e-01, epi_fcas= 9.9990100000e-01,&
                       mid_fcas= 9.9962400000e-01, def_fcas= 1.0000000000e+00!
real(rp), parameter :: end_jca= 9.9997700000e-01, epi_jca= 9.9998500000e-01,&
                       mid_jca= 9.9995300000e-01, def_jca= 1.0000000000e+00!
real(rp), parameter :: end_nca= 2.6802200000e-03, epi_nca= 1.6605500000e-03,&
                       mid_nca= 3.6574500000e-03, def_nca= 0.0000000000e+00!
real(rp), parameter :: end_ffp= 1.0000000000e+00, epi_ffp= 1.0000000000e+00,&
                       mid_ffp= 1.0000000000e+00, def_ffp= 1.0000000000e+00!
real(rp), parameter :: end_fcafp= 1.0000000000e+00, epi_fcafp= 1.0000000000e+00,&
                       mid_fcafp= 1.0000000000e+00, def_fcafp= 1.0000000000e+00!
real(rp), parameter :: end_xrf= 8.1002100000e-06, epi_xrf= 8.2192000000e-06,&
                       mid_xrf= 8.9933600000e-06, def_xrf= 0.0000000000e+00!
real(rp), parameter :: end_xrs= 4.5080100000e-01, epi_xrs= 4.1736100000e-01,&
                       mid_xrs= 4.8852300000e-01, def_xrs= 0.0000000000e+00!
real(rp), parameter :: end_xs1= 2.6994600000e-01, epi_xs1= 2.3541700000e-01,&
                       mid_xs1= 3.2705300000e-01, def_xs1= 0.0000000000e+00!
real(rp), parameter :: end_xs2= 1.9338800000e-04, epi_xs2= 1.9612200000e-04,&
                       mid_xs2= 2.0542200000e-04, def_xs2= 0.0000000000e+00!
real(rp), parameter :: end_xk1= 9.9676600000e-01, epi_xk1= 9.9679900000e-01,&
                       mid_xk1= 9.9690500000e-01, def_xk1= 1.0000000000e+00!
real(rp), parameter :: end_Jrelnp= 2.4790900000e-07, epi_Jrelnp= 4.6254500000e-07,&
                       mid_Jrelnp= 1.7744700000e-06, def_Jrelnp= 0.0000000000e+00!
real(rp), parameter :: end_Jrelp= 3.0972600000e-07, epi_Jrelp= 5.7806800000e-07,&
                       mid_Jrelp= 2.2175800000e-06, def_Jrelp= 0.0000000000e+00!
real(rp), parameter :: end_CaMKt= 1.2425000000e-02, epi_CaMKt= 1.5664500000e-02,&
                       mid_CaMKt= 2.3650200000e-02, def_CaMKt= 0.0000000000e+00!
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS OLD   --------------------------------------------------
!------------------------------------------------------------------------------
!real(rp), parameter :: Vi_OHRNA_end=-87.5,       Vi_OHRNA_epi=-87.5,              &
!                       Vi_OHRNA_mid=-87.5,       Vi_OHRNA_def=-87.5              !
!real(rp), parameter :: end_nai=7,              epi_nai=7,                     &
!                       mid_nai=7,              def_nai=7                     !
!real(rp), parameter :: end_nass=end_nai,       epi_nass=epi_nai,              &
!                       mid_nass=mid_nai,       def_nass=def_nai              !
!real(rp), parameter :: end_ki=145,             epi_ki=145,                    &
!                       mid_ki=145,             def_ki=145                    !
!real(rp), parameter :: end_kss=end_ki,         epi_kss=epi_ki,                &
!                       mid_kss=mid_ki,         def_kss=def_ki                !
!real(rp), parameter :: end_cai=1.0e-4,         epi_cai=1.0e-4,                &
!                       mid_cai=1.0e-4,         def_cai=1.0e-4                !
!real(rp), parameter :: end_cass=end_cai,       epi_cass=epi_cai,              &
!                       mid_cass=mid_cai,       def_cass=def_cai              !
!real(rp), parameter :: end_cansr=1.2,          epi_cansr=1.2,                 &
!                       mid_cansr=1.2,          def_cansr=1.2                 !
!real(rp), parameter :: end_cajsr=end_cansr,    epi_cajsr=epi_cansr,           &
!                       mid_cajsr=mid_cansr,    def_cajsr=def_cansr           !
!real(rp), parameter :: end_m=0,                epi_m=0,                       &
!                       mid_m=0,                def_m=0                       !
!real(rp), parameter :: end_h=1,                epi_h=1,                      &
!                       mid_h=1,                def_h=1                      !
!real(rp), parameter :: end_j=1,                epi_j=1,                       &
!                       mid_j=1,                def_j=1                       !
!real(rp), parameter :: end_mL=0,               epi_mL=0,                      &
!                       mid_mL=0,               def_mL=0                      !
!real(rp), parameter :: end_hL=1,               epi_hL=1,                      &
!                       mid_hL=1,               def_hL=1                      !
!real(rp), parameter :: end_hLp=1,              epi_hLp=1,                     &
!                       mid_hLp=1,              def_hLp=1                     !
!real(rp), parameter :: end_a=0,                epi_a=0,                       &
!                       mid_a=0,                def_a=0                       !
!real(rp), parameter :: end_iF=1,               epi_iF=1,                      &
!                       mid_iF=1,               def_iF=1                      !
!real(rp), parameter :: end_iS=1,               epi_iS=1,                      &
!                       mid_iS=1,               def_iS=1                      !
!real(rp), parameter :: end_ap=0,               epi_ap=0,                      &
!                       mid_ap=0,               def_ap=0                      !
!real(rp), parameter :: end_iFp=1,              epi_iFp=1,                     &
!                       mid_iFp=1,              def_iFp=1                     !
!real(rp), parameter :: end_iSp=1,              epi_iSp=1,                     &
!                       mid_iSp=1,              def_iSp=1                     !
!real(rp), parameter :: end_d=0,                epi_d=0,                       &
!                       mid_d=0,                def_d=0                       !
!real(rp), parameter :: end_ff=1,               epi_ff=1,                      &
!                       mid_ff=1,               def_ff=1                      !
!real(rp), parameter :: end_fs=1,               epi_fs=1,                      &
!                       mid_fs=1,               def_fs=1                      !
!real(rp), parameter :: end_fcaf=1,             epi_fcaf=1,                    &
!                       mid_fcaf=1,             def_fcaf=1                    !
!real(rp), parameter :: end_fcas=1,             epi_fcas=1,                    &
!                       mid_fcas=1,             def_fcas=1                    !
!real(rp), parameter :: end_jca=1,              epi_jca=1,                     &
!                       mid_jca=1,              def_jca=1                     !
!real(rp), parameter :: end_nca=0,              epi_nca=0,                     &
!                       mid_nca=0,              def_nca=0                     !
!real(rp), parameter :: end_ffp=1,              epi_ffp=1,                     &
!                       mid_ffp=1,              def_ffp=1                     !
!real(rp), parameter :: end_fcafp=1,            epi_fcafp=1,                   &
!                       mid_fcafp=1,            def_fcafp=1                   !
!real(rp), parameter :: end_xrf=0,              epi_xrf=0,                     &
!                       mid_xrf=0,              def_xrf=0                     !
!real(rp), parameter :: end_xrs=0,              epi_xrs=0,                     &
!                       mid_xrs=0,              def_xrs=0                     !
!real(rp), parameter :: end_xs1=0,              epi_xs1=0,                     &
!                       mid_xs1=0,              def_xs1=0                     !
!real(rp), parameter :: end_xs2=0,              epi_xs2=0,                     &
!                       mid_xs2=0,              def_xs2=0                     !
!real(rp), parameter :: end_xk1=1,              epi_xk1=1,                     &
!                       mid_xk1=1,              def_xk1=1                     !
!real(rp), parameter :: end_Jrelnp=0,           epi_Jrelnp=0,                  &
!                       mid_Jrelnp=0,           def_Jrelnp=0                  !
!real(rp), parameter :: end_Jrelp=0,            epi_Jrelp=0,                   &
!                       mid_Jrelp=0,            def_Jrelp=0                   !
!real(rp), parameter :: end_CaMKt=0,            epi_CaMKt=0,                   &
!                       mid_CaMKt=0,            def_CaMKt=0                   !
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip),parameter :: OH_NA_ENDO_mod = 201
  integer(ip),parameter :: OH_NA_MID_mod  = 211
  integer(ip),parameter :: OH_NA_EPI_mod  = 221
  integer(ip),parameter :: nvar_ohrna    = 37
  integer(ip),parameter :: ncur_ohrna    = 18
  integer(ip),parameter :: np_ohrna      = 27 
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------ 
  integer(ip), parameter    :: m_stpOHRNA  = 1  
  real(rp),    parameter    :: m_dvdtOHRNA = 0
!------------------------------------------------------------------------------
