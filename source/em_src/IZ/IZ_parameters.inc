!------------------------------------------------------------------------------
! This are the parameters for the Roger and Mcculloch Model
!------------------------------------------------------------------------------
  real(rp), parameter, private   :: v1   = 0.13
  real(rp), parameter, private   :: v2   = 1.0
  real(rp), parameter, private   :: G    = 57.64 

!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS --------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter   :: Vi_IZ    = 0.0_rp

!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------

  integer(ip), parameter :: IZ_mod  = -1;  ! Ischemic zone model
  integer(ip),parameter :: nvar_iz  = 1    ! Number of state variables
  integer(ip),parameter :: ncur_iz  = 0    ! Number of currents
  integer(ip),parameter :: np_iz    = 3    ! Number of modifiable parameters
