! This module implements the Carro2 Model of 2011
! An Improved Human Ventricular Cell Model for Investigation 
! of Cardiac Arrhythmias under Hyperkalemic Conditions. 
!      J Carro2, JF Rodriguez, P Laguna & E Pueyo. 
!      Philosophical Transactions of the Royal Society A (2011)
!------------------------------------------------------------------------------
module mod_carro2
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'carro2_parameters.inc'
  !.
  type t_crr2m
    sequence
    real(rp) :: Cai, CaSR, CaSL, CaJ, Nai, NaSL, NaJ, m, h, j, d, f, f2,     &
                fCaBJ, fCaBSL, xtos, ytos, xtof, ytof, xkr, xks, RyRr, RyRo, &
                RyRi, NaBJ, NABSL, TnCL, TnCHC, TnCHM, CaM, MyoC, MyoM, SRB, &
                SLLJ, SLLSL, SLHJ, SLHSL, CSQNB, Ki
  end type t_crr2m
  !.
  type, private :: t_cur
    sequence
    real(rp) :: INaJunc, INaSL, INa, INaBkJunc, INaBkSL, INaBk, INaKJunc,    &
                INaKSL, INaK, Ikr, IksJunc, IksSL, Iks, Ikp, Itos, Itof,     &
                Ito, IK1, IClCaJunc, IClCaSL, IclCa, IClBk, ICaJunc, ICaSL,  &
                ICa, ICaK, ICaNaJunc, ICaNaSL, ICaNa, ICaL, IncxJunc,        &
                IncxSL, Incx, IpCaJunc, IpCaSL, IpCa, ICaBkJunc, ICaBkSL,    &
                ICaBk, INatotJunc, INatotSL, IKtot, ICatotJunc, ICatotSL,    &
                INatot, ICltot, ICatot, Itot
  end type t_cur
  
  public  :: ic_Carro2, Carro2_A_P01
  private :: concentrations, gates, currents

  type, public:: t_prm
    private
    real(rp) :: A_GCaL;     !. 1
    real(rp) :: A_GKr;      !. 2
    real(rp) :: A_GKs;      !. 3
    real(rp) :: A_GK1;      !. 4
    real(rp) :: A_GNaK;     !. 5
    real(rp) :: A_Gncx;     !. 6
    real(rp) :: A_Gto;      !. 7
    real(rp) :: A_GpCa;     !. 8
    real(rp) :: A_GKp;      !. 9
    real(rp) :: A_GNa;      !.10
    real(rp) :: A_GClCa;    !.11
    real(rp) :: A_GNabk;    !.12
    real(rp) :: A_GClbk;    !.13
    real(rp) :: A_GCabk;    !.14
    real(rp) :: A_JCajuncsl !.15
    real(rp) :: A_JCaslmyo; !.16
    real(rp) :: A_SRleak;   !.17
    real(rp) :: A_ks;       !.18
    real(rp) :: A_VSRCaP;   !.19
    real(rp) :: A_Cao;      !.20
    real(rp) :: p_Gtos;     !.21
    real(rp) :: p_Gtof;     !.22
  end type t_prm
  
  
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_CRR2(lu_st)
!
integer(ip), intent(in)   :: lu_st

write(lu_st,'(A)',advance='no') '% '
write(lu_st,'(A)',advance='no') 'Cai '
write(lu_st,'(A)',advance='no') 'CaSR '
write(lu_st,'(A)',advance='no') 'CaSL '
write(lu_st,'(A)',advance='no') 'CaJ '
write(lu_st,'(A)',advance='no') 'Nai '
write(lu_st,'(A)',advance='no') 'NaSL '
write(lu_st,'(A)',advance='no') 'NaJ '
write(lu_st,'(A)',advance='no') 'm '
write(lu_st,'(A)',advance='no') 'h '
write(lu_st,'(A)',advance='no') 'j '
write(lu_st,'(A)',advance='no') 'd '
write(lu_st,'(A)',advance='no') 'f '
write(lu_st,'(A)',advance='no') 'f2 '
write(lu_st,'(A)',advance='no') 'fCaBJ '
write(lu_st,'(A)',advance='no') 'fCaBSL '
write(lu_st,'(A)',advance='no') 'xtos '
write(lu_st,'(A)',advance='no') 'ytos '
write(lu_st,'(A)',advance='no') 'xtof '
write(lu_st,'(A)',advance='no') 'ytof '
write(lu_st,'(A)',advance='no') 'xkr '
write(lu_st,'(A)',advance='no') 'xks '
write(lu_st,'(A)',advance='no') 'RyRr '
write(lu_st,'(A)',advance='no') 'RyRo '
write(lu_st,'(A)',advance='no') 'RyRi '
write(lu_st,'(A)',advance='no') 'NaBJ '
write(lu_st,'(A)',advance='no') 'NaBSL '
write(lu_st,'(A)',advance='no') 'TnCL '
write(lu_st,'(A)',advance='no') 'TnCHC '
write(lu_st,'(A)',advance='no') 'TnCHM '
write(lu_st,'(A)',advance='no') 'CaM '
write(lu_st,'(A)',advance='no') 'MyoC '
write(lu_st,'(A)',advance='no') 'MyoM '
write(lu_st,'(A)',advance='no') 'SRB '
write(lu_st,'(A)',advance='no') 'SLLJ '
write(lu_st,'(A)',advance='no') 'SLLSL '
write(lu_st,'(A)',advance='no') 'SLHJ '
write(lu_st,'(A)',advance='no') 'SLHSL '
write(lu_st,'(A)',advance='no') 'CSQNB '
write(lu_st,'(A)',advance='no') 'Ki '
end subroutine write_state_CRR2
!------------------------------------------------------------------------------!
subroutine write_current_CRR2(lu_cr)
!
integer(ip), intent(in)   :: lu_cr

write(lu_cr,'(A)',advance='no') '% '
write(lu_cr,'(A)',advance='no') 'INa '
write(lu_cr,'(A)',advance='no') 'INaBk '
write(lu_cr,'(A)',advance='no') 'INaK '
write(lu_cr,'(A)',advance='no') 'IKr '
write(lu_cr,'(A)',advance='no') 'IKs '
write(lu_cr,'(A)',advance='no') 'IKp '
write(lu_cr,'(A)',advance='no') 'Itos '
write(lu_cr,'(A)',advance='no') 'Itof '
write(lu_cr,'(A)',advance='no') 'Ito '
write(lu_cr,'(A)',advance='no') 'IK1 '
write(lu_cr,'(A)',advance='no') 'IclCa '
write(lu_cr,'(A)',advance='no') 'IclBk '
write(lu_cr,'(A)',advance='no') 'ICa '
write(lu_cr,'(A)',advance='no') 'ICaK '
write(lu_cr,'(A)',advance='no') 'ICaNa '
write(lu_cr,'(A)',advance='no') 'ICaL '
write(lu_cr,'(A)',advance='no') 'INaCa '
write(lu_cr,'(A)',advance='no') 'IpCa '
write(lu_cr,'(A)',advance='no') 'ICaBk '
write(lu_cr,'(A)',advance='no') 'IKtot '
write(lu_cr,'(A)',advance='no') 'INatot '
write(lu_cr,'(A)',advance='no') 'ICltot '
write(lu_cr,'(A)',advance='no') 'ICatot '
write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_CRR2
!------------------------------------------------------------------------------!
function get_parameter_CRR2 (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_crr2)

  select case (tcell) !14-> ENDO, 15->EPI
  case (CRR2_ENDO_mod)
  v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,         &
             p_Gtos_endo, p_Gtof_endo /)
    
  case (CRR2_EPI_mod)
  v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
             1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp,         &
             p_Gtos_epi, p_Gtof_epi /)

  case default
    write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_CRR2
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param
  
  param%A_GCaL      = v_prm( 1)    !. 1
  param%A_GKr       = v_prm( 2)    !. 2
  param%A_GKs       = v_prm( 3)    !. 3
  param%A_GK1       = v_prm( 4)    !. 4
  param%A_GNaK      = v_prm( 5)    !. 5
  param%A_Gncx      = v_prm( 6)    !. 6
  param%A_Gto       = v_prm( 7)    !. 7
  param%A_GpCa      = v_prm( 8)    !. 8
  param%A_GKp       = v_prm( 9)    !. 9
  param%A_GNa       = v_prm(10)    !.10
  param%A_GClCa     = v_prm(11)    !.11
  param%A_GNabk     = v_prm(12)    !.12
  param%A_GClbk     = v_prm(13)    !.13
  param%A_GCabk     = v_prm(14)    !.14
  param%A_JCajuncsl = v_prm(15)    !.15
  param%A_JCaslmyo  = v_prm(16)    !.16
  param%A_SRleak    = v_prm(17)    !.17
  param%A_ks        = v_prm(18)    !.18
  param%A_VSRCaP    = v_prm(19)    !.19
  param%A_Cao       = v_prm(20)    !.20
  param%p_Gtos      = v_prm(21)    !.21
  param%p_Gtof      = v_prm(22)    !.22
  
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_Carro2(ict) result(crr2_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Ten Tusscher Model
! tt_m is the main structure which contains the membrane potential, ioninc 
! concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none
  integer (ip), intent(in)  :: ict
  real(rp)                  :: crr2_ic(nvar_crr2)
  
  select case (ict)
  case(CRR2_ENDO_mod) !.----ENDO
    crr2_ic = (/end_Cai, end_CaSR, end_CaSL, end_CaJ, end_Nai, end_NaSL, &
      end_NaJ, end_m, end_h, end_j, end_d, end_f, end_f2, end_fCaBJ,     &
      end_fCaBSL, end_xtos, end_ytos, end_xtof, end_ytof, end_xkr,       &
      end_xks, end_RyRr, end_RyRo, end_RyRi, end_NaBJ, end_NABSL,        &
      end_TnCL, end_TnCHC, end_TnCHM, end_CaM, end_MyoC, end_MyoM,       &
      end_SRB, end_SLLJ, end_SLLSL, end_SLHJ, end_SLHSL, end_CSQNB,      &
      end_Ki /)
  case(CRR2_EPI_mod) !.----EPI
    crr2_ic = (/epi_Cai, epi_CaSR, epi_CaSL, epi_CaJ, epi_Nai, epi_NaSL, &
      epi_NaJ, epi_m, epi_h, epi_j, epi_d, epi_f, epi_f2, epi_fCaBJ,     &
      epi_fCaBSL, epi_xtos, epi_ytos, epi_xtof, epi_ytof, epi_xkr,       &
      epi_xks, epi_RyRr, epi_RyRo, epi_RyRi, epi_NaBJ, epi_NABSL,        &
      epi_TnCL, epi_TnCHC, epi_TnCHM, epi_CaM, epi_MyoC, epi_MyoM,       &
      epi_SRB, epi_SLLJ, epi_SLLSL, epi_SLHJ, epi_SLHSL, epi_CSQNB,      &
      epi_Ki /)
  case default
    crr2_ic = (/def_Cai, def_CaSR, def_CaSL, def_CaJ, def_Nai, def_NaSL, &
      def_NaJ, def_m, def_h, def_j, def_d, def_f, def_f2, def_fCaBJ,     &
      def_fCaBSL, def_xtos, def_ytos, def_xtof, def_ytof, def_xkr,       &
      def_xks, def_RyRr, def_RyRo, def_RyRi, def_NaBJ, def_NABSL,        &
      def_TnCL, def_TnCHC, def_TnCHM, def_CaM, def_MyoC, def_MyoM,       &
      def_SRB, def_SLLJ, def_SLLSL, def_SLHJ, def_SLHSL, def_CSQNB,      &
      def_Ki /)
  end select
  !.

  return
end function ic_Carro2
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_crr2m), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_crr2)
  v_me( 1) = str_me%Cai
  v_me( 2) = str_me%CaSR
  v_me( 3) = str_me%CaSL
  v_me( 4) = str_me%CaJ
  v_me( 5) = str_me%Nai
  v_me( 6) = str_me%NaSL
  v_me( 7) = str_me%NaJ
  v_me( 8) = str_me%m
  v_me( 9) = str_me%h
  v_me(10) = str_me%j
  v_me(11) = str_me%d
  v_me(12) = str_me%f
  v_me(13) = str_me%f2
  v_me(14) = str_me%fCaBJ
  v_me(15) = str_me%fCaBSL
  v_me(16) = str_me%xtos
  v_me(17) = str_me%ytos
  v_me(18) = str_me%xtof
  v_me(19) = str_me%ytof
  v_me(20) = str_me%xkr
  v_me(21) = str_me%xks
  v_me(22) = str_me%RyRr
  v_me(23) = str_me%RyRo
  v_me(24) = str_me%RyRi
  v_me(25) = str_me%NaBJ
  v_me(26) = str_me%NABSL
  v_me(27) = str_me%TnCL
  v_me(28) = str_me%TnCHC
  v_me(29) = str_me%TnCHM
  v_me(30) = str_me%CaM
  v_me(31) = str_me%MyoC
  v_me(32) = str_me%MyoM
  v_me(33) = str_me%SRB
  v_me(34) = str_me%SLLJ
  v_me(35) = str_me%SLLSL
  v_me(36) = str_me%SLHJ
  v_me(37) = str_me%SLHSL
  v_me(38) = str_me%CSQNB
  v_me(39) = str_me%Ki
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_crr2m), intent(out) :: str_me
  
  str_me%Cai    =  v_me( 1);
  str_me%CaSR   =  v_me( 2);
  str_me%CaSL   =  v_me( 3);
  str_me%CaJ    =  v_me( 4);
  str_me%Nai    =  v_me( 5);
  str_me%NaSL   =  v_me( 6);
  str_me%NaJ    =  v_me( 7);
  str_me%m      =  v_me( 8);
  str_me%h      =  v_me( 9);
  str_me%j      =  v_me(10);
  str_me%d      =  v_me(11);
  str_me%f      =  v_me(12);
  str_me%f2     =  v_me(13);
  str_me%fCaBJ  =  v_me(14);
  str_me%fCaBSL =  v_me(15);
  str_me%xtos   =  v_me(16);
  str_me%ytos   =  v_me(17);
  str_me%xtof   =  v_me(18);
  str_me%ytof   =  v_me(19);
  str_me%xkr    =  v_me(20);
  str_me%xks    =  v_me(21);
  str_me%RyRr   =  v_me(22);
  str_me%RyRo   =  v_me(23);
  str_me%RyRi   =  v_me(24);
  str_me%NaBJ   =  v_me(25);
  str_me%NABSL  =  v_me(26);
  str_me%TnCL   =  v_me(27);
  str_me%TnCHC  =  v_me(28);
  str_me%TnCHM  =  v_me(29);
  str_me%CaM    =  v_me(30);
  str_me%MyoC   =  v_me(31);
  str_me%MyoM   =  v_me(32);
  str_me%SRB    =  v_me(33);
  str_me%SLLJ   =  v_me(34);
  str_me%SLLSL  =  v_me(35);
  str_me%SLHJ   =  v_me(36);
  str_me%SLHSL  =  v_me(37);
  str_me%CSQNB  =  v_me(38); 
  str_me%Ki     =  v_me(39); 
  return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine concentrations (dt, cur, prm, crr2m)
!-------------------------------------------------------------------------------
! Function to update Ion concentrations
!-------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)      :: dt
  type (t_cur), intent(in)      :: cur
  type (t_prm), intent(in)      :: prm
  type (t_crr2m),   intent(inout) :: crr2m
  
  real(rp):: dNaBJ, dNaBSL, dTnCl, dTnChc, dTnChm, dCaM, &
             dMyoc, dMyom, dSRB, JCaBcytosol, dSLLJ,     &
             dSLLSL, dSLHJ, dSLHSL, JCaBJunction, JCaBSL,&
             dCSQNB, dNaJ, dNaSL, dNai,JSRCarel, JserCa, &
             JSRleak, dCaJ, dCaSL, dCai, dCaSR, dKi

  ! Sodium Buffers
  dNaBJ = p_KonNa * crr2m%NaJ * (p_BmaxNaJ - crr2m%NaBJ) - p_KoffNa * crr2m%NaBJ;
  dNaBSL = p_KonNa * crr2m%NaSL * (p_BmaxNaSL - crr2m%NaBSL) - p_KoffNa * crr2m%NaBSL;
  
  ! Cytosolic Ca Buffers
  dTnCL = p_KonTnCl * crr2m%Cai * (p_BmaxTnClow - crr2m%TnCl) - & 
        p_KoffTnCl * crr2m%TnCl;
  dTnChc = p_KonTnChCa * crr2m%Cai * (p_BmaxTnChigh - crr2m%TnChc - &
        crr2m%TnChm) - p_KoffTnChCa * crr2m%TnChc;
  dTnChm = p_KonTnChMg * p_Mgi * (p_BmaxTnChigh - crr2m%TnChc - &
        crr2m%TnChm) - p_KoffTnChMg * crr2m%TnChm;
  dCaM = p_KonCaM * crr2m%Cai * (p_BmaxCaM - crr2m%CaM) - p_KoffCaM * crr2m%CaM;
  dMyoc = p_KonMyoCa * crr2m%Cai * (p_BmaxMyosin - crr2m%Myoc - crr2m%Myom) - &
        p_KoffMyoCa * crr2m%Myoc;
  dMyom = p_KonMyoMg * p_Mgi * (p_BmaxMyosin - crr2m%Myoc - crr2m%Myom) - &
        p_KoffMyoMg * crr2m%Myom;
  dSRB = p_KonSR * crr2m%Cai * (p_BmaxSR - crr2m%SRB) - p_KoffSR * crr2m%SRB;
  JCaBcytosol = dTnCl + dTnChc + dTnChm + dCaM + dMyoc + dMyom + dSRB;

  ! Junctional and SL Ca Buffers
  dSLLJ = p_KonSLl * crr2m%CaJ * (p_BmaxSLlowJ - crr2m%SLLJ) - &
        p_KoffSLl * crr2m%SLLJ;
  dSLLSL = p_KonSLl * crr2m%CaSL * (p_BmaxSLlowSL - crr2m%SLLSL) - &
        p_KoffSLl * crr2m%SLLSL;
  dSLHJ = p_KonSLh * crr2m%CaJ * (p_BmaxSLhighJ - crr2m%SLHJ) - &
        p_KoffSLh * crr2m%SLHJ;
  dSLHSL = p_KonSLh * crr2m%CaSL * (p_BmaxSLhighSL - crr2m%SLHSL) - &
        p_KoffSLh * crr2m%SLHSL;
  JCaBJunction = dSLLJ + dSLHJ;
  JCaBSL = dSLLSL + dSLHSL;
  
  ! SR Ca Buffer
  dCSQNB = p_KonCsqn * crr2m%CaSR * (p_BmaxCsqn - crr2m%CSQNB) - &
        p_KoffCsqn * crr2m%CSQNB;
    
  ! Sodium Concentrations 
  dNaJ = -cur%INatotJunc * p_Cap / (p_VJunc * p_F) + &
        p_JNaJuncSL / p_VJunc * (crr2m%NaSL - crr2m%NaJ) - dNaBJ;
  dNaSL = -cur%INatotSL * p_Cap / (p_VSL * p_F) + &
        p_JNaJuncSL / p_VSL * (crr2m%NaJ - crr2m%NASL) + &
        p_JNaSLMyo / p_VSL * (crr2m%Nai - crr2m%NaSL) - dNaBSL;
  dNai = p_JNaSLMyo / p_VMyo * (crr2m%NaSL - crr2m%Nai); 
  
  ! Calcium concentrations
  JSRCarel = prm%A_ks * p_ks * crr2m%RyRo * (crr2m%CaSR - crr2m%CaJ);
  JserCa = prm%A_VSRCaP * p_VmaxSRCaP * ((crr2m%Cai/p_Kmf) ** p_hillSRCaP - &
        (crr2m%CaSR/p_Kmr)**p_hillSRCaP) / (1 + (crr2m%Cai/p_Kmf)**p_hillSRCaP + &
        (crr2m%CaSR/p_Kmr) ** p_hillSRCaP);
  JSRleak = prm%A_SRleak * 5.348e-6 * (crr2m%CaSR - crr2m%CaJ);
  
  dCaJ = -cur%ICatotJunc * p_Cap / (p_VJunc * 2 * p_F) + &
        prm%A_JCajuncsl * p_JCaJuncSL / p_VJunc * (crr2m%CaSL - crr2m%CaJ) - JCaBJunction + &
        (JSRCarel) * p_VSR / p_VJunc + JSRleak * p_VMyo / p_VJunc;
  dCaSL = -cur%ICatotSL * p_Cap / (p_VSL * 2 * p_F) + prm%A_Jcajuncsl * p_JCaJuncSL / &
        p_VSL * (crr2m%CaJ - crr2m%CaSL) + p_JCaSLMyo / p_VSL * &
        (crr2m%Cai - crr2m%CaSL) - JCaBSL;
  dCai = -JserCa * p_VSR / p_VMyo - JCaBCytosol + p_JCaSLMyo / p_VMyo * &
        (crr2m%CaSL - crr2m%Cai);  
  
  dCaSR = JserCa - (JSRleak * p_VMyo / p_VSR + JSRCarel) - dCSQNB;
      

  dKi = -cur%IKtot * p_Cap / (p_VMyo * p_F);

  crr2m%NaBJ = crr2m%NaBJ + dNaBJ * dt;
  crr2m%NaBSL = crr2m%NaBSL + dNaBSL * dt;
   
  crr2m%TnCl = crr2m%TnCl + dTnCl * dt;
  crr2m%TnChc = crr2m%TnChc + dTnChc * dt;
  crr2m%TnChm = crr2m%TnChm + dTnChm * dt;
  crr2m%CaM = crr2m%CaM + dCaM * dt;
  crr2m%Myoc = crr2m%Myoc + dMyoc * dt;
  crr2m%Myom = crr2m%Myom + dMyom * dt;
  crr2m%SRB = crr2m%SRB + dSRB * dt;
  
  crr2m%SLLJ = crr2m%SLLJ + dSLLJ * dt;
  crr2m%SLLSL = crr2m%SLLSL + dSLLSL * dt;
  crr2m%SLHJ = crr2m%SLHJ + dSLHJ * dt;
  crr2m%SLHSL = crr2m%SLHSL + dSLHSL * dt;
  
  crr2m%CSQNB = crr2m%CSQNB + dCSQNB * dt;
  
  crr2m%NaJ = crr2m%NaJ + dNaJ * dt;
  crr2m%NaSL = crr2m%NaSL + dNaSL * dt;
  crr2m%Nai = crr2m%Nai + dNai * dt;
  
  crr2m%CaJ = crr2m%CaJ + dCaJ * dt;
  crr2m%CaSL = crr2m%CaSL + dCaSL * dt;
  crr2m%Cai = crr2m%Cai + dCai * dt;
  crr2m%CaSR = crr2m%CaSR + dCaSR * dt;

  crr2m%Ki = crr2m%Ki + dKi * dt;
  
  return
end subroutine concentrations
!------------------------------------------------------------------------------
subroutine gates(dt, U, prm, crr2m)
!------------------------------------------------------------------------------
! This function updates the gating variables
!
!------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)    :: dt, U
  type(t_crr2m), intent(inout) :: crr2m
  type(t_prm),  intent(in)    :: prm
  real (rp):: mss, taum, ah, bh, tauh, hss, aj, bj, tauj,     &
              jss, xrss, tauxr, xsss, tauxs, xtoss, ytoss,    &
              tauxtos, tauytos, tauxtof, tauytof, taud, fss,  &
              tauf, dfCaBJ, dfCaBSL, MaxSR, MinSR, kCaSR, dss,&
              koSRCa, kiSRCa, RI, dRyRr, dRyRo, dRyRi, f2ss,  &
              tauf2, alpha_f, beta_f, gamma_f, alpha_f2,      &
              gamma_f2, alpha_d, beta_d, gamma_d
   
  ! I_Na: Fast Na Current
  mss = 1 / ((1 + exp( -(56.86 + U) / 9.03))**2);
  taum = 0.1292 * exp(-((U + 45.79) / 15.54)**2) + &
        0.06487 * exp(-((U - 4.823) / 51.12)**2);
  if(U >= -40) then
    ah = 0; 
    bh = 5.9231 / (1 + exp(-(U + 10.66) / 11.1 ));
    
    aj = 0;
    bj = (0.6 * exp(0.057 * U)) / (1 + exp(-0.1 * (U + 32)));
  else
    ah = 0.057 * exp(-(U + 80) / 6.8); 
    bh = 2.7 * exp(0.079 * U) + 3.1e5 * exp(0.3485 * U); 
    
    aj = (-2.5428e4 * exp(0.2444 * U) - 6.948e-6 * exp(-0.04391 * U)) * &
        (U + 37.78) / (1 + exp(0.311 * (U + 79.23)));
    bj = (0.02424 * exp(-0.01052 * U )) / (1 + exp(-0.1378 * (U + 40.14)));
  endif
  
  tauh = 1 / (ah + bh); 
  hss = 1 / ((1 + exp((U + 71.55) / 7.43))**2);
 
  tauj = 1 / (aj + bj);
  jss = 1 / ((1 + exp((U + 71.55) / 7.43))**2);         
 
  !dm = (mss - crr2m%m) / taum;
  !dh = (hss - crr2m%h) / tauh;
  !dj = (jss - crr2m%j) / tauj;
    
  ! I_kr: Rapidly Activating K Current
  xrss = 1 / (1 + exp(-(U + 10) / 5));
  tauxr = 3300 / ((1 + exp((-22 - U) / 9)) * (1 + exp((U +11) / 9))) + &
        230 / (1 + exp((U +40) / 20));
  !dxkr = (xrss - crr2m%xkr) / tauxr;
  
  ! I_ks: Slowly Activating K Current  
  xsss = 1 / (1 + exp(-(U + 3.8) / 14.25));
  tauxs = 990.1 / (1 + exp(-(U + 2.436) / 14.12));
  !dxks = (xsss-crr2m%xks) / tauxs;
    
  ! I_to: Transient Outward K Current
  !Slow component
  xtoss = 1 / (1 + exp(-(U - 19.0) / 13));
  ytoss = 1 / (1 + exp((U + 19.5) / 5));
  tauxtos = 9 / (1 + exp((U + 3.0) / 15)) + 0.5;
  tauytos = 800 / (1 + exp((U + 60.0) / 10)) + 30;
  !dxtos = (xtoss - crr2m%xtos) / tauxtos;
  !dytos = (ytoss - crr2m%ytos) / tauytos;
  !Fast component
  tauxtof = 8.5 * exp(-((U + 45) / 50)**2) + 0.5;
  tauytof = 85 * exp((-(U + 40)**2 / 220)) + 7;
  !dxtof = (xtoss - crr2m%xtof) / tauxtof;
  !dytof = (ytoss - crr2m%ytof) / tauytof;

  ! I_Ca: L-type Calcium Current
  dss = 1 / (1 + exp(-(U + 5) / 6.0));
  alpha_d = 1.4/(1+exp((-35-U)/13)) +0.25;
  beta_d = 1.4 /(1+ exp((U+5)/5));
  gamma_d = 1/(1+exp((50-U)/20));
  taud = alpha_d*beta_d+gamma_d;

  fss = 1/(1+exp((U+24.63)/5.87));
  alpha_f = 1102.5*exp(-((U+27)/15)**2);
  beta_f = 200/(1+exp((20.6-U)/10));
  gamma_f = 180/(1+exp((U+30)/10))+20;
  tauf = alpha_f + beta_f + gamma_f;
  
  f2ss = 0.67/(1+exp((U+24.63)/5.87))+0.33;
  alpha_f2 = 300*exp(-(U+25)**2/170);
  gamma_f2 = 16 / (1 + exp((U+30)/10));
  tauf2 = alpha_f2 + gamma_f2;
  
  !dd = (dss - crr2m%d) / taud;
  !df = (fss - crr2m%f) / tauf;
  !df2 = (f2ss - crr2m%f2) / tauf2;
  
  dfCaBJ = 1.7*crr2m%CaJ*(1-crr2m%fCaBJ) - 11.9e-3*crr2m%fCaBJ;
  dfCaBSL = 1.7*crr2m%CaSL*(1-crr2m%fCaBSL) - 11.9e-3*crr2m%fCaBSL;
  
  ! SR fluxes: Calcium Release, SR Ca pump, SR Ca leak
  MaxSR = 15; 
  MinSR = 1;
  kCaSR = MaxSR - (MaxSR - MinSR) / (1 + (p_ec50SR / crr2m%CaSR)**2.5);
  koSRCa = p_koCa / kCaSR;
  kiSRCa = p_kiCa * kCaSR;
  RI = 1 - crr2m%RyRr - crr2m%RyRo - crr2m%RyRi;
  dRyRr = (p_kim * RI - kiSRCa * crr2m%CaJ * crr2m%RyRr) - &
        (koSRCa * crr2m%CaJ**2 * crr2m%RyRr - p_kom * crr2m%RyRo);
  dRyRo = (koSRCa * crr2m%CaJ**2 * crr2m%RyRr - p_kom * crr2m%RyRo) - &
        (kiSRCa * crr2m%CaJ * crr2m%RyRo - p_kim * crr2m%RyRi);
  dRyRi = (kiSRCa * crr2m%CaJ * crr2m%RyRo - p_kim * crr2m%RyRi) - &
        (p_kom * crr2m%RyRi - koSRCa * crr2m%CaJ**2 * RI);
  
  !Update gates
  !crr2m%m = crr2m%m + dm * dt;
  !crr2m%h = crr2m%h + dh * dt;
  !crr2m%j = crr2m%j + dj * dt;
  crr2m%m = mss - (mss - crr2m%m) * exp(-dt/taum);
  crr2m%h = hss - (hss - crr2m%h) * exp(-dt/tauh);
  crr2m%j = jss - (jss - crr2m%j) * exp(-dt/tauj);
  
  !crr2m%xkr = crr2m%xkr + dxkr * dt;    
  !crr2m%xks = crr2m%xks + dxks * dt;
  crr2m%xkr = xrss - (xrss - crr2m%xkr) * exp(-dt/tauxr);
  crr2m%xks = xsss - (xsss - crr2m%xks) * exp(-dt/tauxs);
  
  !crr2m%xtof = crr2m%xtof + dxtof * dt;
  !crr2m%ytof = crr2m%ytof + dytof * dt;
  !crr2m%xtos = crr2m%xtos + dxtos * dt;
  !crr2m%ytos = crr2m%ytos + dytos * dt;
  crr2m%xtof = xtoss - (xtoss - crr2m%xtof) * exp(-dt/tauxtof);
  crr2m%ytof = ytoss - (ytoss - crr2m%ytof) * exp(-dt/tauytof);
  crr2m%xtos = xtoss - (xtoss - crr2m%xtos) * exp(-dt/tauxtos);
  crr2m%ytos = ytoss - (ytoss - crr2m%ytos) * exp(-dt/tauytos);
  
  !crr2m%d = crr2m%d + dd * dt;
  !crr2m%f = crr2m%f + df * dt;
  !crr2m%f2 = crr2m%f2 + df2 * dt;
  crr2m%d = dss - (dss - crr2m%d) * exp(-dt/taud);
  crr2m%f = fss - (fss - crr2m%f) * exp(-dt/tauf);
  crr2m%f2 = f2ss - (f2ss - crr2m%f2) * exp(-dt/tauf2);
  
  crr2m%fCaBJ = crr2m%fCaBJ + dfCaBJ * dt;
  crr2m%fCaBSL = crr2m%fCaBSL + dfCaBSL * dt;
  
  crr2m%RyRr = crr2m%RyRr + dRyRr * dt;
  crr2m%RyRo = crr2m%RyRo + dRyRo * dt;
  crr2m%RyRi = crr2m%RyRi + dRyRi * dt;
  
  return
end subroutine gates
!------------------------------------------------------------------------------
subroutine currents (Istm, U, crr2m, Qion, prm, cur) 
!------------------------------------------------------------------------------
! This function computes currents and concentrations for the Carro2 Model
!
!
! XXX:AA-BB, 2008
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: Istm, U
  type(t_crr2m), intent(in)   :: crr2m
  real(rp),    intent(out)  :: Qion
  type(t_prm), intent(in)   :: prm
  type(t_cur), intent(out)  :: cur
  !.
  real(rp):: ENaJunc, ENaSL, EK, Eks, ECaJunc, ECaSL, ECl, sigma, fNaK, rkr,   &
             kpkp, aK1, bK1, K1ss, KaJunc, KaSL, s1Junc, s2Junc, s3Junc, s1SL, &
             s2SL, s3SL, IbarCaJ, IbarCaSL, IbarK, IbarNaJ, IbarNaSL

  !. Nerst Potentials
  ENaJunc = p_RTF * log(p_Nao / crr2m%NaJ);
  ENaSL = p_RTF * log(p_Nao / crr2m%NaSL);
  EK = p_RTF * log(p_Ko / crr2m%Ki);
  Eks = p_RTF * log((p_Ko + p_pNaK * p_Nao) / &
        (crr2m%Ki + p_pNaK * crr2m%Nai));
  ECaJunc = p_RTF / 2 * log(p_Cao*prm%A_Cao / crr2m%CaJ);
  ECaSL = p_RTF / 2 * log(p_Cao*prm%A_Cao / crr2m%CaSL);
  ECl = p_RTF * log(p_Cli / p_Clo);
  
  ! INa: Fast Na Current
  
  cur%INaJunc = prm%A_GNa * p_FJunc * p_GNa * crr2m%m**3 * crr2m%h * crr2m%j * (U - ENaJunc);
  cur%INaSL = prm%A_GNa * p_FSL * p_GNa * crr2m%m**3 * crr2m%h * crr2m%j * (U - ENaSL);
  cur%INa = cur%INaJunc + cur%INaSL;

  ! I_nabk: Na Backcrr2ound Current
  cur%INaBkJunc = prm%A_GNabk * p_FJunc * p_GNaB * (U - ENaJunc);
  cur%INaBkSL = prm%A_GNabk * p_FSL * p_GNaB * (U - ENaSL);
  cur%INaBk = cur%INaBkJunc + cur%INaBkSL;

  ! I_nak: Na/K Pump current
  sigma = (exp(p_Nao / 67.3) - 1) / 7;
  fNaK = 1 / (1 + 0.1245 * exp(-0.1 * U * p_iRTF) + &
        0.0365 * sigma * exp(-U*p_iRTF));
  cur%INaKJunc = prm%A_GNaK * p_FJunc * p_IbarNaK * fnak * p_Ko / &
        ((1 + (p_KmNaip / crr2m%NaJ)**4) * (p_Ko + p_KmKo));
  cur%INaKSL = prm%A_GNaK * p_FSL * p_IbarNaK * fnak * p_Ko / &
        ((1 + (p_KmNaip / crr2m%NaSL)**4) * (p_Ko + p_KmKo));
  cur%INaK = cur%INaKJunc + cur%INaKSL;

  ! I_kr: Rapidly Activating K Current
  rkr = 1 / (1 + exp((U + 74) / 24));
  cur%Ikr = prm%A_GKr * p_GKr * sqrt(p_Ko / 5.4) * crr2m%xkr * rkr * (U - EK);
 
  ! I_ks: Slowly Activating K Current
  cur%IksJunc = prm%A_GKs * p_FJunc * p_GKsJunc * crr2m%xks**2 * (U - Eks);
  cur%IksSL = prm%A_GKs * p_FSL * p_GKsSL * crr2m%xks**2 * (U - Eks);
  cur%Iks = cur%IksJunc + cur%IksSL;
  
  ! I_kp: Plateau K Current
  kpkp = 1 / (1 + exp(7.488 - U / 5.98));
  !cur%IkpJunc = p_FJunc * p_Gkp * kpkp * (U - EK);
  !cur%IkpSL = p_FSL * p_Gkp * kpkp * (U - EK);
  !cur%Ikp = cur%IkpJunc + cur%IkpSL;
  cur%Ikp = p_Gkp * kpkp * (U - EK);
  
  ! I_to: Transient Outward K Current
  ! Slow component
  cur%Itos = prm%A_Gto * prm%p_Gtos * crr2m%xtos * crr2m%ytos * (U - EK);
  ! Fast component
  cur%Itof = prm%A_Gto * prm%p_Gtof * crr2m%xtof * crr2m%ytof * (U - EK);
    
  cur%Ito = cur%Itos + cur%Itof;

  ! I_K1: Inward Rectifier K Current
  ak1 = 4.0938/(1.0+exp(0.12165*(U - EK - 49.9344)));
  bk1 = (15.7197*exp(0.06739*(U-EK-3.2571)) + exp(0.06175*(U-EK-594.31)))/ &
        (1.0+exp(-0.16285*(U - EK + 14.2067)));
  K1ss = ak1/(ak1+bk1);
  cur%IK1 = prm%A_GK1 * p_GK1 * sqrt(p_Ko / 5.4) * K1ss * (U - EK);

  ! I_ClCa: Ca-activated Cl Current
  cur%IClCaJunc = prm%A_GClCa * p_FJunc * p_GClCa / (1 + p_KdClCa / crr2m%CaJ) * (U - ECL);
  cur%IClCaSL = prm%A_GClCa * p_FSL * p_GClCa / (1 + p_KdClCa / crr2m%CaSL) * (U - ECL);
  cur%IClCa = cur%IClCaJunc + cur%IClCaSL;

  ! I_Clbk: Background Cl Current
  cur%IClBk = prm%A_GClbk * p_GClB * (U - ECl);
  
  ! I_Ca: L-type Calcium Current
     
  IbarCaJ = p_pCa * (U * p_F * p_iRTF) * (crr2m%CaJ * &
    exp(2 * U * p_iRTF) - p_Cao*prm%A_Cao) / (exp(2 * U * p_iRTF) - 1);
  IbarCaSL = p_pCa * (U * p_F * p_iRTF) * (crr2m%CaSL * &
    exp(2 * U * p_iRTF) - p_Cao*prm%A_Cao) / (exp(2 * U * p_iRTF) - 1);
  IbarK = p_pK * (U * p_F * p_iRTF) * (crr2m%Ki * &
    exp(U * p_iRTF) - p_Ko) / (exp(U * p_iRTF) - 1);
  IbarNaJ = p_pNa * (U * p_F * p_iRTF) * (crr2m%NaJ * &
    exp(U * p_iRTF) - p_Nao) / (exp(U * p_iRTF) - 1);
  IbarNaSL = p_pNa * (U * p_F * p_iRTF) * (crr2m%NaSL * &
    exp(U * p_iRTF) - p_Nao) / (exp(U * p_iRTF) - 1);

  cur%ICaJunc = prm%A_GCaL*(p_FJuncCaL * IbarCaJ * crr2m%d * crr2m%f * crr2m%f2 * &
    (1-crr2m%fCaBJ));
  cur%ICaSL = prm%A_GCaL*(p_FSLCaL * IbarCaSL * crr2m%d * crr2m%f * crr2m%f2 * &
    (1-crr2m%fCaBSL));
  cur%ICa = cur%ICaJunc + cur%ICaSL;
  
  cur%ICaK = prm%A_GCaL*(IbarK * crr2m%d * crr2m%f * crr2m%f2 * (p_FJuncCaL * (1-crr2m%fCaBJ) + &
    p_FSLCaL * (1-crr2m%fCaBSL)));
      
  cur%ICaNaJunc = prm%A_GCaL*(p_FJuncCaL * IbarNaJ * crr2m%d * crr2m%f * crr2m%f2 * &
    (1-crr2m%fCaBJ));
  cur%ICaNaSL = prm%A_GCaL*(p_FSLCaL * IbarNaSL * crr2m%d * crr2m%f * crr2m%f2 * &
    (1-crr2m%fCaBSL));  
  cur%ICaNa = cur%ICaNaJunc + cur%ICaNaSL;

  cur%ICaL = cur%ICa + cur%ICaK + cur%ICaNa;

  ! I_ncx: Na/Ca Exchanger current
  KaJunc = 1 / (1 + (p_Kdact / crr2m%CaJ)**2);
  KaSL = 1 / (1 + (p_Kdact / crr2m%CaSL)**2);
  s1Junc = exp(p_nu * U * p_iRTF) * crr2m%NaJ**3 * p_Cao*prm%A_Cao;
  s1SL = exp(p_nu * U * p_iRTF) * crr2m%NaSL**3 * p_Cao*prm%A_Cao;
  s2Junc = exp((p_nu - 1) * U * p_iRTF) * p_Nao**3 * crr2m%CaJ;
  s3Junc = p_KmCai * p_Nao**3 * (1 + (crr2m%NaJ / p_KmNai)**3) + &
        p_KmNao**3 * crr2m%CaJ * (1 + crr2m%CaJ / p_KmCai) + &
        p_KmCao * crr2m%NaJ**3 + crr2m%NaJ**3 * p_Cao*prm%A_Cao + &
        p_Nao**3 * crr2m%CaJ;
  s2SL = exp((p_nu - 1) * U * p_iRTF) * p_Nao**3 * crr2m%CaSL;
  s3SL = p_KmCai * p_Nao**3 * (1 + (crr2m%NaSL / p_KmNai)**3) + &
        p_KmNao**3 * crr2m%CaSL * (1 + crr2m%CaSL / p_KmCai) + &
        p_KmCao * crr2m%NaSL**3 + crr2m%NaSL**3 * p_Cao*prm%A_Cao + &
        p_Nao**3 * crr2m%CaSL;

  cur%IncxJunc = prm%A_Gncx * p_FJunc * p_IbarNCX * KaJunc * (s1Junc - s2Junc) / &
        s3Junc / (1 + p_ksat * exp((p_nu - 1) * U * p_iRTF));
  cur%IncxSL = prm%A_Gncx * p_Fsl * p_IbarNCX * KaSL * (s1SL - s2SL) / s3SL / &
        (1 + p_ksat * exp((p_nu - 1) * U * p_iRTF));
  cur%Incx = cur%IncxJunc + cur%IncxSL;

  ! I_pCa: Sarcolemmal Ca Pump Current
  cur%IpCaJunc = prm%A_GpCa * p_FJunc * p_IbarPMCA * crr2m%CaJ**1.6 / &
        (p_KmPCa**1.6 + crr2m%CaJ**1.6);
  cur%IpCaSL = prm%A_GpCa * p_FSL * p_IbarPMCA * crr2m%CaSL**1.6 / &
        (p_KmPCa**1.6 + crr2m%CaSL**1.6);
  cur%IpCa = cur%IpCaJunc + cur%IpCaSL;

  ! I_Ca_bk: Backcrr2ound Ca Current
  cur%ICaBkJunc = prm%A_GCabk * p_FJunc * p_GCaB * (U - ECaJunc);
  cur%ICaBkSL = prm%A_GCabk * p_FSL * p_GCaB * (U - ECaSL);
  cur%ICaBk = cur%ICaBkJunc + cur%ICaBkSL;
     
  ! Sodium Concentrations
  cur%INatotJunc = cur%INaJunc + cur%INaBkJunc + 3 * cur%IncxJunc + &
        3 * cur%INaKJunc + cur%ICaNaJunc;
  cur%INatotSL = cur%INaSL + cur%INaBkSL + 3 * cur%IncxSL + &
        3 * cur%INaKSL + cur%ICaNaSL;

  ! Potassium concentration
  cur%IKtot = cur%Ito + cur%Ikr + cur%Iks + cur%IK1 - 2 * cur%INaK + &
        cur%ICaK + cur%Ikp + Istm;
    
  ! Calcium concentrations
  cur%ICatotJunc = cur%ICaJunc + cur%ICaBkJunc + cur%IpCaJunc -  &
                   2 * cur%IncxJunc;
  cur%ICatotSL = cur%ICaSL + cur%ICaBkSL + cur%IpCaSL -  &
                   2 * cur%IncxSL;
  ! Membrane Potential
  cur%INatot = cur%INatotJunc + cur%INatotSL;
  cur%ICltot = cur%IClCa + cur%IClBk;
  cur%ICatot = cur%ICatotJunc + cur%ICatotSL;
  cur%Itot = cur%INatot + cur%ICltot + cur%ICatot + cur%IKtot - Istm;
   
  !------------------------------------
  Qion = cur%Itot;

  return
  
end subroutine currents
!-------------------------------------------------------------------------------
subroutine Carro2_A_P01 (ict,dt,Istm,U,Iion,v_prm,v_crr2,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict   !.Cell Model 
  real(rp),    intent(in)    :: dt, Istm, U
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp), intent(inout)    :: v_crr2(nvar_crr2)
  real(rp),    intent(out)   :: v_cr(:)
  !.
  type(t_crr2m)               :: crr2m
  type (t_prm)               :: param
  type(t_cur)                :: cur
  !
  call put_me_struct(v_crr2,crr2m)
  call put_param(v_prm,param)
  !
  call currents ( Istm, U, crr2m, Iion, param, cur)
  call concentrations ( dt, cur, param, crr2m)
  call gates( dt, U, param, crr2m)
  !  
  call get_me_struct(crr2m,v_crr2)
  !.
  v_cr(1:ncur_crr2) =(/ cur%INa, cur%INaBk, cur%INaK, cur%Ikr, cur%Iks, cur%Ikp,   &
                 cur%Itos, cur%Itof, cur%Ito, cur%IK1, cur%IclCa,cur%IClBk, &
                 cur%ICa, cur%ICaK, cur%ICaNa, cur%ICaL, cur%Incx, cur%IpCa,&
                 cur%ICaBk, cur%IKtot, cur%INatot, cur%ICltot, cur%ICatot,  &
                 cur%Itot /)
  
  return
end subroutine Carro2_A_P01
!-------------------------------------------------------------------------------
end module mod_carro2
!-------------------------------------------------------------------------------
