!------------------------------------------------------------------------------
! This are the parameters for the Roger and Mcculloch Model
!------------------------------------------------------------------------------
  real(rp), parameter, private   :: vp   = 100.0
  real(rp), parameter, private   :: eta1 =   4.4
  real(rp), parameter, private   :: eta2 =   0.012
  real(rp), parameter, private   :: eta3 =   1.0
  real(rp), parameter, private   :: G    =   1.5
  real(rp), parameter, private   :: vth  =  13.0

!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: RM_mod   = 0;  ! Roger and McCulloch model (P)
  integer(ip),parameter  :: nvar_rmc = 1;  ! Number of state variables
  integer(ip),parameter  :: ncur_rmc = 0;  ! Number of currents
  integer(ip),parameter  :: np_rmc   = 6;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS ------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter   :: Vi_RMC    = 0.0_rp
