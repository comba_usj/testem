! ------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! ------------------------------------------------------------------------------
module mod_rm
! ------------------------------------------------------------------------------
  use mod_precision
! ------------------------------------------------------------------------------
  implicit none
  include 'rm_parameters.inc'

  public  :: RMC_A_P01, get_parameter_RM 
  private :: GateW, F_ION, put_parameter_RM

  type, public:: t_prm
          private
          real(rp) :: vp
          real(rp) :: eta1
          real(rp) :: eta2
          real(rp) :: eta3
          real(rp) :: G
          real(rp) :: vth
  end type t_prm 
! ------------------------------------------------------------------------------
contains
!------------------------------------------------------------------------------!
subroutine write_state_RM(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)', advance='no') 'w '
end subroutine write_state_RM
!------------------------------------------------------------------------------!
subroutine write_current_RM(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_RM
! ------------------------------------------------------------------------------
function get_parameter_RM (t_cel) result (v_prm)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)             :: t_cel
  real (rp)               :: v_prm(np_rmc)

!  v_prm = (/ vp, eta1, eta2, eta3, G, vth/)
  v_prm = (/ 1.0, 1.0, 1.0, 1.0, 1.0, 1.0/)
end function get_parameter_RM
! ------------------------------------------------------------------------------
subroutine put_parameter_RM(v_prm, param)
  implicit none
  real(rp), intent(in)    :: v_prm(:)
  type(t_prm), intent(out):: param

  param%vp   = v_prm(1)*vp
  param%eta1 = v_prm(2)*eta1
  param%eta2 = v_prm(3)*eta2
  param%eta3 = v_prm(4)*eta3
  param%G    = v_prm(5)*G
  param%vth  = v_prm(6)*vth
end subroutine put_parameter_RM
! ------------------------------------------------------------------------------
subroutine RMC_A_P01 ( V, dt, W, Iion,v_prm)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: V, dt, v_prm(:)
  real(rp), intent(inout) :: W
  real(rp), intent(out)   :: Iion
  type(t_prm)             :: param

  call put_parameter_RM(v_prm, param)
  W   = GateW(V,W,dt,param)
  Iion= F_ION(V,W,param)
  return
end subroutine RMC_A_P01
! ------------------------------------------------------------------------------
function GateW(V,Wo,dt,prm) result (W)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: V,Wo,dt
  type(t_prm), intent(in) :: prm
  real(rp)                :: W

  W = Wo+ prm%eta2*(V/prm%vp-prm%eta3*Wo)*dt
  return
end function GateW
! ------------------------------------------------------------------------------
function F_ION(V,W,prm) result (Qi)
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(in)     :: V,W
  type(t_prm), intent(in) :: prm
  real(rp)                :: Qi

  Qi = prm%G*V*(1.0 - V/prm%vth)*(1.0 - V/prm%vp) + prm%eta1*V*W
  return
end function F_ION
! ------------------------------------------------------------------------------
end module mod_rm
