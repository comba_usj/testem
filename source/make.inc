#------------------------------------------------------------------------------#
#-------------------------- TEST_ME make include file--------------------------#
#------------------------------------------------------------------------------#

COMP=gcc

SELF_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

EXECNAME = testem_$(COMP)

TESTEM  = $(SELF_DIR)
HPREC = $(TESTEM)/prcsn
H_MEL = $(TESTEM)/em_src
H_SRC = $(TESTEM)/src
HCRR  = $(H_MEL)/carro
HCRR2 = $(H_MEL)/carro2
HCOURT= $(H_MEL)/courtemanche
HMC   = $(H_MEL)/FIBmaccannell
HFK   = $(H_MEL)/FK
HGR   = $(H_MEL)/grandi
HGRHF = $(H_MEL)/grandiHF
HIZ   = $(H_MEL)/IZ
HLR2K = $(H_MEL)/lr2k
HMLK  = $(H_MEL)/maleckar
HNYGR = $(H_MEL)/nygren
HOHR  = $(H_MEL)/ohara
HOHRNA= $(H_MEL)/oharaNa
HPKFST= $(H_MEL)/PKFstewart
HRYMC = $(H_MEL)/rmcl
HTENT = $(H_MEL)/tentusscher

LIB   = $(TESTEM)/lib
LIBME = -L$(LIB) -llr2k -lnygren -lrmc -ltentusscher -lFK -lIZ \
	-lpkfstewart -lgrandi -lgrandiHF -lcarro -lcarro2 \
	-lMacCannell -lmaleckar -lohara -loharana -lcourtemanche

ifeq ($(COMP),intel)
 F95   = ifort
 MODU= -module $(TESTEM)/mod
 COM = -O3 -parallel -heap-arrays
endif

ifeq ($(COMP),gcc)
 F95   = gfortran
 MODU= -I$(TESTEM)/mod -J$(TESTEM)/mod
 COM = -O3 -ffree-line-length-none 
endif

ifeq ($(COMP),ibm)
 MODU= -I$(TESTEM)/mod -qmoddir=$(TESTEM)/mod
 F95   = xlf90_r
 COM = -O3  -qsmallstack=dynlenonheap
endif

ifeq ($(COMP),gccOsx)
 F95   = gfortran
 MODU= -J$(TESTEM)/mod
 COM = -O3 -ffree-line-length-none -fconserve-stack
endif
